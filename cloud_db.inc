<?php

/**
 * @file
 * Provides common database-related functionalites for cloud management.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


/**
 * update cloud table with latest timestamp
 * @return unknown_type
 */
function cloud_update_host_entries_last_update_time($cloud_context) {
  
  db_query( 'update {' . CLOUD_CLOUDS_TABLE . "} set last_update_time  = CURRENT_TIMESTAMP where cloud_name  = '" . $cloud_context . "' ");
  
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('Cloud has been modified: @cloud_context', array('@cloud_context' => $cloud_context)), 
      'link'    => '', //'design/alerts/create&id=' . $alert_id
    )
  ); 
    
  return;
}


function cloud_get_all_scripts($type) {
 
  $query = _cloud_scripting_get_scripts() ;
  //$query .= tablesort_sql( array(array('field' => 'name', 'sort' => 'asc')) ) ;
  $script_options = array();
  $query_args[] = 'type' ;
  $query_args[] = $type ;
  $result = db_query( $query, $query_args ); 
  
  while ($script = db_fetch_object($result)) {
  
    $script_options[$script->id] = $script->name ;
  }
  //asort($script_options);
  return $script_options ;
}


