; Clanavi makefile
; ----------------
; This make file will pull all the clanavi related modules from Drupal.org

core = 6.x

api = 2

projects[] = drupal

projects[] = aws

projects[] = drupal_queue

projects[] = rest_client

projects[cloud][type] = module
projects[cloud][download][type] = git
projects[cloud][download][url] = http://git.drupal.org/sandbox/yas/1075708.git

projects[] = iaas

projects[] = simpletest
