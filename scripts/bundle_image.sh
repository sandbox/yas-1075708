#!/bin/bash

#
# @file
# Used for this module
#
# @date   12/06/2010
# @author Yas Naoi, IT Manager, DOCOMO USA Labs (naoi at docomolabs-usa.com)
#
# @note
# 
# Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
# 
# DOCOMO Communication Laboratories USA, Inc. (DOCOMO USA Labs) licenses
# this software under the GNU General Public License version 2 (GPL2)
# for the software disclosed herein. For terms and conditions under the GPL2,
# please refer to www.gnu.org.
#
# DOCOMO USA Labs may not be held liable for failing to maintain or support
# the software, and DOCOMO USA Labs is not responsible for any damage or loss
# experienced from using the software.
#
# Please contact the IT Manager at DOCOMO USA Labs for all issues
# related to the software.
#
#

DEBUG=1;

TMP_FILE_NAME=`date +%Y-%m-%d-%T`
#LOG_FILE='/tmp/clanavi/bundle_image-'$TMP_FILE_NAME'.log';
LOG_FILE='/tmp/clanavi/bundle_image-test.log';

function log
{
  if [ "$DEBUG" -eq 1 ]; then
    echo $1 >> $LOG_FILE ;
  fi;
}

log "Started the Bundling Instance Process"

log "Checking if user is root"

if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root" 1>&2
  log "This script must be run as root" 1>&2
  exit 1
fi

GREP="`which grep`";
if which apt-get >/dev/null; then
  PACKAGE_MANAGEMENT="`which apt-get` "
else
  PACKAGE_MANAGEMENT="`which yum`"
fi

log "Using Package Manager $PACKAGE_MANAGEMENT"

if which dpkg >/dev/null; then
  PACKAGE_TEST="dpkg --get-selections | $GREP -q "
else
  PACKAGE_TEST="rpm -qa | $GREP -q "
fi

CURL_OPTS=" --silent --retry 1 --retry-delay 1 --retry-max-time 1 "



function log_error
{
  echo -e $1;
  log $1
  exit 1;
}

function check_env
{
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc;
  fi

  log "Check for Amazon EC2 toolkit"
  
  # Tools already exist
  if [ -z `which ec2-describe-instances` ] || [ -z `which ec2-upload-bundle` ] ; then
    log "Amazon EC2 toolkit missing!"
    install_ec2;
  fi

  log "Done Check for Amazon EC2 toolkit"
  log "Check for EC2_HOME"
  
  # EC2_HOME set
  if [ -z "$EC2_HOME" ]; then
    log "Amazon EC2 is not set-up correctly! EC2_HOME not set"
    if ! grep EC2_HOME ~/.bashrc; then
      echo "export EC2_HOME=/usr/local/ec2-api-tools/" >> ~/.bashrc
    fi;
    export EC2_HOME=/usr/local/ec2-api-tools/
    source ~/.bashrc
  fi

  log "Done Check for EC2_HOME"
  
  log "Check for JAVA_HOME"
  # Java
  if [ -z "$JAVA_HOME" ]; then

    if grep -i yum "$PACKAGE_MANAGEMENT" > /dev/null; then
      log_error "nPlease install java manually (do not use yum install java, it is incompatible)nsee JRE http://java.sun.com/javase/downloads/index.jspnDownload, run the bin file, place in /opt/ and update ~/.bashrc. Once complete run 'source ~/.bashrc;'";
    fi;

    $PACKAGE_MANAGEMENT install -y sun-java6-jdk
    JAVA_PATH=/usr/lib/jvm/java-6-sun/jre/;

    echo "export JAVA_HOME=$JAVA_PATH" >> ~/.bashrc
    export JAVA_HOME=$JAVA_PATH
    source ~/.bashrc
  fi

  log "Done Check for JAVA_HOME"
  
  # Keys
  EC2_HOME_DIR='.ec2';
  EC2_PRIVATE_KEY_FILE="/tmp/clanavi/$EC2_HOME_DIR/pk.pem";
  EC2_CERT_FILE="/tmp/clanavi/$EC2_HOME_DIR/cert.pem";

#  if [ ! -d "/tmp/clanavi/$EC2_HOME_DIR" ]; then
#    mkdir -pv "/tmp/clanavi/$EC2_HOME_DIR";
#  fi

  log "Check for CERT file and PVT file"  
  install_ec2_env EC2_PRIVATE_KEY "$EC2_PRIVATE_KEY_FILE";
  install_ec2_env EC2_CERT        "$EC2_CERT_FILE";

  install_ec2_keys_files "$EC2_PRIVATE_KEY_FILE" "Private key";
  install_ec2_keys_files "$EC2_CERT_FILE" "Certificate";

  log "Done Check for CERT file and PVT file"  
  get_region
  get_availability_zone

}

function install_ec2_env
{
  # Variable Variable for $1
  EC2_VARIABLE=${!1};
  EC2_VARIABLE_NAME=$1;
  EC2_FILE=$2;

  #log "VARIABLE: $EC2_VARIABLE_NAME=$EC2_VARIABLE";

  # Variable Variable
  if [ -z "$EC2_VARIABLE" ]; then
    log "Amazon $EC2_VARIABLE_NAME is not set-up correctly!";

    if ! grep -q "$EC2_VARIABLE_NAME" ~/.bashrc > /dev/null; then
      echo "export $EC2_VARIABLE_NAME=$EC2_FILE" >> ~/.bashrc;
    fi;
    export $EC2_VARIABLE_NAME=$EC2_FILE;
    source ~/.bashrc
  else
    if ! grep -q "$EC2_VARIABLE_NAME" ~/.bashrc > /dev/null; then
      echo "export $EC2_VARIABLE_NAME=$EC2_FILE" >> ~/.bashrc;
    else
      log "Amazon $EC2_VARIABLE_NAME var installed";
    fi;
  fi
}

function install_ec2_keys_files
{
  EC2_FILE=$1;
  EC2_DESCRIPTION=$2;
  EC2_CONTENTS='';

  if [ ! -f "$EC2_FILE" ]; then
    log_error "Amazon $EC2_FILE does not exist, please copy your $EC2_DESCRIPTION to $EC2_FILE and re-run this script";
  else
    log "Amazon $EC2_FILE file installed";
  fi
}

function install_ec2
{

  log "Installing packages..."  
  for PACKAGE in curl wget tar bzip2 unzip zip symlinks unzip ruby; do
    if ! which "$PACKAGE" >/dev/null; then
      $PACKAGE_MANAGEMENT install -y $PACKAGE;
    fi
  done;

  log "Done installing packages..."  
  log "Checking AMI tools"  

  # AMI Tools
  if [ -z "`which ec2-upload-bundle`" ]; then
    curl -o /tmp/ec2-ami-tools.zip $CURL_OPTS --max-time 30 http://s3.amazonaws.com/ec2-downloads/ec2-ami-tools.zip
    rm -rf /usr/local/ec2-ami-tools;
    cd /usr/local && unzip /tmp/ec2-ami-tools.zip
    ln -svf `find . -type d -name ec2-ami-tools*` ec2-ami-tools
    chmod -R go-rwsx ec2* && rm -rvf /tmp/ec2*
  fi

  log "Done Checking AMI tools"  
  # API Tools
  if [ -z "`which ec2-describe-instances`" ]; then
    log "Amazon EC2 API toolkit is not installed!"
    curl -o /tmp/ec2-api-tools.zip $CURL_OPTS --max-time 30 http://s3.amazonaws.com/ec2-downloads/ec2-api-tools.zip
    rm -rf /usr/local/ec2-api-tools;
    cd /usr/local && unzip /tmp/ec2-api-tools.zip
    ln -svf `find . -type d -name ec2-api-tools*` ec2-api-tools
    chmod -R go-rwsx ec2* && rm -rvf /tmp/ec2*
  fi

  log "Done Amazon EC2 API toolkit Check"  
  ln -sf /usr/local/ec2-api-tools/bin/* /usr/bin/;
  ln -sf /usr/local/ec2-ami-tools/bin/* /usr/bin/;
  
  rm -f /usr/bin/ec2-*.cmd;

}

function get_availability_zone
{
  # Not reliable between availability zones using meta-data
  # export EC2_AVAILABILITY_ZONE="`curl $CURL_OPTS --max-time 2 http://169.254.169.254/2009-04-04/meta-data/placement/availability-zone`"

  get_instance_id;

  if [ ! -z "$EC2_INSTANCE_ID" ]; then
    EC2_AVAILABILITY_ZONE="`ec2-describe-instances | grep -q $EC2_INSTANCE_ID | awk '{print $11}'`"
    if [ -z "$EC2_AVAILABILITY_ZONE" ] && [ ! "$EC2_AVAILABILITY_ZONE"="" ]; then
      export EC2_AVAILABILITY_ZONE=$EC2_AVAILABILITY_ZONE;
      install_ec2_env EC2_AVAILABILITY_ZONE $EC2_AVAILABILITY_ZONE;
    fi;
  fi;
}

function get_region
{
  get_instance_id;
  if [ ! -z "$EC2_INSTANCE_ID" ]; then
    EC2_REGION="`ec2-describe-instances | grep $EC2_INSTANCE_ID | awk '{print $11}'`"
    if [ -z "$EC2_REGION" ]; then
      export EC2_REGION=$EC2_REGION;
      install_ec2_env EC2_REGION $EC2_REGION;
      install_ec2_env EC2_URL "https://ec2.$EC2_REGION.amazonaws.com" | sed 's/a.amazonaws.com/.amazonaws.com/'
    fi;
  fi;
}

function get_instance_id
{
  instance_id="`curl $CURL_OPTS --max-time 2 http://169.254.169.254/1.0/meta-data/instance-id`"
  if [ ! -z "$instance_id" ]; then
    export EC2_INSTANCE_ID="$instance_id";
  fi;
}

log "Checking environment [ec2 tools]"

check_env

log "Done Checking environment [ec2 tools]"


prefix=clanavi
bucket=CLANAVI_BUCKET_NAME
size=CLANAVI_SIZE

export AWS_USER_ID=CLANAVI_AWS_USER_ID
export AWS_ACCESS_KEY_ID=CLANAVI_AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=CLANAVI_AWS_SECRET_ACCESS_KEY

if [ $(uname -m) = 'x86_64' ]; then
  arch=x86_64
else
  arch=i386
fi

log "Starting ec2-bundle-vol"
ec2-bundle-vol -r $arch -d /mnt -p $prefix  -u $AWS_USER_ID -k $EC2_PRIVATE_KEY_FILE -c $EC2_CERT_FILE -s $size -e /mnt,/root/.ssh,/tmp/clanavi/$EC2_HOME_DIR >> $LOG_FILE
log "Completed ec2-bundle-vol"
log "Starting ec2-upload-bundle"
ec2-upload-bundle  -b $bucket  -m /mnt/$prefix.manifest.xml -a $AWS_ACCESS_KEY_ID -s $AWS_SECRET_ACCESS_KEY -url CLANAVI_IMAGE_UPLOAD_URL >> $LOG_FILE
log "Completed ec2-upload-bundle"
log "Starting ec2-register"
ec2-register  -K $EC2_PRIVATE_KEY_FILE -C $EC2_CERT_FILE $bucket/$prefix.manifest.xml -U 'CLANAVI_IMAGE_REGISTER_URL' >> $LOG_FILE
log "Completed ec2-register"

rm -fr /tmp/clanavi/$EC2_HOME_DIR ;






