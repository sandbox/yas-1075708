<?php

/**
 * @file
 * Enables users to access the Privately managed clouds.
 * Provides common functionalites for cloud management.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

module_load_include('inc', 'cloud', 'cloud_constants');
module_load_include('inc', 'cloud', 'cloud_db');
module_load_include('inc', 'cloud');

/**
 * Implementation of hook_init().
 */
function cloud_init() {

  drupal_add_link(array(
    'type'  => 'text/css',
    'rel'   => 'stylesheet', 
    'media' => 'all', 
    'href'  => base_path()
             . drupal_get_path('module', 'cloud')
             . CLOUD_PATH_SEPARATOR
             . 'css/cloud.css')
  );
}


/**
 * Implementation of hook_help().
 */
function cloud_help($section) {
  
  switch ($section) {

    
    case 'admin/help#':
      $output = '<p>' . t('The cloud module creates a user interface for users to manage clouds. Users can Create Instances,  Describe Instances etc..') . '</p>';
      return $output;
    case 'admin/content/comment':
    case 'admin/content/comment/create':
      return '<p>' . t("Below is a list of the latest comments posted to your site. Click on a subject to see the comment,  the author's name to edit the author's user information ,  'edit' to modify the text,  and 'delete' to remove their submission.") . '</p>';
    case 'admin/content/comment/approval':
      return '<p>' . t("Below is a list of the comments posted to your site that need approval. To approve a comment,  click on 'edit' and then change its 'moderation status' to Approved. Click on a subject to see the comment,  the author's name to edit the author's user information,  'edit' to modify the text,  and 'delete' to remove their submission.") . '</p>';
    case 'admin/content/comment/settings':
      return '<p>' . t("Comments can be attached to any node,  and their settings are below. The display comes in two types: a 'flat list' where everything is flush to the left side,  and comments come in chronological order,  and a 'threaded list' where replies to other comments are placed immediately below and slightly indented,  forming an outline. They also come in two styles: 'expanded',  where you see both the title and the contents,  and 'collapsed' where you only see the title. Preview comment forces a user to look at their comment by clicking on a 'Preview' button before they can actually add the comment.") . '</p>';
   }
}



/**
 * Implementation of hook_menu()
 */
function cloud_menu() {
  
  $items = array();
  $access = user_access('access dashboard');

  
  $items['clouds'] = array(
    //'path'           => 'design', 
    'title'            => 'Clouds', 
    'description'      => 'Multiple Clouds', 
    //'type'           => MENU_SUGGESTED_ITEM, 
    'page callback'    => '_cloud_all_instances', 
    'access arguments' => array('access dashboard'), 
    'weight'           => -10, 
    'file'             => ''
  );

  $items['clouds/getdata'] = array(
    'title'            => 'Clouds get data',
    'description'      => 'Get data of sub clouds.',
    'page callback'    => '_cloud_fetch_data',
    'access arguments' => array('access dashboard'),
    'weight'           => -10,
    'file'             => '' ,
    'type'             => MENU_CALLBACK,
  );

  $items['clouds/callback_get_all_instances_list'] = array(
    'page callback' => '_cloud_callback_get_all_instances_list',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access dashboard'),
  );
  
  return $items;
}


/**
 * Implementation of hook_perm().
 */
function cloud_perm() {
  return array(
    'access dashboard' ,
    'administer cloud' ,
  );
}

/**
 * Returns a form to get list of instances 
 * under all the cloud together
 *
 * @return return a dashboard form
 */
function _cloud_all_instances() {

  return drupal_get_form('cloud_display_dashboard') ;  
}

/**
 * Implementation of hook_theme().
 */
function cloud_theme() {
  return array(
    'cloud_display_dashboard' => array(
      'arguments' => array('form' => NULL), 
    ),
  );
}


/**
 * Modules notify Cloud module when uninstalled, disabled, etc.
 *
 * @param string $op
 *   the module operation: uninstall, install, enable, disable
 * @param string $module
 *   the name of the affected module.
 */
function cloud_notify($op, $module ) {

  switch ($op) {
    case 'install'  :
    case 'uninstall':
    case 'enable'   :
    case 'disable'  :
      // for cloud sub system
      _cloud_set_info                   ($op, $module) ;
      // for resource allocator
      break;
  }
}

/**
 * To fetch the data of sub_clouds info
 * @return return a sub-cloud data and redirect to cloud dashboard page
 */ 
function _cloud_fetch_data() {

  cloud_update_all_cloud_data() ; 
  // Return to the Common Clouds Page
  drupal_goto( 'clouds' ) ;
}


function cloud_update_all_cloud_data() {

  // TODO: Make CONSTANT (by Jamir)
  set_time_limit(1000) ;

  $cloud_list         = cloud_get_all_clouds();
  $cloud_display_list = cloud_get_all_clouds_display_name() ;
  foreach ($cloud_list as $cloud_context) {

    $is_cloud_enabled = cloud_is_settings_done($cloud_context) ;
    if ($is_cloud_enabled === FALSE ) {
    
      // Skip this Cloud since it is not configured
      if (user_access($cloud_context . ' administer cloud')) {
      
        $admin_url = filter_xss( l( t('@cloud_name Settings', array('@cloud_name' => $cloud_display_list[$cloud_context])), "admin/settings/$cloud_context" ) );
      }
      else {
      
        $admin_url = $cloud_display_list[$cloud_context] ;
      }
      
      drupal_set_message(check_plain(t('The variables are not correctly configured: ')) . $admin_url, 'error');
      continue ;
    }
    
    module_invoke($cloud_context, 'cloud_update_data') ;
  }
}

/**
 * Cloud Module Action
 *        This function is used to perform action on the sub-cloud
 * @param string $templateid
 *   Server template id : The template on whcih the action is to be performed
 * @param string $op
 *   the operation: launch, terminate to be executed
 * @param string $cloud_context
 *   The sub-cloud on which the operation is to be executed
 * @param string $params
 *   The parameters to be passed
 * 
 */
function cloud_perform_action($templateid = '', $op, $cloud_context = '' , $params = array(), $all = '' ) {

  $params['templateid'] = $templateid ;
  $params['all'       ] = $all;

  $cloud_list           = cloud_get_all_clouds();
  $cloud_display_list   = cloud_get_all_clouds_display_name() ;

  // Check if Cloud is enabled
  $is_cloud_enabled     = cloud_is_settings_done($cloud_context) ;
  if ($is_cloud_enabled === FALSE ) {

    // Skip this Cloud since it is not configured
    if (user_access($cloud_context . ' administer cloud')) {
    
        $admin_url = filter_xss( l( t('@cloud_name Settings', array('@cloud_name' => $cloud_display_list[$cloud_context])), "admin/settings/$cloud_context" ) );
    }
    else {
        
        $admin_url  = $cloud_display_list[$cloud_context] ;
    }
    
    drupal_set_message(check_plain(t('The variables are not correctly configured: ')) . $admin_url, 'error');
    return ;
  }
   
  return module_invoke( $cloud_context, 'cloud_action', $op, $params ) ;
}

