// $Id$

/**
 * @file
 * Used for this module
 *
 * @date   01/29/2011
 * @author Yas Naoi, IT Manager, DOCOMO USA Labs (naoi at docomolabs-usa.com)
 *
 * @note
 * 
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 * 
 * DOCOMO Communication Laboratories USA, Inc. (DOCOMO USA Labs) licenses
 * this software under the GNU General Public License version 2 (GPL2)
 * for the software disclosed herein. For terms and conditions under the GPL2,
 * please refer to www.gnu.org.
 *
 * DOCOMO USA Labs may not be held liable for failing to maintain or support
 * the software, and DOCOMO USA Labs is not responsible for any damage or loss
 * experienced from using the software.
 *
 * Please contact the IT Manager at DOCOMO USA Labs for all issues
 * related to the software.
 *
 */

function switchOptions() {

  var fAction = document.getElementById("fAction").value;

  if (fAction == 1) {
    document.getElementById("fScript").style.display = "";
  }
  else{
    document.getElementById("fScript").style.display = "none";
  }
}