BASIC INFO
==========

- Provides common functionalites for cloud management.
- Cloud module is a heart of cloud package.  This requires at least one
  "cloud support module" such as Amazon EC2 module, OpenStack XCP or
  and so on.
- Once you install the 'Cloud' module be sure to grant the 'access dashboard' permission.
  Users with above permission can view all the running instances for the enabled sub-clouds. 
- Be sure to only grant 'administer cloud' permission to the cloud administrator.

e.g. (For an administrator)
- Turn on
  'administer cloud' and
  'access dashboard' permissions 
 
(For a generic users)
- Turn on
  'access dashboard' permission only

  
* After ver.0.83, the database schema has changed.  If you already install
  Clanavi 0.82 or less (before 01/29/2011), please uninstall and install modules.
     


SYSTEM REQUIREMENTS
===================
- PHP    5.2 or Higher
- MySQL  5.1 or Higher
- Drupal 6.x
- 512MB Memory: If you use Amazon EC2 module, the running host of this
  system requires more than 512MB memory to download a list of images
 (because it's huge amount of data for listing).


DIRECTORY STRUCTURE
===================

cloud
  +-modules (depends on Cloud module)(Cloud is a core module for Cloud package)
    +-cloud_activity_audit
    +-cloud_alerts
    x-cloud_auto_scaling
    +-cloud_billing
    +-cloud_cluster
    x-cloud_failover
    +-cloud_inputs
    +-cloud_pricing
    +-cloud_resource_allocator
    x-cloud_scaling_manager
    +-cloud_scripting
    +-cloud_server_templates

    x... Not released yet.


hook_* FOR SUBSIDIARY MODULES
=============================

- hook_cloud_set_info()
- hook_server_template()
- hook_get_all_instances()

* Example for Sub Cloud Family
  e.g. Amazon EC2, XCP, OpenStack nova and so on...

function hook_cloud_set_info() {
  return array(
    'cloud_name'         => 'generic_cloud_context', // Used for cloud ID
    'cloud_display_name' => 'Generic Cloud'        , // Uuser for display name in menu, etc. 
    'instance_types'     => array(
    )
  );
}

function hook_server_template($op, $params = array())

  $form = array( // make some form);
  return $form;
}


CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org
2010/11/09 ver.0.7  released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt