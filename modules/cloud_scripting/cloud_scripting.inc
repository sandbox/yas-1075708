<?php

/**
 * @file
 * Provides scripting feature such as bash, Perl and etc (Mainly bash scripts).
 * Works with Cloud and Server Templates module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

/**
 * Updated by yas   2011/02/14
 * Updated by yas   2011/02/11
 * Updated by yas   2011/02/10
 * Updated by yas   2011/02/08
 * Updated by yas   2011/02/02
 */

//scripting callback function
function _cloud_scripting_list($module) {
  
  return drupal_get_form('cloud_scripting_list') ;  
}

/**
 * Returns a form with listing of scripts
 * Build a form including table header and table body
 * @param $form_submit
 *        This is the form-submit submitted by list scripts form
 * @return return a form
 */
function cloud_scripting_list($form_submit) {

  drupal_add_js(cloud_get_module_base() . 'js/cloud.js', 'module');
  
  $column = 'name' ;

  $form['options'] = array(
    '#prefix' => '<div class="container-inline">',
    '#type'   => 'fieldset',
    '#suffix' => '</div>',
 // '#title' => t('Operations'), 
  );

  $options = array(
    t('Nickname' ) ,
    t('Type'     ) ,
//  t('Packages' ) ,
  );

  $filter     = cloud_get_filter_value( $form_submit , 'filter'   );
  $filter     = trim($filter);

  $filter_col = cloud_get_filter_value( $form_submit , 'operation');
  $filter_col = isset($filter_col) && $filter_col ? $filter_col : 0; // default: Script Name

  if ( $filter_col == 0 )  {
    $column  = 'Script Name' ;
    $sql_col = 'name'        ;
  }
  elseif ($filter_col == 1 ) {
    $column  = 'Type' ;
    $sql_col = 'type' ;
  }
  elseif ($filter_col == 2 ) {
    $column  = 'Packages' ;
    $sql_col = 'packages' ;
  }

  if ( isset($filter) ) {
    $query_args[] = $sql_col ;
    $query_args[] = $filter  ;
  }
  else {
    $filter = ' 1 ' ;
    $query_args[] = ' ' ;
  }

// asort($options);

  $script_count = _cloud_scripting_get_count_db() ;
  $filter_disabled = '' ;
  if ( $script_count < 2 ) {

    $filter_disabled = TRUE ;
  }
  
  $form['options']['label'    ] = array( '#type' => 'item'     , '#title'   => t('Filter'));
  $form['options']['operation'] = array( '#type' => 'select'   , '#options' => $options  , '#default_value' => $filter_col, '#disabled' => $filter_disabled );
  $form['options']['filter'   ] = array( '#type' => 'textfield', '#size'    => 40        , '#default_value' => $filter    , '#disabled' => $filter_disabled );
  $form['options']['submit'   ] = array( '#type' => 'submit'   , '#value'   => t('Apply')                                 , '#disabled' => $filter_disabled );
  if (user_access('create script') ) {
    $form['options']['script' ] = array( '#type' => 'submit'   , '#value' => t('Create'));
  }

  $form['header'] = array(

    '#type'  => 'value',  
    '#value' => array(
      array('data' => t('Nickname'), 'field' => 'name'                    ,
                                     'sort'  => 'asc'                     ,
                                     'class' => 'nickname-column'       ) ,
      array('data' => t('Type'    ), 'field' => 'type'                  ) ,
      array('data' => t('Packages'), 'field' => 'packages'              ) ,
      array('data' => t('Inputs'  ), 'field' => 'cloud_inputs'          ) ,
    //array('data' => t('Status'  ), 'field' => 'status'                ) ,
      array('data' => t('Created' ), 'field' => 'created'               ) ,
      array('data' => t('Updated' ), 'field' => 'updated'               ) ,
      array('data' => t('Action'  ), 'class' => 'action-column'         ) ,
    )
  );

  $query  = _cloud_scripting_get_scripts() ;
  $query .= tablesort_sql( $form['header']['#value'] ) ;

  $result = pager_query( $query , CLOUD_SCRIPTING_PAGER_LIMIT, 0, NULL, $query_args );

  $destination        = drupal_get_destination();
  $scripting_options  = cloud_server_templates_get_scripting_options();
  while ($scripts_obj = db_fetch_object($result)) {

    $form['Name'    ][$scripts_obj->id] = array( array('#value' => l( $scripts_obj->name , CLOUD_SCRIPTING_PATH . '/describe/' . $scripts_obj->id . '/info',  array('query' => 'id=' . urlencode($scripts_obj->id)) ) ) );//,  array(),   'id=' . urlencode($scripts_obj->id) ,  NULL,  FALSE,  TRUE ) ) );
    $form['hdnName' ][$scripts_obj->id] = array('#type' => 'hidden', '#value' => addslashes($scripts_obj->name) );
    $form['Type'    ][$scripts_obj->id] = array( array('#value' => check_plain( $scripting_options['CLOUD_SCRIPTING_TYPE_OPTIONS'][$scripts_obj->type] ) ) );
    $form['Packages'][$scripts_obj->id] = array( array('#value' => check_plain( $scripts_obj->packages ) ) );
    $form['Inputs'  ][$scripts_obj->id] = array( array('#value' => check_plain( $scripts_obj->inputs   ) ) );
  //$form['Status'  ][$scripts_obj->id] = array( array('#value' => t( (($scripts_obj->status == 1) ? 'Active' : 'Inactive') ) ) ) ;
    $form['Created' ][$scripts_obj->id] = array('#value' => format_date(strtotime($scripts_obj->created), 'small') ) ;
    $form['Updated' ][$scripts_obj->id] = array('#value' => format_date(strtotime($scripts_obj->updated), 'small') ) ;
  }

  $form['pager'    ] = array('#value' => theme('pager', NULL, CLOUD_SCRIPTING_PAGER_LIMIT, 0));
  $form['#redirect'] = FALSE;

  return $form;
}


function theme_cloud_scripting_list($form) {

  $output = drupal_render($form['options']);
//$form['Name'] = !empty($form['Name']) ? $form['Name'] : '';
  $form['pager']['#value'] = !empty($form['pager']['#value']) ? $form['pager']['#value'] : '';
  $rows = array();
  if ( !empty($form['Name']) ) {

    foreach (element_children($form['Name']) as $key) {
  
      $row = array(
        array('data' => drupal_render($form['Name'][$key]), 'class' => 'nickname-column'),
        drupal_render($form['Type'    ][$key]),
        drupal_render($form['Packages'][$key]),
        drupal_render($form['Inputs'  ][$key]),
    //  drupal_render($form['Status'  ][$key]),
        drupal_render($form['Created' ][$key]),
        drupal_render($form['Updated' ][$key]),
      );
  
      $form['hdnName'][$key]['#value'] = !empty($form['hdnName'][$key]['#value']) ? $form['hdnName'][$key]['#value'] : '';    
      $prop['onclick'] = cloud_get_messagebox('Are you sure you want to delete the Script "' . $form['hdnName'][$key]['#value'] . '" ?') ;
  
      //print user_access('delete script') . 'here';die;
      $action_data = '' ;
      if (user_access('delete script')) {
  
        $action_data .= cloud_display_action(
                          'images/icon_delete',
                          t('Delete'),
                          CLOUD_SCRIPTING_PATH . '/' . urlencode($key) . '/delete' ,
                          array('query' => 'id=' . urlencode($key), 'html' => TRUE), $prop['onclick']
                        );
      }
      if (user_access('edit script')) {
        $action_data .= cloud_display_action(
                          'images/icon_clear',
                          t('Edit'),
                          CLOUD_SCRIPTING_PATH  . '/' . urlencode($key) . '/edit' ,
                          array('query' => 'id=' . urlencode($key), 'html' => TRUE)
                        );
      }
  
      // elseif (user_access('delete own template'))
      //{
      //if ( in_array($form['KeyName'][$key]['#value'], $user_keys_all ) )
      //{
      //$action_data .= cloud_display_action( 'images/icon_clear', t('Edit') , CLOUD_SCRIPTING_PATH . '/create', 'id=' . urlencode($key)  );
      //}
      //}
      
      $row[]  =  array('data' => $action_data, 'class' => 'action-column' );
      $rows[] = $row;
    }
  }
  //print_r($form);die;
  $output .= theme('table', $form['header']['#value'], $rows);

  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $reload_link = l( '- Refresh Page -' ,
                    CLOUD_SCRIPTING_PATH . '/getdata',
                    array( 'query' => 'src=' . CLOUD_SCRIPTING_PATH . '/list' )
                 );

  $ref_link = array(
    '#type'   => 'item',
    '#prefix' => '<div id="link_reload" align="right">',
    '#suffix' => '</div>',
    '#value'  => $reload_link
  );

  //$output .= drupal_render($ref_link);
  $output .= drupal_render($form);

  return $output;
}


function cloud_scripting_desc_info($form_submit='', $script_id='') {

  if (empty($script_id) || strlen($script_id) == 0  )  {
    drupal_goto( CLOUD_SCRIPTING_PATH . '/list') ;

    return;
  }

  if (user_access('view script') === FALSE ) {

      drupal_set_message(t('You are not authorized to access this page.'));
      drupal_goto( CLOUD_SCRIPTING_PATH . '/list' ) ;
      return ;
  }
  
  variable_set( '_desc_info', $script_id );

  $query      = _cloud_scripting_get_scripts() ;
  $query_args = array(
    'id',
    $script_id,
  );

  $result = db_query( $query, $query_args );
  $count  = 0 ;
//print_r($result);die;

  $form['fieldset_script_info'] = array('#type' => 'fieldset', '#title' => t('Details'));
  
  while ($key = db_fetch_object($result)) {

 // $form['Instance_Name'        ]['Name_label'] = array('#type' => 'item', '#title' => t('Nickname')  );
 // $form['Instance_Name_details']               = array('#prefix' => '<div class="container-inline">', '#suffix' => '</div>');
 // $form['Instance_Name_details']['Name_text' ] =  array( '#type' => 'textfield', '#value' => t( $key->instance_nickname) ) ;
 // $form['Instance_Name_details']['submit'    ] = array('#type' => 'submit', '#value' => t('Update'));
 // $form['idVal'] = array( array('#value' => t( isset_variable($key->id) ) ) );

    $script_name = $key->name;
    $form['name'        ]['name_label'       ] = array('#type' => 'item', '#title' => t('Script Name')      ) ;
    $form['name'        ]['name_text'        ] = array( array('#value' => check_plain( $key->name)        ) ) ;

    $form['type'        ]['type_label'       ] = array('#type' => 'item', '#title' => t('Script Type')      ) ;
    $form['type'        ]['type_text'        ] = array( array('#value' => t( $key->type)                  ) ) ;

    $form['description' ]['description_label'] = array('#type' => 'item', '#title' => t('Description')      ) ;
    $form['description' ]['description_text' ] = array( array('#value' => check_plain( $key->description) ) ) ;

    $form['packages'    ]['packages_label'   ] = array('#type' => 'item', '#title' => t('Packages')         ) ;
    $form['packages'    ]['packages_text'    ] = array( array('#value' => check_plain( $key->packages   ) ) ) ;

    $form['cloud_inputs']['inputs_label'     ] = array('#type' => 'item', '#title' => t('Inputs')           ) ;
    $form['cloud_inputs']['inputs_text'      ] = array( array('#value' => check_plain( $key->inputs     ) ) ) ;

    $form['script'      ]['script_label'     ] = array('#type' => 'item', '#title' => t('Script')           ) ;
    $form['script'      ]['script_text'      ] = array('#type' => 'textarea', '#value' => ( ($key->script_template)), '#disabled' => 1 ) ;

    $form['created'     ]['created_label'    ] = array('#type' => 'item', '#title' => t('Created')          ) ;
    $form['created'     ]['created_text'     ] = array( array('#value' => format_date(strtotime($key->created), 'small') ) );
    
    $form['updated'     ]['updated_label'    ] = array('#type' => 'item', '#title' => t('Updated')       ) ;
    $form['updated'     ]['updated_text'     ] = array( array('#value' => format_date(strtotime($key->updated), 'small') ) );
    
    $form['Script'      ]['id'               ] = array('#type' => 'hidden', '#value' => $key->id            ) ;

    $count++ ;
  }

  //$form['name']['action'] = array('#type' => 'item', '#title' => t('Action')  );
  if ( $count == 0 ) {

    drupal_goto( MODULE_PATH . '/list') ;

    return ;
  }

  $form['submit_buttons'] = array(
    '#type'   => 'fieldset',
    '#prefix' => '<span class="clear"></span><div class="container-inline"><div class="buttons">',
    '#suffix' => '</div></div>',
  );
  
  if (user_access('edit script')) {

    // Edit Button
    $location = check_url(url(CLOUD_SCRIPTING_PATH . '/' . $script_id . '/edit')) . '&id=' . $script_id;
    $action = "document.forms[0].action='" . $location . "';";
    $onclick = $action;
    $form['submit_buttons']['Edit'] = array( '#type' => 'submit', '#value' => t('Edit'), '#attributes' => array('onclick' => $onclick) );
  }
  
  if (user_access('delete script')) {

    // Delete Button
    $confirm_msg = cloud_get_messagebox('Are you sure you want to delete the Script "' . $script_name . '" ?') ;
    $location    = check_url(url(CLOUD_SCRIPTING_PATH . '/' . $script_id . '/delete')) . '&id=' . $script_id;
    $action      = "document.forms[0].action='" . $location . "';";
    $onclick     = $action . $confirm_msg;
    $form['submit_buttons']['Delete'] = array( '#type' => 'submit', '#value' => t('Delete'), '#attributes' => array('onclick' => $onclick) );
  }

  // List Scripts Button
  $location = check_url(url(CLOUD_SCRIPTING_PATH . '/list'));
  $action   = "document.forms[0].action='" . $location . "';";
  $onclick  = $action;
  $form['submit_buttons']['List Scripts'] = array(
    '#type'       => 'submit',
    '#value'      => t('List Scripts'),
    '#attributes' => array('onclick' => $onclick)
  );
  
  return $form ;
}


function theme_cloud_scripting_desc_info($form) {

  $rows = array(

    array(
      drupal_render($form['name']['name_label']),
      drupal_render($form['name']['name_text' ]),
    ),
    array(
      drupal_render($form['type']['type_label']),
      drupal_render($form['type']['type_text' ]),
    ),
    array(
      drupal_render($form['description']['description_label']),
      drupal_render($form['description']['description_text' ]),
    ),
    array(
      drupal_render($form['packages']['packages_label'  ]),
      drupal_render($form['packages']['packages_text'   ]),
    ),
    array(
      drupal_render($form['cloud_inputs']['inputs_label']),
      drupal_render($form['cloud_inputs']['inputs_text' ]),
    ),
    array(
      drupal_render($form['script']['script_label'      ]),
      drupal_render($form['script']                      )
    . drupal_render($form['script']['script_text'       ]),
    ),
    array(
      drupal_render($form['created']['created_label'    ]),
      drupal_render($form['created']['created_text'     ]),
    ),
    array(
      drupal_render($form['updated']['updated_label'    ]),
      drupal_render($form['updated']['updated_text'     ]),
    ),
  );

  $table = theme('table', NULL, $rows );
  $form['fieldset_script_info']['#children'] = $table;

//cf. Waning by Coder module: Potential problem: when FAPI element '#type' is set to 'markup' (default), '#value' only accepts filtered text, be sure to use check_plain(), filter_xss() or similar to ensure your $variable is fully sanitized.
//$form['fieldset_script_info']['list'] = array('#type' => 'markup', '#value' => $table);

  $output  = drupal_render($form['submit_buttons'      ]) ;
  $output .= drupal_render($form['fieldset_script_info']) ;
  $output .= drupal_render($form['id'                  ]) ;
  $output .= drupal_render($form                        ) ;
  
  return $output ;
}


function cloud_scripting_desc_script($form_submit='', $script_id='') {

  if (empty($script_id) || strlen($script_id) == 0  )  {

    drupal_goto( CLOUD_SCRIPTING_PATH . '/list') ;

    return;
  }

  variable_set( $user->uid . '_desc_info' , $script_id );

  $query      = _cloud_scripting_get_scripts() ;
  $query_args = array(
    'id'      ,
    $script_id,
  );

  $result = db_query( $query, $query_args );
  $count  = 0 ;
  //print_r($result);die;
  while ($key = db_fetch_object($result)) {

  //$form['Instance_Name'        ]['Name_label'] = array('#type' => 'item', '#title' => t('Nickname')  );
  //$form['Instance_Name_details'] = array('#prefix' => '<div class="container-inline">', '#suffix' => '</div>');
  //$form['Instance_Name_details']['Name_text' ] =  array( '#type' => 'textfield', '#value' => t( $key->instance_nickname) ) ;
  //$form['Instance_Name_details']['submit'    ] = array('#type' => 'submit', '#value' => t('Update'));

    $form['packages'    ]['packages_label'] = array('#type' => 'item', '#title' => t('Packages')         ) ;
    $form['packages'    ]['packages_text' ] = array( array('#value' => check_plain( $key->packages) ) ) ;

    $form['cloud_inputs']['inputs_label'  ] = array('#type' => 'item', '#title' => t('Inputs')           ) ;
    $form['cloud_inputs']['inputs_text'   ] = array( array('#value' => check_plain( $key->inputs)   ) ) ;

    $form['script'      ]['script_label'  ] = array('#type' => 'item', '#title' => t('Script')           ) ;
    $form['script'      ]['script_text'   ] = array('#type' => 'textarea', '#value' => check_plain( $key->script_template), '#disabled' => 1 ) ;

    //$form['Instance_Id']['instance_id'] = array('#type' => 'hidden', '#value' => $key->instance_id  ) ;
    //*/
    $form['Script'      ]['id'            ] = array('#type' => 'hidden', '#value' => $key->id         ) ;

    $count++ ;
  }

  if ( $count == 0 ) {

    drupal_goto( MODULE_PATH . '/list') ;

    return ;
  }

  return $form ;
}


function theme_cloud_scripting_desc_script($form) {

  $rows = array(

    array(
      drupal_render($form['packages']['packages_label'  ]),
      drupal_render($form['packages']['packages_text'   ]),
    ),
    array(
      drupal_render($form['cloud_inputs']['inputs_label']),
      drupal_render($form['cloud_inputs']['inputs_text' ]),
    ),
    array(
      drupal_render($form['script']['script_label'      ]),
      drupal_render($form['script']                      )
    . drupal_render($form['script']['script_text'       ]),
    ),
  );
  
  $output  = theme('table', NULL, $rows ) ;
  $output .= drupal_render( $form['id'] ) ;
  $output .= drupal_render($form);

  return $output ;
}


function cloud_scripting_list_submit($form_id, $form_values) {

  $form_values = $form_values['values'];
  if ( $form_values['op'] == t('Create')) {

    return drupal_goto( CLOUD_SCRIPTING_PATH . '/create') ;
  }
}


function cloud_scripting_delete($script_id = '') {

  _cloud_scripting_delete($script_id) ;

  drupal_set_message(t('Script has been deleted successfully.'));
  drupal_goto(CLOUD_SCRIPTING_PATH . '/list') ;
}


function cloud_scripting_new($form_submit='', $script_id='') {

  $scripting_options = cloud_server_templates_get_scripting_options();
  $action = t('Add');
  drupal_set_title(t('Add Script'));

  //$form['launch'] = array( '#type' => 'submit', '#value' => t('Launch') );
  //$image_options = _cloud_server_templates_get_images_for_new_launch('machine') ;

  $form['fieldset_script_info'      ] = array('#type' => 'fieldset', /* '#title' => t('Script Info') */ );
  
  $form['name'           ]['name_label'           ] = array('#type' => 'item', '#title' => t('Name')       );
  $form['name'           ]['name_text'            ] = array('#type' => 'textfield' );

  $form['description'    ]['description_label'    ] = array('#type' => 'item', '#title' => t('Description') );
  $form['description'    ]['description_text'     ] = array('#type' => 'textarea' );

  $form['script_type'    ]['script_type_label'    ] = array(
    '#type' => 'item'        ,
    '#title' => t('Script Type') ,
    '#description' => t('The field value takes Boot, Operational, and Termination<br />when a script should be executed.<br />Currently only "booting" time execution is supported.'),
  );
  $form['script_type'    ]['script_type_select'   ] = array('#type' => 'select', '#options' => $scripting_options['CLOUD_SCRIPTING_TYPE_OPTIONS']);

  $form['packages'       ]['packages_label'       ] = array(
    '#type'      => 'item'    ,
    '#title'     => t('Packages'),
    '#description' => 'Curretnly not used.',
  );
  $form['packages'       ]['packages_text'        ] = array('#type' => 'textfield' );

  $form['cloud_inputs'   ]['inputs_label'         ] = array(
    '#type'        => 'item'  ,
    '#title'       => t('Inputs'),
    '#description' => t('Use the inputs as variables under Script code below.<br />Enter comma separated values.  (e.g. MESSAGE, OUTPUT_FILE)<br />The input parameters are configurable;<br />the actual parameters can be settled in a template.'),
  );
  $form['cloud_inputs'   ]['inputs_text'          ] = array('#type' => 'textfield' );

  $form['script_template']['script_template_label'] = array(
    '#type'  => 'item'  ,
    '#title' => t('Script'),
    '#description' => t('Put a script template here.  (e.g. echo MESSAGE > OUTPUT_FILE)'),
  );
  $form['script_template']['script_template_text' ] = array('#type' => 'textarea' );

  if ($script_id) {

    drupal_set_title(t('Edit Script'));
    $query      = _cloud_scripting_get_scripts() ;
    $query_args = array(
      'id'      ,
      $script_id,
    );
    $result       = db_query( $query, $query_args );
    $count        = 0 ;

    $scripts_obj = db_fetch_object($result);
    $action = t('Edit');
    //print_r($alert_object->name);die;
    $form['script_id'      ] = array('#type' => 'hidden', '#value' => $script_id );
    $form['inputs_text_old'      ] = array('#type' => 'hidden', '#value' => $scripts_obj->inputs );
    $form['name'           ]['name_text'           ]['#default_value'] = isset_variable($scripts_obj->name             );
    $form['description'    ]['description_text'    ]['#default_value'] = isset_variable($scripts_obj->description      );
    $form['script_type'    ]['script_type_select'  ]['#default_value'] = isset_variable($scripts_obj->type             );
    $form['packages'       ]['packages_text'       ]['#default_value'] = isset_variable($scripts_obj->packages         );
    $form['cloud_inputs'   ]['inputs_text'         ]['#default_value'] = isset_variable($scripts_obj->inputs           );
    $form['script_template']['script_template_text']['#default_value'] = isset_variable(($scripts_obj->script_template));
    //print 'here';
  }

  $form['submit_buttons'] = array(
    '#type'   => 'fieldset',  
    '#prefix' => '<span class="clear"></span><div class="container-inline"><div class="buttons">',
    '#suffix' => '</div></div>'
  );

  $form['submit_buttons'][$action ] = array('#type' => 'submit', '#value' => t($action ) );
  $form['submit_buttons']['Cancel'] = array('#type' => 'submit', '#value' => t('Cancel') );

  return $form ;
}


function theme_cloud_scripting_new($form) {

  $rows = array(

    array(
      drupal_render($form['name'          ]['name_label'           ] ),
      drupal_render($form['name'          ]['name_text'            ] ),
    ),
    array(
      drupal_render($form['description'   ]['description_label'    ] ),
      drupal_render($form['description'   ]['description_text'     ] ),
    ),
    array(
      drupal_render($form['script_type'   ]['script_type_label'    ] ),
      drupal_render($form['script_type'   ]['script_type_select'   ] ),
    ),
    array(
      drupal_render($form['packages'       ]['packages_label'       ] ),
      drupal_render($form['packages'       ]['packages_text'        ] ),
    ),
    array(
      drupal_render($form['cloud_inputs'   ]['inputs_label'         ] ),
      drupal_render($form['cloud_inputs'   ]['inputs_text'          ] ),
    ),
    array(
      drupal_render($form['script_template']['script_template_label'] ),
      drupal_render($form['script_template']['script_template_text' ] ),
    ),
  );

  $table = theme('table', NULL, $rows );
  $form['fieldset_script_info']['#children'] = $table;

//cf. Waning by Coder module: Potential problem: when FAPI element '#type' is set to 'markup' (default), '#value' only accepts filtered text, be sure to use check_plain(), filter_xss() or similar to ensure your $variable is fully sanitized.
//$form['fieldset_script_info']['list'] = array('#type' => 'markup', '#value' => $table);
  
  $output  = drupal_render($form['launch']);
  $output .= drupal_render($form['fieldset_script_info']);
  $output .= drupal_render($form['submit_buttons']);
  $output .= drupal_render($form);

  return $output;
}


function cloud_scripting_new_validate($form_id, $form_values) {

  $form_values = $form_values['values'];
  if ( $form_values['op'] == t('Add' )
  ||   $form_values['op'] == t('Edit') ) {

    //$count =  $form_values['Count_text'] ;
    if ( empty($form_values['name_text']) ) {
        
      form_set_error('', t('You must enter valid Script Name'));
        
    }
    else {

      $query = _cloud_scripting_get_script_query();
      $query_args = array(
        'name'                   ,
        $form_values['name_text'],
      );
      $result = db_query( $query, $query_args );
      $t_count = !empty($result->num_rows) ? $result->num_rows : 0;//db_num_rows($result);
        
      if ($t_count>0) {

        $db_obj    = db_fetch_object($result);
        $script_id = $db_obj->id;

        if ($script_id != $form_values['script_id']) {
          form_set_error('', t('Name is already used by a Script.'));
        }
      }
    }

    if ( empty($form_values['script_template_text']) ) {
        
      form_set_error('', t('You must enter valid script'));
    }
  }
}


function cloud_scripting_new_submit($form_id, $form_values) {

  $form_values = $form_values['values'];

  if ( $form_values['op'] == t('Cancel') ) {

    drupal_goto(CLOUD_SCRIPTING_PATH . '/list') ;

    return ;
  }
  elseif ($form_values['op'] == t('Add')) {
     
    _cloud_scripting_insert_script($form_values) ;
    //print 'here';die;
    drupal_set_message(t('Script has been saved.'));
    drupal_goto(CLOUD_SCRIPTING_PATH . '/list') ;
  }
  elseif ($form_values['op'] == t('Edit')) {

    $script_id = $form_values['script_id'] ;
    _cloud_scripting_update_script($form_values , $script_id ) ;
    drupal_set_message(t('Script has been saved.'));
    drupal_goto(CLOUD_SCRIPTING_PATH . '/list') ;
  }
}


function _cloud_scripting_update_instance_status($dns) {
  
  $instance_id = _get_instance_by_dns_from_db($dns) ;
  
  if ( _cloud_failover_is_endbled_check_by_instance_id($instance_id) > 0  ) {

    $result_script_status = _instance_status_update($instance_id, CLOUD_INSTANCE_STATUS_BOOTING ) ;
  }
  else {

    $scripts_arr  = _cloud_scripting_get_scripts_for_instance($instance_id) ;
    // $scripts_arr = array();
    if (sizeof($scripts_arr) == 0 )  {

      if ( _cloud_scripting_check_is_executed($instance_id) > 0 ) {
        // Scripts are to be executed
        $result_script_status = _instance_status_update($instance_id, CLOUD_INSTANCE_STATUS_BOOTING ) ;
      }
      else {
        // No scripts present
        $result_script_status = _instance_status_update($instance_id, CLOUD_INSTANCE_STATUS_OPERATIONAL ) ;
      }
    }
    else {
      // Scripts are to be executed
      $result_script_status = _instance_status_update($instance_id, CLOUD_INSTANCE_STATUS_BOOTING ) ;
    }
  }  
}


function _cloud_scripting_check_is_executed($instance_id) {

  $check_scripts_query = " SELECT COUNT(*) AS script_cnt FROM ( SELECT ds.template_id AS tid FROM cloud_cluster_servers ds
                    LEFT JOIN {" . CLOUD_SERVER_TEMPLATES_TABLE . "} st
                    ON ds.template_id=st.template_id
                    WHERE ds.instance_id='%s'
                  ) tmp_tb 
                  LEFT JOIN {" . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . "} sts 
                  ON tmp_tb.tid=sts.server_template_id 
                  AND sts.deleted='0' " ;
  
  $check_scripts_query_args = array( 
    $instance_id,
  ) ;
  $result = db_query($check_scripts_query , $check_scripts_query_args );      
  
  $script_cnt = 0 ;
  while ($row = db_fetch_object($result)) {
  
    $script_cnt = $row->script_cnt ;
  }
  
  if ($script_cnt > 0 )
    return $script_cnt ;

  $check_scripts_array_query = "SELECT COUNT(*) AS script_cnt
                                FROM
                                (
                                  SELECT arrayid FROM 
                                  ( SELECT tas.arrayid as arrayid FROM {" . CLOUD_AUTO_SCALING_ARRAY_LAUNCH_TABLE . "} tas
                                    WHERE tas.instance_id='%s' ) vas
                                  LEFT JOIN {" . INSTANCE_ARRAY_TABLE                 . "} sia on vas.arrayid = sia.id 
                                  LEFT JOIN {" . CLOUD_SERVER_TEMPLATES_TABLE         . "} st on sia.server_template_id=st.template_id
                                  LEFT JOIN {" . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . "} sts on st.template_id=sts.server_template_id AND sts.deleted='0' 
                                ) cnt_tbl " ;
  $check_scripts_array_query_args = array(
    $instance_id,
  ) ;
  $result_arr = db_query($check_scripts_array_query, $check_scripts_array_query_args );

  while ($row_arr = db_fetch_object($result_arr)) {
  
    $script_cnt = $row_arr->script_cnt ;
  }  
  
  return $script_cnt ;
}


function _cloud_scripting_getdata($src = '') {

  if ( $src == NULL
  ||   empty($src) ) {

    drupal_goto( CLOUD_SCRIPTING_PATH . '/list' ) ;
  }
  else {

    drupal_goto( $src ) ;
  }

  return;
}
