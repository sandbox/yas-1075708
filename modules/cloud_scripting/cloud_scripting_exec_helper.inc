<?php

/**
 * @file
 * Provides scripting feature such as bash, Perl and etc (Mainly bash scripts).
 * Works with Cloud and Server Templates module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

module_load_include('inc', 'cloud', 'cloud_constants');
module_load_include('inc', 'cloud', 'cloud'          );

function _cloud_scripting_exec_script_on_instances($cloud_context) {
  
  $output_dir = file_create_path(AWS_EC2_LIB_TEMP_DIR);
  if (file_exists($output_dir) === FALSE ) {  
    mkdir($output_dir, 0777, TRUE ); 
  }
  
  //Check the instances for Executed scripts...
  _cloud_scripting_check_instance_script_status($cloud_context);
  
  $execute_script_array = _cloud_scripting_get_execute_ready_scripts($cloud_context);
  $row_count = count($execute_script_array);

  for ($row = 0 ; $row < $row_count ; $row ++) {

    $instance_id = $execute_script_array[$row]['instance_id'];
    $instance    = cloud_get_instance($cloud_context, $instance_id);
    
    if ($instance) {
      _cloud_scripting_exec_scripts_on_instance($cloud_context, $instance, $instance_id, $execute_script_array[$row]);

    }
    else{
      //Do Nothing because there is no running instance.
    }
  }

  return;
}

/**
 * @param <type> $cloud_context 
 */
function _cloud_scripting_check_instance_script_status($cloud_context) {
    
  $execute_script_array = _cloud_scripting_get_under_progress_scripts($cloud_context);
  $instance_array       = array();
  $row_count            = count($execute_script_array);  
  
  for ($row = 0 ; $row < $row_count ; $row ++) {

    $instance_id = $execute_script_array[$row]['instance_id'];
    $script_id   = $execute_script_array[$row]['script_id'  ];
    
    $file = CLOUD_SCRIPTING_TEMP_DIRECTORY . $instance_id . CLOUD_PATH_SEPARATOR . $script_id;
    
    if (isset($instance_array[$instance_id])) {
      $instance = $instance_array[$instance_id];
    }
    else{
      $instance = cloud_get_instance($cloud_context, $instance_id);
      if ($instance === FALSE ) {
            continue ; // Skip this instance
      }
      $instance_array[$instance_id] = $instance;
    }
    
    $key_name = $instance['key_name'];
    $perm_file = _cloud_scripting_get_perm_file($instance_id, $key_name, $cloud_context);

    $instance_ip = $instance['dns_name'];
    
    if ( _cloud_scripting_is_script_execution_done($cloud_context, $instance_ip, $instance_id, $perm_file, $script_id) == '1') {

      $script_id = '\'' . $script_id . '\'';
       _cloud_scripting_update_scripts_status($instance_id, $script_id, CLOUD_SCRIPTING_SCRIPT_EXECUTED);
    }
    else {
      continue;
    }        
  }
}

function _cloud_scripting_exec_query_on_instance_without_cron($instance_id, $script_id) {
  //cloud_log_to_db("Execute Script on Instance without Cron.",  '');
  //_scripting_exec_scripts_on_instance($instance_id, $script_id);
  //return;
}

/**
 *
 * @param <type> CLOUD_SCRIPTING_TMP_LOG_FILE_PATH
 * @param <type> $instance_id
 */
function _cloud_scripting_exec_scripts_on_instance($cloud_context, $instance, $instance_id, $instance_script_params ) {

  $script_id     = $instance_script_params['script_id'];
  $script_params = _cloud_scripting_get_script($script_id);

  $ssh_user_name = aws_ec2_lib_get_ssh_user( $cloud_context , $instance_id ) ;
  
  //Temoprarily Commented Out because we are executing Script One by One...
  //$command = 'rm -fr '. CLOUD_SCRIPTING_TEMP_DIRECTORY . $instance_id; //Command for removing already existing instance script folder
  //exec($command, $output, $retval);

  $dir     = CLOUD_SCRIPTING_TEMP_DIRECTORY . $instance_id;
  $command = 'mkdir ' . $dir;

  if ( _cloud_scripting_check_local_directory_exist($dir) == CLOUD_SCRIPTING_DIRECTORY_NOT_EXIST) {
      
    exec($command, $output, $retval); //command for creating new empty folder
  }
  
  $key_name = $instance['key_name'];

  $script_files = '';
  $script_ids   = '';
  $script_dest_path = '' ;
  
  $perm_file = _cloud_scripting_create_temp_permission_file($cloud_context,  $instance_id, $key_name);
  //This for loop will create files for scripts
  //for ($row_counter = 0; $row_counter < count($script_params); $row_counter++) {
  //  $sid = $script_params["script_id"];
  $cluster_id  = $instance_script_params['cluster_id' ];
  $template_id = $instance_script_params['template_id'];
  $instance_ip = $instance['dns_name'];
  $script_ids .= '\'' . $script_id . '\'';
  //        if ($row_counter != (count($script_params) -1 ))
  //            $script_ids .=', ';
  $script = $script_params['script_template'];

  $script        = str_replace("\r\n",  "\n" , $script, $count);
  $script_files .= _cloud_scripting_create_temp_script_files($instance_id, $script_id, $script, $instance_ip, $cluster_id, $cloud_context, $template_id);
  $script_files .= ' ';
  $script_dest_path .= '/tmp/' . $instance_id . CLOUD_PATH_SEPARATOR  . $script_id . ' ' ;
  //}

  //Here the main logic starts for executing Script on Remote Machine
  if (!empty($script_ids)) {

      if ( _cloud_scripting_is_ssh_enabled($cloud_context, $instance_ip, $instance_id, $perm_file) == '0' ) {

          // Instance is not ready for ssh
          // Do not execute script right now 
          return ;
      }

      $log_file_name     = CLOUD_SCRIPTING_TMP_LOG_FILE_PATH . $instance_id . '.' . time() . '.log'  ;
      _cloud_scripting_update_scripts_status($instance_id, $script_ids,  CLOUD_SCRIPTING_SCRIPT_UNDER_EXECUTION);//Change Script Status to Under Progress

      $tmp_args = ' ' . $instance_ip . ' ' . $perm_file . ' ' . $log_file_name . ' ' . "\"" . $script_files . "\"" .
                  ' ' . $instance_id . ' ' . $script_id . ' ' . CLOUD_SSH_PATH . ' ' . CLOUD_SCP_PATH .
                  ' ' . CLOUD_SCRIPTING_TEMP_DIRECTORY  . ' ' . CLOUD_SCRIPTING_OUTPUT_LOG_FILE . ' ' . $ssh_user_name .
                  ' ' .  "\"" . $script_dest_path . "\""  ;
      
      $command     = CLOUD_PHP_PATH . ' -f ' . dirname(__FILE__) . CLOUD_PATH_SEPARATOR . 'cloud_scripting_exec.inc ' . $tmp_args . ' >> ' . CLOUD_SCRIPTING_TMP_LOG_FILE_PATH . 'cloud.log'  ;

      exec( $command , $output , $retval);
    }

  return;
}


function _cloud_scripting_create_temp_permission_file($cloud_context, $instance_id, $key_name) {

  $key       = cloud_get_ssh_key($cloud_context, $key_name);
  $perm_file = CLOUD_SCRIPTING_TEMP_DIRECTORY . $instance_id . CLOUD_PATH_SEPARATOR . $key_name;
  
  if ( _cloud_scripting_check_local_file_exists($perm_file) == CLOUD_SCRIPTING_FILE_NOT_EXIST) {

    $fh = fopen($perm_file, 'w') ;
    if ( $fh == FALSE) {

      drupal_set_message( t('Unable to create the key file. Contact administrator'), 'error' );
      return ;
    }

    fwrite($fh, $key);
    fclose($fh);

    $command = "chmod 400 $perm_file" ; // Make the file readonly
    exec( $command, $output, $retval);
  }

  return $perm_file;
}


/**
 * This function will do the following.
 * a) Remove any magic words
 * b) It will check if cluster id is there then we will use Keywords related to cluster
 * c) If clusterId is blank then we will use Keywords realted to templateId
 * @param <type> $instance_id
 * @param <type> $script_id
 * @param <type> $script
 * @param <type> $instance_ip
 * @param <type> $cluster_id
 * @param <type> $cloud
 * @param <type> $template_id
 * @return <type>
 */
function _cloud_scripting_create_temp_script_files($instance_id, $script_id, $script, $instance_ip, $cluster_id, $cloud, $template_id) {

  //$key_files = '';

  $input_params_flag_set = FALSE ;
  $org_script            = $script ;

  //Temporarily Commented. Will be Uncommented after Inputs is Done

  //  if (!empty($cluster_id)) {
  //    $query =  _cloud_scripting_get_cluster_input_query($cluster_id, $script_id);
  //    $db_result = db_query( $query );
  //    while ($input = db_fetch_object($db_result)) {
  //
  //      $input_keyword = $input->input_parameter;
  //      $input_keyword_val = $input->cluster_value;
  //      $rem_instance_id = $input->value_of_instance_id;
  //      $type = $input->param_type;
  //      if ($type == 'text') {
  //
  //        if ( empty($input_keyword_val) == TRUE ) {
  //          $input_params_flag_set = FALSE ;
  //          $script = $org_script ;
  //          break ;
  //        }
  //        $input_params_flag_set = TRUE ;
  //
  //        $script =  str_replace  (trim($input_keyword), $input_keyword_val, $script) ;
  //      }
  //      elseif ($type == 'env') {
  //        $inst_value = _cloud_scripting_get_value_from_instance_table($input_keyword_val, $rem_instance_id, $cloud);
  //        $script =  str_replace  (trim($input_keyword), $inst_value, $script) ;
  //      }
  //      elseif ($type == 'key') {
  //          //This was for Auto Scaling. Ommitted from next release.
  ////        $loadbalancer_permfile = _cloud_scripting_create_temp_permission_file($instance_id, $input_keyword_val);
  ////        $script =  str_replace  (trim($input_keyword), $loadbalancer_permfile, $script) ;
  ////        $key_files .= $loadbalancer_permfile . " ";
  //      }
  //    }
  //  }

  $script = _scripting_update_inputs($template_id , $script_id , $script);

  $script_file = CLOUD_SCRIPTING_TEMP_DIRECTORY . $instance_id . CLOUD_PATH_SEPARATOR . $script_id;
  $fh = fopen($script_file, 'w');
  if ( $fh == FALSE) {
    drupal_set_message( t('Unable to create the script file.'), 'error' );
    
    return ;
  }

  fwrite($fh, $script );
  // The path passed is the destination temporary folder path.
  // The path should be /tmp
  fwrite($fh, _scripting_get_magic_statement( '/tmp/' . $instance_id . CLOUD_PATH_SEPARATOR . $script_id , $script ));
  fclose($fh);

  return $script_file;
}

/**
 *
 * @param <type> $template_id
 * @param <type> $script
 * @return <type> 
 */
function _scripting_update_inputs($template_id , $script_id , $script) {
  
  if (! empty($template_id)) {

    $inputs    = _cloud_scripting_get_script_inputs($template_id, $script_id);
    $row_count = count($inputs);
    
    for ($row = 0 ; $row < $row_count ; $row ++) {
       
      $keyword = $inputs[$row]['input_parameter'];
      $val     = $inputs[$row]['template_value'];
      $type    = $inputs[$row]['param_type'];
      
      if (trim($type) == 'text') {
          
        $script = str_replace(trim($keyword) , $val , $script);
      }
    }
  }
  
  return $script;
}

/**
 *
 * @param <type> $script
 * @return <type> 
 */
// TODO: THIS FUNCTION IS NOT USED (by Ram)
function _get_complete_script($script) {

  $last_char = $str[strlen(trim($str))-1];
    
  return $last_char == ';'
       ? $script
       : "$script;";
}

/**
 *
 * @param <type> $script_file
 * @return <type> 
 */
function _scripting_get_magic_statement($script_file , $script_data) {
  
  $script_data = trim($script_data) ;  
  $last_char = $script_data[strlen($script_data)-1];
  if ( $last_char === ';') {
  
    return "  touch $script_file.done;";
  }  
  else {
  
    return " ; touch $script_file.done; ";
  }
  
  
}

/**
 *
 * @param <type> $instance_ip
 * @param <type> $instance_id
 * @param <type> $perm_file
 * @param <type> $script_id
 * @return <type> 
 */
function _cloud_scripting_is_script_execution_done($cloud_context, $instance_ip, $instance_id, $perm_file, $script_id ) {

  $ssh_user_name  = aws_ec2_lib_get_ssh_user( $cloud_context , $instance_id ) ;  
  $script_file    = '/tmp/' . $instance_id . CLOUD_PATH_SEPARATOR . $script_id . '.done';
  $remote_command = "[ -f $script_file ] && echo '1' || echo '0'";
  $command        = CLOUD_SSH_PATH . ' ' . $ssh_user_name . '@' . $instance_ip . ' -i "' . $perm_file . '" -o \'StrictHostKeyChecking no\' "'. $remote_command . '"' ;
  exec($command, $output, $retval);

  $result = isset($output[0])  ? $output[0] : 0 ;

  return $result ;
}



/**
 *
 * @param <type> $instance_ip
 * @param <type> $instance_id
 * @param <type> $perm_file
 * @return <type>
 */
function _cloud_scripting_is_ssh_enabled($cloud_context, $instance_ip, $instance_id, $perm_file) {

  $ssh_user_name  = aws_ec2_lib_get_ssh_user( $cloud_context , $instance_id ) ;  
  $remote_command = "[ -d /tmp ] && echo '1' || echo '0'";
  $command        = CLOUD_SSH_PATH . ' ' . $ssh_user_name . '@' . $instance_ip . ' -i "' . $perm_file . '" -o \'StrictHostKeyChecking no\' "'. $remote_command . '"' ;
  exec($command, $output, $retval);

  $result = isset($output[0])  ? $output[0] : '0' ;

  return $result ;
}

/**
 *
 * @param <type> $instance_id
 * @param <type> $key_name
 * @return <type> 
 */
function _cloud_scripting_get_perm_file($instance_id, $key_name, $cloud_context) {

  $perm_file = CLOUD_SCRIPTING_TEMP_DIRECTORY . $instance_id . CLOUD_PATH_SEPARATOR . $key_name;

  if ( _cloud_scripting_check_local_file_exists($perm_file) == CLOUD_SCRIPTING_FILE_NOT_EXIST) {

    $perm_file = _cloud_scripting_create_temp_permission_file($cloud_context,  $instance_id, $key_name);
  }

  return $perm_file;
}


/**
 *
 * @param <type> $file
 * @return <type> 
 */
function _cloud_scripting_check_local_file_exists($file) {

  exec("[ -f $file ] && echo '1' || echo '0'", $output, $retval);
    
  return $output[0];
}

function _cloud_scripting_check_local_directory_exist($dir) {

  exec("[ -d $dir ] && echo '1' || echo '0'", $output, $retval);
    
  return $output[0];
}