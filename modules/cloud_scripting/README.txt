BASIC INFO
==========

- Provides scripting feature such as bash, Perl and etc (Mainly bash scripts)
  on running an instance (or a virtual machine).
- Works with Cloud and Server Templates module.

* Windows is NOT supported.

HOW TO USE
==========

To create a scripting item for a template

1) Enable Scripting module
2) Go to the menu: Design | Scripting
3) Click 'New Script' button
4) Enter Name, Description, Script Type, Packages, Inputs, and Script
5) Click 'Add' button
6) Create a template
7) Go to the menu: Design | Template
8) Select a scripting item

* Scripts are executed by drupal's cron.
  Please set up and adjust cron job time for script execution.

  If you set up one minute for cron job, the scripting module
  try to execute scripts every one minutes or re-try those.


DIRECTORY STRUCTURE
===================

cloud
  +-modules (depends on Cloud module)(Cloud is a core module for Cloud package)
    +-cloud_activity_audit
    +-cloud_alerts
    x-cloud_auto_scaling
    +-cloud_billing
    +-cloud_cluster
    x-cloud_failover
    +-cloud_inputs
    +-cloud_pricing
    +-cloud_resource_allocator
    x-cloud_scaling_manager
    o-cloud_scripting (This module)
    +-cloud_server_templates

    x... Not released yet.


CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org

Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt