<?php

/**
 * @file
 * Provides scripting feature such as bash, Perl and etc (Mainly bash scripts).
 * Works with Cloud and Server Templates module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

// scripting functions
//start
function _cloud_scripting_get_scripts() {

  return $query =  'SELECT * FROM {' . CLOUD_SCRIPTING_TABLE . '} c WHERE %s like \'%%%s%%\' AND deleted=0' ;
}

function _cloud_scripting_get_script_query() {

  return $query =  'SELECT * FROM {' . CLOUD_SCRIPTING_TABLE . '} c WHERE %s = \'%s\' AND deleted=0' ;
}


function _cloud_scripting_get_count_db() {

   $script_count = 0 ;
   $query =  'SELECT count(*) as script_count FROM {' . CLOUD_SCRIPTING_TABLE . '} c  WHERE deleted=0 ' ;
   $result = db_query( $query, NULL );
   while ($script = db_fetch_object($result)) {

       $script_count = $script->script_count ;
   }

  return $script_count ;
}


function _cloud_scripting_get_script_by_id($script_Id) {

  $query =  'SELECT * FROM {' . CLOUD_SCRIPTING_TABLE . '} c WHERE id=\'%s\' AND deleted=0' ;
  $query_args = array(
    $script_Id,
  ) ;
  $result = db_query( $query, $query_args );
  $scripts_obj = db_fetch_object($result);
  return $scripts_obj ;
}


function _cloud_scripting_delete($id) {

  //// add watchdog log
  $query      = _cloud_scripting_get_scripts() ;
  $query_args = array(
    // TODO: What kind of id? (by Manoj)
    'id',
    $id ,
  );

  $result = db_query( $query, $query_args );
  $count = 0 ;
  $scripts_obj = db_fetch_object($result);

  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('Script has been deleted: @script_name', array('@script_name' => $scripts_obj->name)),
      'link'    => '',
    )
  );
  
//delete script
  $delete_query = 'UPDATE {' . CLOUD_SCRIPTING_TABLE . '} SET deleted=1 WHERE id=\'%s\''; 
  $query_args = array(
    $id,
  );
  db_query( $delete_query, $query_args );
  
  //DELETE FROM scripting template table
  $delete_query = 'UPDATE {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} SET deleted=1 WHERE script_id=\'%s\''; 
  $query_args = array(
    $id,
  );
  db_query( $delete_query, $query_args );

  return;
}


/**
 * Insert a script in database
 * This function insert entry in database and audit logs as well.
 *
 * @param $form_values
 *        This is the form-values submitted by new script form
 * @return return a last inserted script-id
 */ 
function _cloud_scripting_insert_script($form_values) {

  //print uuid(); die;
  $insert_query = 'INSERT INTO {' . CLOUD_SCRIPTING_TABLE . '}
                                (
                                  `name`              , 
                                  `description`       , 
                                  `type` , `packages` , 
                                  `inputs`            , 
                                  `script_template`   , 
                                  `created`           , 
                                  `updated` 
                                )  values '; 

  $insert_query = $insert_query . " ( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ) " ;
  $id           = uuid();
//$query_args[] = $id;
  $query_args = array(
    $form_values['name_text'           ] ,
    $form_values['description_text'    ] ,
    $form_values['script_type_select'  ] ,
    $form_values['packages_text'       ] ,
    $form_values['inputs_text'         ] ,
    $form_values['script_template_text'] ,
    date('c')                            , // UTC
    date('c')                            , // UTC
  );

  db_query( $insert_query, $query_args );
  $id = db_last_insert_id(CLOUD_SCRIPTING_TABLE, 'id');

  if ( module_exists('cloud_inputs')) {

    $inputs = explode(',', $form_values['inputs_text']);

    if (is_array($inputs) && count($inputs)>0) {
  
      foreach ($inputs as $input) {
  
        if (!empty($input)) {
          $insert_input_query = '';
          $insert_input_query = 'INSERT INTO {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '}
                                              ( `script_id`       ,  
                                                `input_parameter` , 
                                                `param_type`
                                              )  values '; 

          $insert_input_query = $insert_input_query . "  (  '%s', '%s', '%s' ) " ;
          $query_args = array(
            $id    ,
            $input ,
            'text' ,
          );
          db_query( $insert_input_query, $query_args );
        }
      }
    }
  }
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',
      'message' => t('New Script has been added: @script_name', array('@script_name' => $form_values['name_text'])),
      'link'    => '', //d'design/scripting/describe/info&id=' . $id);
    )
  );
  return;
}

function _cloud_scripting_update_script($form_values, $script_id='') {

  $update_query = 'UPDATE {' . CLOUD_SCRIPTING_TABLE . '}
                          SET `name`=\'%s\'            , 
                              `description`=\'%s\'     , 
                              `type`=\'%s\'            ,
                              `packages`=\'%s\'        , 
                              `inputs`=\'%s\'          ,  
                              `script_template`=\'%s\' ,  
                              `updated`=\'%s\' 
                          WHERE id=\'%s\' '; 

  $query_args = array(
    $form_values['name_text'           ] ,
    $form_values['description_text'    ] ,
    $form_values['script_type_select'  ] ,
    $form_values['packages_text'       ] ,
    $form_values['inputs_text'         ] ,
    $form_values['script_template_text'] ,
    date('c')                            , // UTC
    $script_id                           ,
  );
  db_query( $update_query, $query_args );

  ///insert into script input parameter
  if ( module_exists('cloud_inputs')) {
  $inputs = explode(',', $form_values['inputs_text']);
  $inputs_old = explode(',', $form_values['inputs_text_old']);
//print_r( $inputs_old);die;
  foreach ($inputs_old as $input_old) {
$delete_query = 'delete FROM {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '}
                    WHERE script_id="' . $script_id . '" AND input_parameter = "' . $input_old . '" ';
    //print $delete_query;die;
  $result3 = db_query( $delete_query );
  }
  foreach ($inputs as $input) {

    if (!empty($input) ) {
        
      $get_query = 'SELECT * FROM {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '}
                    WHERE script_id="' . $script_id . '" AND input_parameter = "' . $input . '" ';

      $result1 = db_query( $get_query );
        
      $total_input_rows = !empty($result->num_rows) ? $result->num_rows : '';//mysql_num_rows($result1);
        
      if ($total_input_rows<=0) {
        //print $total_input_rows;die;
        $insert_input_query = 'INSERT INTO {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '}
                                            ( `script_id`      ,  
                                             `input_parameter` , 
                                              `param_type`
                                            )  values '; 

        $insert_input_query = $insert_input_query . "  ( '%s', '%s', '%s' ) " ;
        $query_args = array(
          $script_id ,
          $input     ,
          'text'     ,
        );
        db_query( $insert_input_query ,   $query_args );
      }
    }
  }
  }
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',
      'message' => t('Script has been modified: @script_name', array('@script_name' => $form_values['name_text'])),
      'link'    => '', // 'design/scripting/describe/info&id=' . $script_id);
    )
  );

  return;
}


// function _cloud_scripting_add_ready_for_exec_query($instance_id, $template_id, $cluster_id, $cloud) {
//
//   $query  = 'INSERT INTO {' . CLOUD_SCRIPTING_INSTANCES_TABLE . "} (instance_id, script_id, script_executed,  started,  cluster_id,  cloud,  template_id) ";
//   $query .= "SELECT \"" . $instance_id . "\" , script_id, '0', NOW(), \"" . $cluster_id . "\", \"". $cloud . "\" , \"" . $template_id . "\"  FROM {" . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . "} c WHERE server_template_id in(\"".  $template_id . "\" ) and deleted=0  ";
//   $query .= ' and script_id not in (SELECT script_id  FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . "} WHERE instance_id  = '" . $instance_id . "') ORDER BY `order`";
//
//   drupal_log_to_file("DEPLOYMENT::ADDNEWSCRIPT",  "DRUPAL_BASE",  "INFO",  "Query:=" . $query  );
//   return $query;
// }


function _cloud_scripting_get_scripts_of_template() {

  return $query = 'SELECT * FROM {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} c WHERE server_template_id=\'%s\' AND deleted=0  ORDER BY `order`' ;
}


function _cloud_scripting_get_executed_scripts() {

  $query = 'SELECT * FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '} c WHERE instance_id=\'%s\' AND script_executed=2 ' ;
  return cloud_get_db_results($query);
}


function _cloud_scripting_get_selected_scripts($str_script) {

  $query = 'SELECT * FROM {' . CLOUD_SCRIPTING_TABLE . '} c WHERE id IN (' . $str_script . ') AND deleted=0 ' ;
  return cloud_get_db_results($query);
}


function _cloud_scripting_get_script($script_id) {
    $query = 'SELECT * FROM {' . CLOUD_SCRIPTING_TABLE . '} WHERE id IN (\'' . $script_id . '\') AND deleted = 0  ' ;
    $return_val = cloud_get_db_results($query);
    return $return_val[0];
}


function _cloud_scripting_register_exec_save($instance_id, $script_id, $status) {

  $select_query = 'SELECT * FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '} c WHERE instance_id=\'%s\' AND script_id=\'%s\' ' ;
  $query_args = array(
    $instance_id ,
    $script_id   ,
  ) ;
  $result = db_query( $select_query, $query_args );

  $found = FALSE ;
  while ($key = db_fetch_object($result)) {

    $found = TRUE ;
  }

  drupal_log_to_file('DEPLOYMENT::EXECUTE_SCRIPT', 'DRUPAL_BASE', 'INFO', 'FOUND=' . $found );
  if ($found) { // Update Table

    $update_query = 'UPDATE {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '} SET script_executed=\'%s\'
                     WHERE instance_id=\'%s\' and script_id=\'%s\' ';
    $query_args = array(
      $status      ,
      $instance_id ,
      $script_id   ,
    ) ;
    db_query( $update_query, $query_args );

    drupal_log_to_file('DEPLOYMENT::EXECUTE_SCRIPT', 'DRUPAL_BASE', 'INFO', 'UPDATE=' . $update_query );
  }
  else { // Insert in table

    $insert_query = 'INSERT INTO {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '}
                                  ( `instance_id` ,  
                                    `script_id` , 
                                    `script_executed` 
                                  )  values '; 

    $insert_query = $insert_query . "  (  '%s', '%s', '%s' ) " ;

    $query_args = array(
      $instance_id ,
      $script_id   ,
      $status      ,
    ) ;
    db_query( $insert_query, $query_args );
    drupal_log_to_file('DEPLOYMENT::EXECUTE_SCRIPT', 'DRUPAL_BASE', 'INFO', 'INSERT=' . $insert_query );
  }

  return;
}


/**
 * function for checking if already there is same script ready for execution. *
 * @param unknown_type $instance_id
 * @param unknown_type $script_id
 * @return TRUE/FALSE
 */
function _cloud_scripting_is_ready_for_exec($instance_id, $script_id) {

  $query = "SELECT * FROM {cloud_instance_script} WHERE instance_id ='" . $instance_id . "' AND script_id='" . $script_id . "' AND script_executed = 0";
  $row_count = count(cloud_get_db_results($query));

  if ($row_count > 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}


/**
 * function for adding script ready for execution on instance.
 * @return
 */
function _cloud_scripting_add_ready_for_exec_on_instance($instance_id, $script_id, $script_executed, $cloud, $cluster_id, $template_id) {

  if ( _cloud_scripting_is_ready_for_exec($instance_id, $script_id)) return;
    
    $insert_query = 'INSERT INTO {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '}
                                  ( `instance_id`     ,
                                    `started`         ,  
                                    `script_id`       ,
                                    `script_executed` ,
                                    `cluster_id`      ,
                                    `cloud`           ,
                                    `template_id`
                                  )  values ';

  $insert_query = $insert_query . "  ( '%s', now(), '%s', '%s', '%s', '%s', '%s' ) " ;

  $query_args = array(
    $instance_id     ,
    $script_id       ,
    $script_executed ,
    $cluster_id      ,
    $cloud           ,
    $template_id     ,
  );
  
  db_query( $insert_query, $query_args );

  return 0;
}

function _cloud_scripting_register_script_save($instance_id, $script_id, $status , $cloud , $template_id) {

  $insert_query = 'INSERT INTO {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '}
                                ( `instance_id`     ,
                                  `started`         ,  
                                  `script_id`       ,
                                  `script_executed` ,
                                  `cloud`           ,
                                  `template_id`
                                )  values ';

  $insert_query = $insert_query . " ( '%s', now(), '%s', '%s', '%s', '%s' ) " ;

  $query_args = array(
    $instance_id ,
    $script_id   ,
    $status      ,
    $cloud       ,
    $template_id ,
  ) ;

  db_query( $insert_query, $query_args );

  return;
}


function _cloud_scripting_get_master_script_id_query() {

  return $query = 'SELECT * FROM {' . CLOUD_SCRIPTING_MASTER_TABLE . "} c WHERE script_name='%s' " ;
}


function _cloud_scripting_get_by_script_id_instance_id() {

  return $query = 'SELECT * FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . "} c WHERE instance_id='%s' AND script_id='%s' " ;

}

function _cloud_scripting_get_scripts_for_instance($instance_id) {

  $scripts_query      = $query = 'SELECT inst_script.script_id FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . "} inst_script LEFT JOIN {" . CLOUD_SCRIPTING_TABLE . "} scr ON inst_script.script_id=scr.id WHERE instance_id='%s' AND scr.deleted='0' " ;
  $scripts_query_args = array(
    $instance_id,
  ) ;
  
  $result = array() ;
  $result_scripts = db_query( $scripts_query, $scripts_query_args );
  while ($script  = db_fetch_object($result_scripts)) {
      
    $result[] = $script->script_id ;
  }

  return $result ;
}

/**
 *
 * @param $instance_id
 * @return unknown_type
 */
function _cloud_scripting_get_scripts_ready_for_instance_query($instance_id, $script_id) {

  $query = '';
  if (empty($script_id)) {
    $query = 'SELECT vis.instance_id, vis.script_id script_id, vis.cluster_id, vis.cloud, vis.template_id, s.script_template script FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '} vis, 
          {' . CLOUD_SCRIPTING_TABLE . "} s WHERE s.id = vis.script_id and vis.instance_id = '" . $instance_id . "' ORDER BY vis.id";
  }
  else {
    $query = 'SELECT vis.instance_id, vis.script_id script_id, vis.cluster_id, vis.cloud, vis.template_id, s.script_template script FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '} vis, 
          {' . CLOUD_SCRIPTING_TABLE . "} s WHERE s.id = vis.script_id and vis.instance_id = '" . $instance_id . "' and vis.script_id = '" .  $script_id . "' ORDER BY vis.id";
  }

 // $query = "SELECT vis.instance_id,  vis.script_id script_id,  vis.cluster_id,  vis.cloud,  vis.template_id,  s.script_template script FROM cloud_instance_script vis,  scripts s WHERE s.id = vis.script_id and vis.instance_id = '" .  $instance_id . "' and vis.script_id not in (";
 // $query .= "  SELECT script_id FROM cloud_instance_script WHERE instance_id = '" .  $instance_id . "' and script_executed = '2') ORDER BY vis.id";

  return $query;
}


/**
 *
 * @param $cluster_id
 * @param $script_id
 * @return unknown_type
 */
function _cloud_scripting_get_cluster_input_query($cluster_id, $script_id) {

  $query = 'SELECT ip.script_id, ip.param_type, ip.input_parameter, pv.cluster_value, pv.value_of_instance_id FROM {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '} ip, 
                        {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . " } pv WHERE ip.param_id = pv.param_id AND pv.cluster_id = " . $cluster_id . " and ip.script_id = '" . $script_id . "'";
  return $query;
}


/**
 *
 * @param <type> $template_id
 * @param <type> $script_id
 * @return array()
 */
function _cloud_scripting_get_script_inputs($template_id, $script_id) {

  $query = 'SELECT ip.script_id, ip.param_type, ip.input_parameter, pv.template_value, pv.value_of_instance_id FROM {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '} ip,
                        {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . "} pv WHERE ip.param_id = pv.param_id AND pv.template_id = " . $template_id . " and ip.script_id = '" . $script_id . "'";
    
  return cloud_get_db_results($query);
}

/**
 * function for getting script id from master scripts table.
 * @return script_id(varchar)
 */
function _cloud_scripting_get_id_from_master($script) {
    
    $master_script_query      = _cloud_scripting_get_master_script_id_query();
    $master_script_query_args = array(
      $script,
    ) ;
    
    $script_id = '' ;
    
    $result_master_script = db_query($master_script_query, $master_script_query_args ) ; 

    if ($script_data = db_fetch_object($result_master_script)) {

      $script_id = $script_data->script_id ;
    }

    return $script_id;
}

/*
 * Required functions
 */

/**
 * 
 * @param <type> $instance_id
 * @param <type> $script_ids
 * @param <type> $script_executed
 * @return <type>
 * function for Updating the current status for Script.
 */
function _cloud_scripting_update_scripts_status($instance_id, $script_ids, $script_execution_status  ) {
    
  $query = 'UPDATE {' . CLOUD_SCRIPTING_INSTANCES_TABLE . "} SET script_executed = '" . $script_execution_status .
          "' WHERE instance_id ='" . $instance_id . "' AND script_id in (" . $script_ids . ')';
  db_query( $query );

  return;
}

/***NEW FUNCTIONS ADDED****/

/**
 *
 * @param <type> $cloud_context
 * @return <type> 
 */
function _cloud_scripting_get_execute_ready_scripts($cloud_context) {

  $query  = 'SELECT * FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . "} WHERE `script_executed` = '" . CLOUD_SCRIPTING_SCRIPT_READY_FOR_EXECUTION . "' AND instance_id NOT IN ";
  $query .= ' ( SELECT instance_id FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . "} WHERE `script_executed` = '" . CLOUD_SCRIPTING_SCRIPT_UNDER_EXECUTION . "') ORDER BY instance_id, id";
  
  return cloud_get_db_results($query);
}


function _cloud_scripting_get_instance_script_query() {

  return $query = 'SELECT * FROM {' . CLOUD_SCRIPTING_INSTANCES_TABLE . '} c WHERE %s = \'%s\'' ;
}


function _cloud_scripting_get_under_progress_scripts() {
   $query        = _cloud_scripting_get_instance_script_query();
   $query_args   = array(
     'script_executed'                      ,
     CLOUD_SCRIPTING_SCRIPT_UNDER_EXECUTION ,
   );

   return cloud_get_db_results($query, $query_args);
}