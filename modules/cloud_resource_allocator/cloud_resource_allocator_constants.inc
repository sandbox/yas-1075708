<?php

/**
 * @file
 * Define constants: Resource Allocator for Pluggable Algorithm
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


// Namespace-related
define('CLOUD_RESOURCE_ALLOCATOR_PREFIX'                   , 'cloud_'                                              ) ;
define('CLOUD_RESOURCE_ALLOCATOR_NAME'                     , 'Resource Allocator'                                  ) ;
define('CLOUD_RESOURCE_ALLOCATOR_MODULE_NAME'              , 'cloud_resource_allocator'                                  ) ;

// Hook-related
define('CLOUD_RESOURCE_ALLOCATOR_HOOK_SET_ALGO_INFO'       , 'set_algo_info'                                       ) ;
define('CLOUD_RESOURCE_ALLOCATOR_HOOK_DEPLOY_INFO'         , 'get_deploy_info'                                     ) ;

// Database-related
define('CLOUD_RESOURCE_ALLOCATOR_SCHEMA_VERSION'           , 'cloud_resource_allocator_schema_version'             ) ;
define('CLOUD_RESOURCE_ALLOCATOR_TABLE_NAME'               , CLOUD_RESOURCE_ALLOCATOR_PREFIX . 'resource_allocator') ;
define('CLOUD_RESOURCE_ALLOCATOR_FIELD_ALGO_MODULE_NAME'   , 'algo_module_name'                                    ) ;
define('CLOUD_RESOURCE_ALLOCATOR_FIELD_ALGO_DISPLAY_NAME'  , 'algo_display_name'                                   ) ;

// Default Algo-related
define('CLOUD_RESOURCE_ALLOCATOR_DEFAULT_ALGO_MODULE_NAME' , 'resource_allocator_round_robin_algo'                 ) ;
define('CLOUD_RESOURCE_ALLOCATOR_DEFAULT_ALGO_DISPLAY_NAME', 'Round Robin'                                         ) ;
