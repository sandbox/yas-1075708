HOW TO USE Resource Allocator
=============================

  1) Go to Site configuration | Resource Allocator
  2) Select Algorithm for instance allocation onto specific host or datacenter
     The default algorithm is 'Round Robin Algo'


How to implement an algorithm for instance allocation
=====================================================

  1) Resource Allocator calls a
  
  hook_get_deploy_info($params)
  
  function for a selected instance allocation algorithm in
  
  Site configuration | Resource Allocator

  There are four functions to implement as shown below:

  *.install file:

  function hook_enable() {
    drupal_load('module', 'resource_allocator');
    resource_allocator_notify('enable' , 'your_module_prefix');
  }

  function round_robin_algo_disable() {
    resource_allocator_notify('disable', 'your_module_prefix');
  } 

  *.module file:

  hook_algo_set_algo_info()
  hook_algo_get_deploy_info($params)  

  See also comments in round_robin_algo.* files (indicated "TODO: Must Implement") 


DIRECTORY STRUCTURE
===================


cloud
  +-modules (depends on Cloud module)(Cloud is a core module for Cloud package)
    +-cloud_activity_audit
    +-cloud_alerts
    x-cloud_auto_scaling
    +-cloud_billing
    +-cloud_cluster
    x-cloud_failover
    +-cloud_inputs
    +-cloud_pricing
    o-cloud_resource_manager (This module)
      +-allocation_algo      ( depends on resource_manager module )
      +-round_robin_algo     ( depends on resource_manager module )
      +-example_algo         ( depends on resource_manager module )
      +-...
    x-cloud_scaling_manager
    +-cloud_scripting
    o-cloud_server_templates (This module)

    x... Not released yet.


CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org
2010/11/09 ver.0.7  released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt