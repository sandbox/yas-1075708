<?php

/**
 * @file
 * Define constants: Allocation Algorithm
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


define('CLOUD_RESOURCE_ALLOCATOR_ALLOCATION_ALGO_DISPLAY_NAME' , 'Allocation Algo'       ) ;
define('CLOUD_RESOURCE_ALLOCATOR_ALLOCATION_ALGO_MODULE_NAME'  , 'cloud_allocation_algo' ) ;
define('CLOUD_RESOURCE_ALLOCATOR_ALLOCATION_ALGO_CLOUD_RESOURCE_ALLOCATOR_MODULE_NAME', 'cloud_resource_allocator') ;
