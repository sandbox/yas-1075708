DIRECTORY STRUCTURE
===================

cloud
  +-resource_manager       ( depends on cloud modules )(Cloud is a core module)
    +-allocation_algo      ( depends on resource_manager module )
    +-round_robin_algo          ( depends on resource_manager module )
    +-example_algo         ( depends on resource_manager module )
    +-...


CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org
2010/11/09 ver.0.7 released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.