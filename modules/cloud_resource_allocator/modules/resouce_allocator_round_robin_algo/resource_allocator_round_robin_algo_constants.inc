<?php

/**
 * @file
 * *.constants.inc file: Resource Allocator for Round Robin Algorithm
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


/**
 * @file
 * Define constants: Rroundrobin Algorithm
 *
 */

define('CLOUD_RESOURCE_ALLOCATOR_ROUND_ROBIN_ALGO_DISPLAY_NAME' , 'Round Robin Algo'  ) ;
define('CLOUD_RESOURCE_ALLOCATOR_ROUND_ROBIN_ALGO_MODULE_NAME'  , 'resource_allocator_round_robin_algo'  ) ;
define('CLOUD_RESOURCE_ALLOCATOR_ROUND_ROBIN_ALGO_RESOURCE_ALLOCATOR_MODULE_NAME', 'cloud_resource_allocator') ;
