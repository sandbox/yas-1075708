BASIC INFO
==========

- Provides server template functionality for multiple cloud of
  each cloud sub-system.
- Works with Cloud module.


HOW TO USE
==========

1) Enable Server Template module
2) Go to the menu: Design | Template
3) Click 'New Template' button
4) Input server template information
5) Click 'Add' button


DIRECTORY STRUCTURE
===================

cloud
  +-modules (depends on Cloud module)(Cloud is a core module for Cloud package)
    +-cloud_activity_audit
    +-cloud_alerts
    x-cloud_auto_scaling
    +-cloud_billing
    +-cloud_cluster
    x-cloud_failover
    +-cloud_inputs
    +-cloud_pricing
    +-cloud_resource_allocator
    x-cloud_scaling_manager
    +-cloud_scripting
    o-cloud_server_templates (This module)
  
    x... Not released yet.


CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org
2010/11/09 ver.0.7  released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt