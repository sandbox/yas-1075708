<?php

/**
 * @file
 * Provides server template functionality for each cloud sub-system.
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

/**
 * Updated by yas   2011/02/14
 * Updated by yas   2011/02/11
 * Updated by yas   2011/02/10
 * Updated by yas   2011/02/08
 * Updated by yas   2011/02/02
 * Updated by yas   2011/02/01
 * Updated by Jamir 2011/02/01
 */

///server templates callback functions
/**
 * Returns a form with listing of server-templates
 *
 * @param $module, $cloud_context, $params
 * @return return a form
 */
function _cloud_server_templates_list($module, $cloud_context='', $params = array()) {
  
  return drupal_get_form('cloud_server_templates_list', $cloud_context, $params) ;  
}

  
/**
 * Returns a form with listing of server-templates
 * Build a form including table header and table body
 * @param $form_submit
 *        This is the form-submit submitted by list server template form
 * @param $cloud_context
 *        This is the sub-cloud who's template is being displayed
 * @return return a form
 */
function cloud_server_templates_list($form_submit='', $cloud_context, $params = array()) {

  drupal_add_js(cloud_get_module_base() . 'js/cloud.js', 'module');
    
  $column     = 'template_nickname' ;
  $cloud_type = $cloud_context ;

  
  $destination          = drupal_get_destination();
  $settings_done        = FALSE ;
  $images_found         = FALSE ;
  $instances_data_found = FALSE ;

  $cloud_list = cloud_get_all_clouds_display_name() ;
  if ( $cloud_context && empty($params['all']) ) { // Cloud specified

    $is_cloud_enabled = cloud_is_settings_done($cloud_context) ;
    if ($is_cloud_enabled === FALSE ) {

      if (user_access($cloud_context . ' administer cloud')) {    
      
        $admin_url = filter_xss( l( t('@cloud_name Settings', array('@cloud_name' => $cloud_list[$cloud_context])), "admin/settings/$cloud_context" ) );
      }
      else {
      
        $admin_url = $cloud_list[$cloud_context] ;
      }
      
      drupal_set_message(check_plain(t('The variables are not correctly configured: ')) . $admin_url, 'error');
    }
    else {

      $settings_done = TRUE ;
    }
  }
  else { // Check for all Clouds

    foreach ($cloud_list as $cloud_context_lp => $cloud_display_name ) {

      $is_cloud_enabled = cloud_is_settings_done($cloud_context_lp) ;
      if ($is_cloud_enabled === FALSE ) {

            if (user_access($cloud_context_lp . ' administer cloud')) {    
            
                $admin_url = filter_xss( l( t('@cloud_name Settings', array('@cloud_name' => $cloud_display_name)), "admin/settings/$cloud_context_lp" ) );
            }
            else {
                
                $admin_url = $cloud_display_name ;
            }
            drupal_set_message(check_plain(t('The variables are not correctly configured: ')) . $admin_url, 'error');
      }
      else {
        $settings_done = TRUE ;
      }
    }
  }

  if ($settings_done) {

    if ( $cloud_context && empty($params['all']) ) { // Cloud specified

      $cloud_has_data = cloud_perform_action('' , 'check_key_sg_data' , $cloud_context) ;
      if ( $cloud_has_data == FALSE ) {

        $create_ssh_key_url = '' ;
        $cloud_has_key_data = cloud_perform_action('' , 'check_key_data' , $cloud_context) ;
        if ($cloud_has_key_data === FALSE ) {

            if (user_access($cloud_context . ' register key')) {

                $create_ssh_key_url = filter_xss( l( t('Create SSH Key'   ), "clouds/$cloud_context/ssh_keys/create",  array( 'query' => $destination ) ) );
            }

            drupal_set_message(check_plain(t('There is no information of SSH Keys ')) . $create_ssh_key_url , 'warning');
        }

        $create_sg_url = '' ;
        $cloud_has_sg_data = cloud_perform_action('' , 'check_sg_data' , $cloud_context) ;
        if ($cloud_has_sg_data === FALSE ) {

            if (user_access($cloud_context . ' register security group')) {

               $create_sg_url = filter_xss( l( t('Create Security Group' ), "clouds/$cloud_context/security_groups/create",  array( 'query' => $destination ) ) );
            }
            drupal_set_message(check_plain(t('There is no information of Security Group ')) . $create_sg_url , 'warning');
        }       
      }
      else {
      
        $instances_data_found = TRUE ;
      }
      
      $image_count = cloud_perform_action('' , 'get_images_count' , $cloud_context) ;
      if ($image_count == 0) {
      
        $images_url = filter_xss( l( t('@cloud_name - Update Image Information', array('@cloud_name' => $cloud_list[$cloud_context])), "clouds/$cloud_context/getimagedata"  , array( 'query' => $destination ) ) );
        drupal_set_message(check_plain(t('There are no image information: ')) . $images_url, 'warning');
      }
      else {
      
        $images_found = TRUE ;
      }
    }
    else { // Check for all Clouds

      foreach ($cloud_list as $cloud_context_lp => $cloud_display_name ) {

        if ( cloud_is_settings_done($cloud_context_lp) === FALSE ) {
          continue ;
        }

        $cloud_has_data = cloud_perform_action('' , 'check_key_sg_data' , $cloud_context_lp) ;
        if ( $cloud_has_data == FALSE ) {

              $cloud_has_data = cloud_perform_action('' , 'check_key_sg_data' , $cloud_context_lp) ;
              if ( $cloud_has_data == FALSE ) {

                $create_ssh_key_url = '' ;
                $cloud_has_key_data = cloud_perform_action('' , 'check_key_data' , $cloud_context_lp) ;
                if ($cloud_has_key_data === FALSE ) {

                    if (user_access($cloud_context_lp . ' register key')) {

                        $create_ssh_key_url = filter_xss( l( t('Create SSH Key'   ), "clouds/$cloud_context_lp/ssh_keys/create",  array( 'query' => $destination ) ) );
                    }

                    drupal_set_message(check_plain(t('@cloud_name - There is no information of SSH Keys ' , array('@cloud_name' => $cloud_display_name) )) . $create_ssh_key_url , 'warning');
                }

                $create_sg_url = '' ;
                $cloud_has_sg_data = cloud_perform_action('' , 'check_sg_data' , $cloud_context_lp) ;
                if ($cloud_has_sg_data === FALSE ) {

                    if (user_access($cloud_context_lp . ' register security group')) {

                       $create_sg_url = filter_xss( l( t('Create Security Group' ), "clouds/$cloud_context_lp/security_groups/create",  array( 'query' => $destination ) ) );
                    }
                    drupal_set_message(check_plain(t('@cloud_name - There is no information of Security Group ' , array('@cloud_name' => $cloud_display_name) )) . $create_sg_url , 'warning');
                }
              }
        }
        else {

          $instances_data_found = TRUE ;
        }
        
        $image_count = cloud_perform_action('' , 'get_images_count' , $cloud_context_lp) ;
        if ($image_count == 0) {

          $images_url = filter_xss( l( t('@cloud_name - Update Image Information', array('@cloud_name' => $cloud_display_name)), "clouds/$cloud_context_lp/getimagedata", array( 'query' => $destination ) ) );
          drupal_set_message(check_plain(t('There are no image information: ')) . $images_url, 'warning');
        }
        else {

          $images_found = TRUE ;
        }
      }
    }
  }
  else {
      
    $images_found = FALSE ;
    $instances_data_found = FALSE ;
  }

  $options = array(
     t('Nickname'     ),
     t('Type'         ),
 //  t('Cloud Type'   ),
 //  t('User Data'    ),
  );
  
  $filter     = cloud_get_filter_value($form_submit, 'filter'    );
  $filter     = trim($filter);  

  $filter_col = cloud_get_filter_value( $form_submit, 'operation');
  $filter_col = isset($filter_col) && $filter_col ? $filter_col : 0; // default: Template Name
   
  if ( $filter_col == 0 )  {

    $column  = 'Template Name'      ;
    $sql_col = 'a.template_nickname' ;
  }
  elseif ($filter_col == 1 ) {

    $column  = 'Instance Type' ;
    $sql_col = 'instance_type' ;
  }
  elseif ($filter_col == 1 || isset($cloud_type)) {

    $column  = 'Cloud Type' ;
    $sql_col = 'cloud_type' ;
  }
  elseif ($filter_col == 3 ) {

    $column  = 'User Data' ;
    $sql_col = 'user_data' ;
  }  
    
  if ( isset($filter) ) {
    
    $query_args[] = $sql_col ;
    $query_args[] = $filter  ;
  }
  else {

    $filter       = ' 1 ' ;
    $query_args[] = 'a.template_nickname' ;
    $query_args[] = ' ' ;
  }
  
//asort($options);

  $form['options'] = array(
    '#prefix' => '<div class="container-inline">',
    '#type'   => 'fieldset',
    '#suffix' => '</div>',
 // '#title' => t('Operations'), 
  );
  
  $tmpl_count = _cloud_server_templates_get_server_templatelist_count($cloud_context, $params) ;
  $filter_disabled = '' ;
  if ( $tmpl_count < 2 ) {

    $filter_disabled = TRUE ;
  }
  
  $form['options']['label'    ] = array( '#type' => 'item'     , '#title'   => t('Filter'));
  $form['options']['operation'] = array( '#type' => 'select'   , '#options' => $options, '#default_value' => $filter_col, '#disabled' => $filter_disabled );
  $form['options']['filter'   ] = array( '#type' => 'textfield', '#size'    => 40      , '#default_value' => $filter    , '#disabled' => $filter_disabled );
  $form['options']['submit'   ] = array( '#type' => 'submit'   , '#value'   => t('Apply')                               , '#disabled' => $filter_disabled );

  if (user_access('create server template') ) {

    if ($images_found === FALSE || $instances_data_found === FALSE )  {
      $form['options']['server_temaplate'] = array('#type' => 'submit', '#value' => t('Create') , '#disabled' => TRUE );
    }
    else {
      $form['options']['server_temaplate'] = array('#type' => 'submit', '#value' => t('Create'));
    }
  }  
  
  $form['header'] = array(
  
    '#type'  => 'value',
    '#data'  => '',
    '#value' => array(
      array('data' => t('Nickname'    ), 'field' => 'template_nickname'                            ,
                                          'sort'  => 'asc'                                         ,
                                          'class' => 'nickname-column'                           ) ,
      array('data' => t('Description' ), 'field' => 'description'       /* , 'width' => '250' */ ) , 
    //array('data' => t('Cloud'       ), 'field' => 'cloud_type'        /* , 'width' => '80'  */ ) , 
      array('data' => t('Type'        ), 'field' => 'instance_type'     /* , 'width' => '80'  */ ) , 
      array('data' => t('Instances'   ), 'field' => 'count_instances'   /* , 'width' => '70'  */ ) , 
    //array('data' => t('Status'      ), 'field' => 'template_status'                            ) ,
      array('data' => t('Created'     ), 'field' => 'template_created'  /* , 'width' => '120' */ ) , 
      array('data' => t('Updated'     ), 'field' => 'template_updated'  /* , 'width' => '120' */ ) ,
      array('data' => t('Action'      ), 'class' => 'action-column'                              ) ,
    )
  );

  $query  = _cloud_server_templates_get_server_templatelist($cloud_context, $params) ;
  $query .= tablesort_sql( $form['header']['#value'] ) ;   
  $result = pager_query( $query, CLOUD_SERVER_TEMPLATES_PAGER_LIMIT, 0, NULL, $query_args );
  $destination = drupal_get_destination();

  while ($template_obj = db_fetch_object($result)) {

    $date_created = '';
    $date_created = ($template_obj->template_created != '0000-00-00 00:00:00'
                  && $template_obj->template_created != '')
                  ? format_date(strtotime($template_obj->template_created), 'small')
                  : '-' ;
    $date_updated = '';
    $date_updated = ($template_obj->template_updated != '0000-00-00 00:00:00'
                  && $template_obj->template_updated != '')
                  ? format_date(strtotime($template_obj->template_updated), 'small')
                  : '-' ;
    $server_template_id = $template_obj->serverTemplateId;
  
    $form['Name'           ][$server_template_id] = array( array('#value' => l( $template_obj->template_nickname , CLOUD_SERVER_TEMPLATES_PATH . '/' . $template_obj->cloud_type . '/' .  urlencode($server_template_id) . '/view',  array('query' => 'id=' . urlencode($server_template_id) . '&' . 'cloud_context=' . $template_obj->cloud_type ) ) ) );//,  array(),   'id=' . urlencode($server_template_id) ,  NULL,  FALSE,  TRUE ) ) );
    $form['hdnName'        ][$server_template_id] = array( '#type' => 'hidden' , '#value' => addslashes($template_obj->template_nickname) );
    $form['Description'    ][$server_template_id] = array( array('#value' => t( $template_obj->description     ) ) );
    $form['cloud_type'     ][$server_template_id] = array( array('#value' => t( $template_obj->cloud_type      ) ) );
    $form['instance_type'  ][$server_template_id] = array( array('#value' => t( $template_obj->instance_type   ) ) );
  //$form['ec2_image_id'   ][$server_template_id] = array( array('#value' => t( $template_obj->image_id        ) ) );
    $form['count_instances'][$server_template_id] = array( array('#value' => t( $template_obj->count_instances ) ) );
  //$form['Status'         ][$server_template_id] = array( array('#value' => t( (($template_obj->template_status == 1) ? 'Active' : 'Inactive') ) ) ) ;
    $form['Created'        ][$server_template_id] = array( '#value' => $date_created ) ;
    $form['Updated'        ][$server_template_id] = array( '#value' => $date_updated ) ;
  }
  $server_template_id = !empty($server_template_id) ? $server_template_id : '';
  $form['pager'        ] = array('#value' => theme('pager', NULL, CLOUD_SERVER_TEMPLATES_PAGER_LIMIT, 0));
  $form['id'           ] = array('#type'  => 'hidden', '#value' => $server_template_id ) ;
  $form['cloud_context'] = array('#type'  => 'hidden', '#value' => $cloud_context      ) ;
  $form['#redirect'    ] = FALSE;
  
  return $form;
}


function theme_cloud_server_templates_list($form) {

  $output = drupal_render($form['options']);
  //$form['Name'] = !empty($form['Name']) ? $form['Name'] : '';
  $form['pager']['#value'] = !empty($form['pager']['#value']) ? $form['pager']['#value'] : '';
  $rows = array();
  
  if ( !empty($form['Name']) ) {
    foreach (element_children($form['Name']) as $key) {
    
    $form['hdnName'][$key]['#value'] = !empty($form['hdnName'][$key]['#value']) ? $form['hdnName'][$key]['#value'] : '';
    $form['cloud_type'][$key][0]['#value'] = !empty($form['cloud_type'][$key][0]['#value']) ? $form['cloud_type'][$key][0]['#value'] : '';
      $row = array(
        array('data' => drupal_render($form['Name'][$key]), 'class' => 'nickname-column'),
        drupal_render($form['Description'    ][$key]),
     // drupal_render($form['cloud_type'     ][$key]),
        drupal_render($form['instance_type'  ][$key]),
        drupal_render($form['count_instances'][$key]),
     // drupal_render($form['Status'         ][$key]),
        drupal_render($form['Created'        ][$key]),
        drupal_render($form['Updated'        ][$key]),
      ); 
      //print_r($form['Name']);die;
      $prop['onclick'] = cloud_get_messagebox('Are you sure you want to delete the template "' . trim($form['hdnName'][$key]['#value']) . '" ?') ;
   
      $action_data = '' ;
      if (user_access('delete server template')) {
        $action_data .= cloud_display_action( 'images/icon_delete', t('Delete'), CLOUD_SERVER_TEMPLATES_PATH . '/' . $form['cloud_type'][$key][0]['#value'] . '/' . urlencode($key) . '/delete', array('query' => 'id=' . urlencode($key), 'html' => TRUE), $prop['onclick']);
      }
  
      if (user_access('edit server template')) {
        $action_data .= cloud_display_action( 'images/icon_clear', t('Edit'  ), CLOUD_SERVER_TEMPLATES_PATH . '/' . $form['cloud_type'][$key][0]['#value'] . '/' . urlencode($key) . '/edit', array('query' => 'id=' . urlencode($key), 'html' => TRUE));
      }
  
      if (  user_access('launch server template') && !strstr($form['cloud_type'][$key][0]['#value'], 'cp')) {
        $action_data .= cloud_display_action( 'images/icon_play', t('Launch' ), CLOUD_SERVER_TEMPLATES_PATH . '/' . $form['cloud_type'][$key][0]['#value'] . '/' . urlencode($key) . '/launch', array('query' => 'id=' . urlencode($key) . '&count=' . $form['count_instances'][$key][0]['#value'] . '&type=' . $form['cloud_type'][$key][0]['#value'], 'html' => TRUE));//,  'id=' . urlencode($key) . '&count=' . $form['count_instances'][$key][0]['#value'] . '&type=' . $form['cloud_type'][$key][0]['#value'] );
      }  
  
      if (  user_access('copy server template')) {
        $action_data .= cloud_display_action( 'images/icon_copy', t('Copy'   ), CLOUD_SERVER_TEMPLATES_PATH . '/' . $form['cloud_type'][$key][0]['#value'] . '/' . urlencode($key) . '/copy', array('query' => 'id=' . urlencode($key) . '&count=' . $form['count_instances'][$key][0]['#value'] . '&type=' . $form['cloud_type'][$key][0]['#value'], 'html' => TRUE));//,  'id=' . urlencode($key) . '&count=' . $form['count_instances'][$key][0]['#value'] . '&type=' . $form['cloud_type'][$key][0]['#value'] );
      } 
    
      if (user_access('edit server template') && module_exists('cloud_inputs')) {  
        $action_data .= cloud_display_action( 'images/table_gear', t('Inputs'), 'design/cloud_server_templates/' . urlencode($key) . '/inputs', array('query' => 'template_id=' . urlencode($key) . '&module=cloud_server_templates&type=' . $form['cloud_type'][$key][0]['#value'], 'html' => TRUE));//,  'template_id=' . urlencode($key) . '&module=server_templates&type=' . $form['cloud_type'][$key][0]['#value']);
      }  
  
      unset($form['cloud_type'][$key]);
      
      $row[] =  array('data' => $action_data, 'class' => 'action-column' );
      $rows[] = $row;
    }
  }
  $output .= theme('table', $form['header']['#value'], $rows);
   //print_r($form['pager']);die;
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $reload_link = l('- Refresh Page -',
                   CLOUD_SERVER_TEMPLATES_PATH . '/getdata',
                   array( 'query' => 'src=' . CLOUD_SERVER_TEMPLATES_PATH . '/list' ) ); //, NULL ,  "src=" . CLOUD_SERVER_TEMPLATES_PATH . '/list' ) ;  
  $ref_link = array('#type'   => 'item',
                    '#prefix' => '<div id="link_reload" align="right">',
                    '#suffix' => '</div>',
                    '#value'  => $reload_link
              );  
  
  //$output .= drupal_render($ref_link);
  $output .= drupal_render($form);

  return $output; 
}


function cloud_server_templates_list_submit($form_id, $form_values) {

  $form_values = $form_values['values'];

  if ( $form_values['op'] == t('Create')) {

    return drupal_goto( CLOUD_SERVER_TEMPLATES_PATH . '/' . $form_values['cloud_context'] . '/create') ;    
  }
}

/**
 * Delete a server-template
 *
 * @param $templateid, $cloud_context
 * @return return to listing page
 */

/**
 * Delete a server-template
 * redirect to listing page
 * @param $templateid
 *        This is template-id
 * @param $cloud_context
 *        This is the sub-cloud who's template is being deleted
 * @return return a form
 */ 
function cloud_server_templates_delete($templateid='', $cloud_context) {
  _cloud_server_templates_delete($templateid) ;
 
  drupal_set_message(t('Template has been deleted successfully.')) ;
  drupal_goto(CLOUD_SERVER_TEMPLATES_PATH . '/' . $cloud_context . '/list') ;
}


/**
 * Copy a server-template
 * redirect to listing page
 * @param $templateid
 *        This is template-id
 * @param $cloud_context
 *        This is the sub-cloud who's template is being copied
 * @return return a form
 */ 
function cloud_server_templates_copy($template_id='', $cloud_context) {
 
  _cloud_server_templates_copy($template_id) ;  
  
  drupal_set_message(t('Template copied.'));
  drupal_goto(CLOUD_SERVER_TEMPLATES_PATH . '/list/' . $cloud_context) ;
}


function _cloud_server_templates_new_submit($form_id, $form_values) {

  //$form_values = $form_values['values'];

  $cloud_context = $form_values['cloud_type_select'] ;
  if ( $form_values['op'] == t('Cancel') ) {
  
    drupal_goto( CLOUD_SERVER_TEMPLATES_PATH . '/' . $cloud_context . '/list') ;

    return ;
  }
  elseif ($form_values['op'] == t('Add')) {
      
    _cloud_server_templates_insert_server_template($form_values) ;  
    
    drupal_set_message(t('Template has been saved.'));
    drupal_goto(CLOUD_SERVER_TEMPLATES_PATH . '/' . $cloud_context . '/list') ;
    
  }
  elseif ($form_values['op'] == t('Edit')) {

    $template_id = $form_values['templateid'] ;
    _cloud_server_templates_update_server_template($form_values, $template_id) ;
    
    drupal_set_message(t('Template has been updated.'));  
    drupal_goto(CLOUD_SERVER_TEMPLATES_PATH . '/' . $cloud_context . '/list') ;
  }

}


function _cloud_server_templates_view_submit($form_id, $form_values) {

//$form_values = $form_values['values'];
  $cloud_context = $form_values['cloud_context'] ;
  if ( $form_values['op'] == t('List Templates')) {  

    drupal_goto( CLOUD_SERVER_TEMPLATES_PATH . '/list/' . $cloud_context) ;

    return ;    
  }
}  


function theme_cloud_server_templates_new($form) {

  $cloud_context     = $form['cloud_type']['cloud_type_select']['#value'] ;
  $scripting_options = cloud_server_templates_get_scripting_options();

  $rows[] = array(  
    drupal_render($form['name']['name_label'] ),
    drupal_render($form['name']['name_text' ] ),
  );
  
  $rows[] = array(  
    drupal_render($form['description']['description_label'] ),
    drupal_render($form['description']['description_text' ] ),
  );
  
  $rows[] = array(  
    drupal_render($form['cloud_type']['cloud_type_label'       ] ),
    drupal_render($form['cloud_type']['cloud_type_display'     ] ),
  );
  
  $rows[] = array(
    drupal_render($form['instance_type']['instance_type_label' ] ),
    drupal_render($form['instance_type']['instance_type_select'] ),
  );
  
  if (!strstr($cloud_context, 'cp')) {
  $rows[] = array(  
    drupal_render($form['ec2_image_lbl'] ),
    drupal_render($form['ec2_image_id']) . drupal_render($form['ec2_image_id']['ec2_image_id_select'] ),
  );
  }
  else {
    $rows[] = array(  
      drupal_render($form['image_id']['image_id_label'] ),
      drupal_render($form['image_id']['image_id_select'] ),
    );
  }
  
  $rows[] = array(  
    drupal_render($form['kernel_image_lbl']                           ),
    drupal_render($form['kernel_image_id' ]                           )
  . drupal_render($form['kernel_image_id' ]['kernel_image_id_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['ramdisk_image_lbl']),
    drupal_render($form['ramdisk_image_id' ])
  . drupal_render($form['ramdisk_image_id' ]['ramdisk_image_id_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['user_name_details']['user_name_label' ] ),
    drupal_render($form['user_name_details']['user_name_text'] ),
  );
  
  $rows[] = array(
    drupal_render($form['ssh_keys_details']['ssh_keys_label' ] ),
    drupal_render($form['ssh_keys_details']['ssh_keys_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['SG_details']['SG_label' ] ),
    drupal_render($form['SG_details']['SG_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['count_details']['Count_label'] ),
    drupal_render($form['count_details']['Count_text' ] ),
  );
  
  $rows[] = array(  
    drupal_render($form['zone_details']['zone_label' ] ),
    drupal_render($form['zone_details']['zone_select'] ),
  );
  
  foreach ($scripting_options['CLOUD_SCRIPTING_TYPE_OPTIONS'] as $script => $script_val) {
    $rows[] = array(  
      drupal_render($form[$script][$script . '_label'  ] ),
      drupal_render($form[$script][$script . '_content'] ),
    );
  }  
  
  $rows[] = array(
    drupal_render($form['alert_id']['alert_id_label']   ),
    drupal_render($form['alert_id']['alert_id_content'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['user_data']['user_data_label'] ),
    drupal_render($form['user_data']['user_data_text' ] ),
  );

  $table = theme('table', NULL, $rows );
  $form['fieldset_template_info']['#children'] = $table;

//cf. Waning by Coder module: Potential problem: when FAPI element '#type' is set to 'markup' (default), '#value' only accepts filtered text, be sure to use check_plain(), filter_xss() or similar to ensure your $variable is fully sanitized.
//$form['fieldset_template_info']['list'] = array('#type' => 'markup', '#value' => $table);

  $output  = drupal_render($form['fieldset_template_info']);
  $output .= drupal_render($form['submit_buttons']);
  $output .= drupal_render($form['type_val']);  
  $output .= drupal_render($form);
  
  return $output; 
}


function theme_cloud_server_templates_view($form) {

  $cloud_context     = !empty ($form['cloud_type']['cloud_type_select']['#value']) ? $form['cloud_type']['cloud_type_select']['#value'] : '';
  $scripting_options = cloud_server_templates_get_scripting_options();
  $rows[] = array(
    drupal_render($form['name']['name_label'] ),
    drupal_render($form['name']['name_text' ] ),
  );
  
  $rows[] = array(
    drupal_render($form['description']['description_label'] ),
    drupal_render($form['description']['description_text' ] ),
  );
  
  $rows[] = array(  
    drupal_render($form['cloud_type']['cloud_type_label' ] ),
    drupal_render($form['cloud_type']['cloud_type_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['instance_type']['instance_type_label'       ] ),
    drupal_render($form['instance_type']['instance_cloud_type_select'] ),
  );
  
  if (!strstr($cloud_context, 'cp')) {
    $rows[] = array(  
      drupal_render($form['ec2_image_lbl'] ),
      drupal_render($form['ec2_image_id' ] )
    . drupal_render($form['ec2_image_id' ]['ec2_image_id_select'] ),
    );
  }
  else {
    $rows[] = array(  
      drupal_render($form['image_id']['image_id_label' ] ),
      drupal_render($form['image_id']['image_id_select'] ),
    );
  }
  
  $rows[] = array(  
    drupal_render($form['kernel_image_id']['kernel_image_id_label' ] ),
    drupal_render($form['kernel_image_id']['kernel_image_id_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['ramdisk_image_id']['ramdisk_image_id_label' ] ),
    drupal_render($form['ramdisk_image_id']['ramdisk_image_id_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['user_name_details']['user_name_label' ] ),
    drupal_render($form['user_name_details']['user_name_text'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['ssh_keys_details']['ssh_keys_label' ] ),
    drupal_render($form['ssh_keys_details']['ssh_keys_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['SG_details']['SG_label' ] ),
    drupal_render($form['SG_details']['SG_select'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['count_details']['Count_label'] ),
    drupal_render($form['count_details']['Count_text' ] ),
  );
  
  $rows[] = array(  
    drupal_render($form['zone_details']['zone_label' ] ),
    drupal_render($form['zone_details']['zone_select'] ),
  );
  
  foreach ($scripting_options['CLOUD_SCRIPTING_TYPE_OPTIONS'] as $script => $script_val) {
    $rows[] = array(  
      drupal_render($form[$script][$script . '_label'  ] ),
      drupal_render($form[$script][$script . '_content'] ),
    );
  }  
  
  $rows[] = array(  
    drupal_render($form['alert_id']['alert_id_label'  ] ),
    drupal_render($form['alert_id']['alert_id_content'] ),
  );
  
  $rows[] = array(  
    drupal_render($form['user_data']['user_data_label'] ),
    drupal_render($form['user_data']['user_data_text' ] ),
  );

  $table = theme('table', NULL, $rows );
  $form['fieldset_template_info']['#children'] = $table;

//cf. Waning by Coder module: Potential problem: when FAPI element '#type' is set to 'markup' (default), '#value' only accepts filtered text, be sure to use check_plain(), filter_xss() or similar to ensure your $variable is fully sanitized.
//$form['fieldset_template_info']['list'] = array('#type' => 'markup', '#value' => $table);  
  
  $output  = drupal_render($form['options'               ] ) ;
  $output .= drupal_render($form['submit_buttons'        ] ) ;
  $output .= drupal_render($form['fieldset_template_info'] ) ;
  $output .= drupal_render($form['type_val'              ] ) ;
  $output .= drupal_render($form['cloud_context'         ] ) ;  
  $output .= drupal_render($form);
  
  return $output; 
}
