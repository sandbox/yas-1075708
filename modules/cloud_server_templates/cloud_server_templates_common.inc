<?php

/**
 * @file
 * Provides server template functionality for each cloud sub-system.
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

/**
 * Updated by yas   2011/02/28
 * Updated by yas   2011/02/04
 * Updated by yas   2011/02/02
 */

function _cloud_server_templates_get_images_for_new_launch($image_type_value, $cloud_context='') {

  $query      = _get_describe_images_query($cloud_context) ;
  $query_args = array(
    'image_type',
    $image_type_value,
  );

  $result = db_query( $query, $query_args );

  $image_options = array();
  if ( $image_type_value == 'ramdisk'
  ||   $image_type_value == 'kernel') {

    $image_options[CLOUD_SERVER_TEMPLATES_NONE] = CLOUD_SERVER_TEMPLATES_NONE ;
  }

  while ($image = db_fetch_object($result)) {

    $image_options[$image->image_id] = $image->image_nickname ;
  }
  asort($image_options);

  return $image_options ;
}


function _cloud_server_templates_get_instance_type($cloud_context='') {

  $instance_type_options = array();

  $query = 'SELECT * FROM {' . CLOUD_SERVER_TEMPLATES_INSTANCE_TYPE_TABLE . '} c WHERE 1 and cloud_type="' . $cloud_context . ' " ' ;
  $instance_type_result = db_query( $query );

  while ($type = db_fetch_object($instance_type_result)) {

    $instance_type_options[$type->instance_type] = $type->instance_type ;
  }
  asort($instance_type_options);

  return $instance_type_options ;
}

// @TODO Check if this function is used or not.
function _cloud_server_templates_get_instance_type_query($cloud_context='') {

  if (!module_exists('cloud_pricing')) return NULL;

  return $query = 'SELECT instance_type FROM {' . CLOUD_SERVER_TEMPLATES_INSTANCE_TYPE_TABLE . '} where cloud_type = "' . $cloud_context . '" and instance_type not in (select instance_type from {' . CLOUD_PRICING_INSTANCES_TABLE . '} where cloud_type = "' . $cloud_context . '" order by display_order asc '  ;
}

function _cloud_server_templates_instance_type($cloud_context='') {

  if (!module_exists('cloud_pricing')) return NULL;
  
  $search_where = '';

  if ($cloud_context) {
    $search_where = ' and cloud_type="' . $cloud_context . ' "';
  }

  // return $query = 'SELECT * FROM {' . CLOUD_SERVER_TEMPLATES_INSTANCE_TYPE_TABLE . '} c WHERE 1 ' . $search_where . ' order by display_order asc ' ;
  return $query = 'SELECT * FROM {' . CLOUD_PRICING_INSTANCES_TABLE . '} c WHERE 1 ' . $search_where . ' ' ;
}


function _cloud_server_templates_get_default_instance_type($cloud_context='') {

  $search_where = '';

  if ($cloud_context) {
    $search_where = ' and cloud_type="' . $cloud_context . ' "';
  }

  $query = 'SELECT * FROM {' . CLOUD_SERVER_TEMPLATES_INSTANCE_TYPE_TABLE . '} c WHERE 1 ' . $search_where . ' order by display_order asc ' ;
  $instance_type_result = db_query( $query );

  while ($type = db_fetch_object($instance_type_result)) {

    // Since the instance type are inserted as per the display order
    // Return the first instance type
    return $type->instance_type ;
  }

  // No instance type present
  return FALSE ;
}


function _cloud_server_templates_get_instance_type_list($cloud_context='') {

  $query = _cloud_server_templates_instance_type($cloud_context) ;
  if ($query == NULL) return array();
  
  $instance_type_options = array();
  $instance_type_result  = db_query( $query );

  while ($type = db_fetch_object($instance_type_result)) {

    $instance_type_options[$type->instance_type] = $type->instance_type ;
  }
  asort($instance_type_options);

  return $instance_type_options ;
}


function cloud_server_templates_instance_template_create($form_submit='', $cloud_context) {

  $form['cloud_context'          ] = array('#type' => 'hidden'  , '#value' => $cloud_context    );
  $form['launch'                 ] = array('#type' => 'submit'  , '#value' => t('Launch')       );
  $form['fieldset_template_info' ] = array('#type' => 'fieldset', '#title' => t('Template Info'));
  
  $image_options = _cloud_server_templates_get_images_for_new_launch('machine') ;
  $form['image_details'        ]['image_label'    ] = array('#type' => 'item'  , '#title'   => t('EC2 Image')    );
  $form['image_details'        ]['image_select'   ] = array('#type' => 'select', '#options' => $image_options    );

  $kernel_options = _cloud_server_templates_get_images_for_new_launch('kernel') ;
  $form['kernel_details'       ]['kernel_label'   ] = array('#type' => 'item'  , '#title'   => t('Kernel image') );
  $form['kernel_details'       ]['kernel_select'  ] = array('#type' => 'select', '#options' => $kernel_options   );

  $ramdisk_options = _cloud_server_templates_get_images_for_new_launch('ramdisk') ;
  $form['RAMDisk_details'      ]['RAMDisk_label'  ] = array('#type' => 'item'  , '#title'   => t('Ramdisk image'));
  $form['RAMDisk_details'      ]['RAMDisk_select' ] = array('#type' => 'select', '#options' => $ramdisk_options  );

  $form['nickname_details'     ]['nickname_label' ] = array('#type' => 'item'  , '#title' => t('Nickname')       );
  $form['nickname_details'     ]['nickname_text'  ] = array('#type' => 'textfield' );

  $form['count_details'        ]['Count_label'    ] = array('#type' => 'item', '#title' => t('Count')            );
  $form['count_details'        ]['Count_text'     ] = array('#type' => 'textfield' );

  $ssh_key_options = _get_sshkeys($cloud_context) ;
  $form['ssh_keys_details'     ]['ssh_keys_label' ] = array('#type' => 'item'  , '#title' => t('SSH Key')        );
  $form['ssh_keys_details'     ]['ssh_keys_select'] = array('#type' => 'select', '#options' => $ssh_key_options  );

  $security_group_options = _get_security_groups($cloud_context) ;
  $form['SG_details'           ]['SG_label'       ] = array('#type' => 'item'  , '#title' => t('Security Group(s)') );
  $form['SG_details'           ]['SG_select'      ] = array('#type' => 'select', '#options' => $security_group_options , '#multiple' => TRUE );

  $instance_type_options = _cloud_server_templates_get_instance_type() ;
  $form['Instance_Type_details']['Instance_Type_label' ] = array('#type' => 'item'  , '#title' => t('Instance type')       );
  $form['Instance_Type_details']['Instance_Type_select'] = array('#type' => 'select', '#options' => $instance_type_options );

  $zone_options = _get_zones($cloud_context) ;
  $form['zone_details'         ]['zone_label'     ] = array('#type' => 'item'  , '#title'   => t('Availability Zone') );
  $form['zone_details'         ]['zone_select'    ] = array('#type' => 'select', '#options' => $zone_options          );

  $form['user_data_datails'    ]['user_data_label'] = array('#type' => 'item'  , '#title' => t('User data')  );
  $form['user_data_datails'    ]['user_data_text' ] = array('#type' => 'textarea'                            );

  $form['submit_buttons'       ] = array(
    '#prefix' => '<span class="clear"></span><div class="container-inline"><div class="buttons">',
    '#type'   => 'fieldset',  
    '#suffix' => '</div></div>',
  );

  $form['submit_buttons']['Add'   ] = array( '#type' => 'submit', '#value' => t('Add'   ) );
  $form['submit_buttons']['Cancel'] = array( '#type' => 'submit', '#value' => t('Cancel') );
  
  return $form ;
}


function theme_cloud_server_templates_instance_template_create($form) {

  $cloud_context = $form['cloud_context']['#value']  ;
  $output  = cloud_server_templates_notify( '' , 'new_theme', $cloud_context , $form ) ;
  $output .= drupal_render($form['cloud_context']);

  return $output ;
}


function cloud_server_templates_instance_template_create_validate($form_id, $form_values) {

  $form_values = $form_values['values'];
  if ( $form_values['op'] == t('Add') ) {

    $count =  $form_values['Count_text'] ;
    if ( !preg_match(CLOUD_VALID_DIGIT, $count)) {

      form_set_error('',  t('You must enter valid Count'));

    }
    elseif ( empty($form_values['nickname_text']) ) {

      form_set_error('',  t('You must enter Nickname'));
    }
  }

  return;
}


function cloud_server_templates_instance_template_create_submit($form_id, $form_values) {

  $form_values = $form_values['values'];
  if ( $form_values['op'] == t('Launch')
  ||   $form_values['op'] == t('Cancel') ) {

    drupal_goto(CLOUD_SERVER_TEMPLATES_PATH . '/instance_templates') ;

    return ;
  }
  elseif ($form_values['op'] == t('Add')) {

    _cloud_server_templates_instance_template_save($form_values) ;
    drupal_set_message(t('Instance template has been saved.'));
    drupal_goto(CLOUD_SERVER_TEMPLATES_PATH . '/instance_templates') ;
  }

  return;
}


function cloud_server_templates_get_instance_template_form() {

  global $user;
  
  $column      = 'template_nickname'    ;
  $form_values = $form_values['values'] ;
  $filter      = $form_values['filter'] ;

  $options = array(
    t('Nickname'     ) ,
    t('Instance Type') ,
  );

  // TODO: change variable_get (Jamir)
  $filter     = variable_get( $user->uid . 'instances_template_filter_value', ''     );
  $filter_col = variable_get( $user->uid . 'instances_template_operation_value' ,  0 );
  $filter_col = isset($filter_col) && $filter_col ? $filter_col : 0; // default: Template Name
  
  if ( $filter_col == 0 )  {
    $column  = 'Nickname' ;
    $sql_col = 'template_nickname' ;
  }
  elseif ($filter_col == 1 ) {
    $column  = 'Instance Type' ;
    $sql_col = 'instance_type' ;
  }

  if ( isset($filter) ) {

    $query_args[] = $sql_col ;
    $query_args[] = $filter ;
  }
  else {
    $filter = ' 1 ' ;
    $query_args[] = ' ' ;
  }

  $form['options'] = array(
    '#prefix' => '<div class="container-inline">',
    '#type' => 'fieldset',
    '#suffix' => '</div>',
 // '#title' => t('Operations'), 
  );
  
  asort($options);
  $form['options']['label'    ] = array( '#type' => 'item'     , '#title' => t('Filter'));
  $form['options']['filter'   ] = array( '#type' => 'textfield', '#size'  => 40, '#default_value' => $filter );
  $form['options']['submit'   ] = array( '#type' => 'submit'   , '#value' => t('Apply' ));

  if (user_access('create template') ) {
    $form['options']['template'] = array('#type' => 'submit', '#value' => t('Create'));
  }

  $form['header'] = array(

    '#type'  => 'value',  
    '#value' => array(
      array('data' => t('Nickname'    )  , 'field' => 'template_nickname', 'sort' => 'asc' ),
      array('data' => t('ImageId'     ) ),
      array('data' => t('Count'       )  , 'field' => 'count_instances' ),
      array('data' => t('Instancetype')  , 'field' => 'instance_type'   ),
      array('data' => t('Action'      ) ),
    )
  );

  $query  = _cloud_server_templates_get_describe_instance_templates_query() ;
  $query .= tablesort_sql( $form['header']['#value'] ) ;

  $result = pager_query( $query , CLOUD_SERVER_TEMPLATES_PAGER_LIMIT, 0, NULL, $query_args );

  $destination = drupal_get_destination();

  while ($instance_template = db_fetch_object($result)) {

    $form['Nickname'    ][$instance_template->template_id] = array( array('#value' => l( $instance_template->template_nickname , CLOUD_SERVER_TEMPLATES_PATH . '/instance_templates/describe/info',  array('query' => 'template_id=' . urlencode($instance_template->template_id)) ) ) );//,  array(),   'template_id=' . urlencode($instance_template->template_id) ,  NULL,  FALSE,  TRUE ) ) );
    $form['hdnName'     ][$instance_template->template_id] = array( '#type' => 'hidden', '#value' => addslashes($instance_template->template_nickname) );
    if ( isset($instance_template->image_nickname) == FALSE
    ||   empty($instance_template->image_nickname) )
    $instance_template->image_nickname = $instance_template->image_id ;
    $form['Image'       ][$instance_template->template_id] = array( array('#value' => t( $instance_template->image_nickname ) ) );
    $form['Count'       ][$instance_template->template_id] = array( array('#value' => t( $instance_template->count_instances ) ) );
    $form['Instancetype'][$instance_template->template_id] = array( array('#value' => t( $instance_template->instance_type ) ) ) ;
    $form['TemplateName'][$instance_template->template_id] = array( '#type' => 'hidden', '#value' => $instance_template->template_nickname ) ;
    $form['KeyName'     ][$instance_template->template_id] = array( '#type' => 'hidden', '#value' => $instance_template->key_name ) ;
  }

  return $form;
}

function cloud_server_templates_get_instance_template_form_submit($form_id, $form_values) {

  $form_values = $form_values['values'];
  if ( $form_values['op'] == t('Create')) {

    return drupal_goto( CLOUD_SERVER_TEMPLATES_PATH . '/instance_templates/template_new') ;
  }
  else {
    variable_set( $user->uid . 'instances_template_filter_value'   , $form_values['filter'   ] );
    variable_set( $user->uid . 'instances_template_operation_value', $form_values['operation'] );
  }
  return;
}


function cloud_server_templates_instance_delete($cloud_context, $template_id='') {

  _cloud_server_templates_instance_template_delete($template_id) ;

  drupal_set_message(t('Instance Template deleted'));
  drupal_goto(CLOUD_SERVER_TEMPLATES_PATH . '/instance_templates') ;

  return;
}

function cloud_server_templates_get_scripting_options() {

  $_CLOUD_SCRIPTING_TYPE_OPTIONS = array(
    'boot'        => t('Boot'       ),
    'operational' => t('Operational'),
    'termination' => t('Termination')
  ) ;

  asort($_CLOUD_SCRIPTING_TYPE_OPTIONS);
  
  $scripting_options = array('CLOUD_SCRIPTING_TYPE_OPTIONS' => $_CLOUD_SCRIPTING_TYPE_OPTIONS);

  return $scripting_options;
}
