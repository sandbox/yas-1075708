<?php

/**
 * @file
 * Provides server template functionality for each cloud sub-system.
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

// scripting functions 

/**
 * Returns a query to get list of server-templates
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a sql-query
 */
function _cloud_server_templates_get_all_server_template_query($cloud_context='') {

  return $query = 'SELECT * FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} c WHERE %s like \'%%%s%%\' and deleted=0 ' ;
}

/**
 * Returns a query to get list of server-templates
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a sql-query
 */ 
function _cloud_server_templates_get_server_template($cloud_context='') {

  return $query = 'SELECT * FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} c WHERE %s = \'%s\' and deleted=0 ' ;
}

/**
 * Returns a query to get list of server-templates by giving template-id
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $template_id
 *        This is the template-id of template
 * @return return a cloud-type
 */
function _cloud_server_templates_get_cloud($template_id='') {

  $query = 'SELECT c.* FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} c WHERE template_id=' . $template_id ;
  $result = db_query( $query  );
  $template_obj = db_fetch_object($result);
  return $template_obj->cloud_type;
}

// scripting functions 

function _cloud_server_templates_get_server_templatelist_count($cloud_context='', $params = array()) {

  global $user;
  $search_where = '';
  $group_by = '';
  $order_by = '';

  if (in_array('actor', $user->roles)) {
    $search_where = ' and a.uid=' . $user->uid;
  }
  if ($cloud_context && empty($params['all']) ) {
    $search_where .= ' and a.cloud_type="' . $cloud_context  . '" ';
  }

  $query = 'SELECT count(*) as template_count FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} a
               WHERE 1 ' . $search_where . ' and a.deleted=0 ' ;

  $tmpl_count = 0 ;
  $result = db_query( $query, NULL );
  while ($tmpl = db_fetch_object($result)) {

       $tmpl_count = $tmpl->template_count ;
   }

  return $tmpl_count ;
}


/**
 * Returns a query to get list of server-templates
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a sql-query
 */ 
function _cloud_server_templates_get_server_templatelist($cloud_context='', $params = array()) {

  global $user;
  $search_where = '';
  $group_by = '';
  $order_by = '';
  
  if (in_array('actor', $user->roles)) {
    $search_where = ' and a.uid=' . $user->uid;
  }
  if ($cloud_context && empty($params['all']) ) {
    $search_where .= ' and a.cloud_type="' . $cloud_context  . '" ';
  }

  $search_where .= ' and %s like \'%%%s%%\' ';  

  return $query = 'SELECT distinct a.template_id as serverTemplateId, a.status as template_status,  a.created as template_created,  a.updated as template_updated, a.* FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} a
               WHERE 1 ' . $search_where . ' and a.deleted=0 ' ;
}

/**
 * delete a server template from database
 * This function deletes entry from database and updates audit logs.
 *  * 
 * @param $template_id
 *        This is the template-id which is to be deleted
 * @param $cloud_context
 *        This is the sub-cloud who's template is being deleted
 * @return return
 */
function _cloud_server_templates_delete($id, $cloud_context='') {
  
  $query      = _cloud_server_templates_get_server_template_detail() ; 
  $query_args = array(
    'a.template_id',
    $id,    
  );
  $result = db_query( $query, $query_args );

  $count  = 0 ;
  $template_nickname = '';
  if ($result) {
    $template_obj = db_fetch_object($result);
    $template_nickname = $template_obj->template_nickname;
  } 
//print_r($template_obj);die;   
    
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('Template has been deleted: @template_nickname', array('@template_nickname' => $template_nickname)),
      'link'    => '',
    )
  );
  
  $delete_query = 'update {' . CLOUD_SERVER_TEMPLATES_TABLE . '} set deleted=1
                  where template_id=\'%s\'
          '; 
  $query_args = array(
    $id,
  );
  //print $delete_query;die;
  db_query( $delete_query ,  $query_args );  
  
  return;
}
 
/**
 * To get first element of image
 * This function uses a split logic to return first element from image_name
 *
 * @param $image_name
 *        This is the image name
 * @return return a first element from name
 */
function _cloud_server_templates_get_first_element_image($image_name) {

  if (isset($image_name) ) {
    $image_name = trim($image_name) ;
  }
  
  if ( isset($image_name) === FALSE  || empty($image_name) || $image_name === CLOUD_SERVER_TEMPLATES_NONE )
    return CLOUD_SERVER_TEMPLATES_NONE ;

  $start_brac = strrpos($image_name , '(') ;
  $end_brac = strrpos($image_name , ')') ;
  $len = $end_brac - $start_brac ;
  
  $image_id = substr( $image_name , $start_brac + 1 , $len - 1  ) ;
  
  return $image_id ;
}

/**
 * Insert a server template in database
 * This function insert entry in database and audit logs as well.
 *
 * @param $form_values
 *        This is the form-values submitted by new server template form
 * @return return a last inserted template-id
 */ 
function _cloud_server_templates_insert_server_template($form_values) {

  global $user;
  
  $insert_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_TABLE . '} 
    ( `template_nickname` , 
      `description`       , 
      `cloud_type`        , 
      `instance_type`      , 
      `image_id`           ,
      `count_instances`   ,
      `user_name`           ,
      `key_name`           ,
      `kernel_id`          ,
      `ramdisk_id`         ,
      `group_id`           ,
      `user_data`         ,
      `placement`         , 
      `created`           , 
      `updated`, `uid` 
    )  values 
  '; 
          
  $insert_query = $insert_query . "  (  '%s' , '%s' , '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' , '%s' ) " ;
  $query_args = array();
  $server_template_id = uuid();
//$query_args[] =  $server_template_id;
  $query_args[] =  $form_values['name_text'           ] ;
  $query_args[] =  $form_values['description_text'    ] ;
  $query_args[] =  $form_values['cloud_type_select'   ] ;
  $query_args[] =  isset( $form_values['instance_type_select'] )  ? $form_values['instance_type_select'] : '' ;
  $query_args[] = (!empty($form_values['image_id_select']) )
                ?  $form_values['image_id_select']
                :( _cloud_server_templates_get_first_element_image($form_values['ec2_image_id_select']));
  $query_args[] =  isset( $form_values['Count_text'] ) ? $form_values['Count_text'] : '' ;
  $query_args[] =  $form_values['user_name_text'     ] ;
  $query_args[] =  $form_values['ssh_keys_select'     ] ;
  
  $query_args[] = _cloud_server_templates_get_first_element_image(isset($form_values['kernel_image_id_select']) ? $form_values['kernel_image_id_select'] : '' );
  $query_args[] = _cloud_server_templates_get_first_element_image(isset( $form_values['ramdisk_image_id_select'] ) ? $form_values['ramdisk_image_id_select'] : '' ) ;

  if (empty($form_values['SG_select']) ) {
    $query_args[] = '' ;
  }
  else {
    $query_args[] = implode(',', $form_values['SG_select'] );
  }

  $query_args[] = isset( $form_values['user_data_text'] ) ? $form_values['user_data_text'] : '' ;
  $query_args[] = $form_values['zone_select'   ] ;
  $query_args[] = date('c') ; // UTC
  $query_args[] = date('c') ; // UTC
  $query_args[] = $user->uid;

  db_query( $insert_query, $query_args );  
  
  $server_template_id = _cloud_server_templates_last_inserted_id();
  if (module_exists('cloud_scripting')) {

    $scripting_options = cloud_server_templates_get_scripting_options();

    foreach ($scripting_options['CLOUD_SCRIPTING_TYPE_OPTIONS'] as $script => $script_val) {  
      $i=0;
      if ( isset($form_values[$script . '_selected']) &&
              is_array($form_values[$script . '_selected'])
      && count(    $form_values[$script . '_selected']) > 0 ) {
        foreach (  $form_values[$script . '_selected'] as $script_id ) {
  
          //insert templates scripts
          $_insert_scripts_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} 
            ( `id`                 ,  
              `server_template_id` , 
              `script_id`          , 
              `order`              ,
              `created`            , 
              `updated` 
            )  values 
          '; 
                  
          $_insert_scripts_query = $_insert_scripts_query . "  (  '%s', '%s', '%s', '%s', '%s', '%s' ) " ; 
    
          $query_args = array(
    
            uuid()              ,
            $server_template_id ,
            $script_id          ,
            $i                  ,
            date('c')           , // UTC
            date('c')           , // UTC
          );
    
          $i++;    
          db_query( $_insert_scripts_query, $query_args );
        }  
      }
    }
  }

  if (module_exists('cloud_alerts')) {
    if (isset($form_values['alert_id_selected']) && is_array($form_values['alert_id_selected']) && count($form_values['alert_id_selected'])>0) {
      foreach ($form_values['alert_id_selected'] as $alert_id) {
        //insert templates alerts
        $_alerts_inserts_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_ALERTS_TABLE . '} 
          ( `id` ,  
            `server_template_id` , 
            `alert_id` , 
            `created` , 
            `updated` 
          )  values 
        '; 
                
        $_alerts_inserts_query = $_alerts_inserts_query . " (  '%s', '%s', '%s', '%s', '%s' ) " ; 
        $id = uuid();
        $query_args = array(
          $id                 ,
          $server_template_id ,
          $alert_id           ,
          date('c')           , // UTC
          date('c')           , // UTC
        );
        db_query( $_alerts_inserts_query, $query_args );
      }  
    }
  }
  
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('New Template has been added: @server_template_nickname', array('@server_template_nickname' => $form_values['name_text'])), 
      'link'    => '', // 'design/server_templates/create&id=' . $server_template_id
    )
  );
  return;
}

/**
 * update a server template to database
 * This function update entry in database and audit logs as well.
 *
 * @param $form_values
 *        This is the form-values submitted by edit server template form
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a updated template-id
 */
function _cloud_server_templates_update_server_template($form_values, $server_template_id='', $cloud_context='') {
             
  $update_query = 'UPDATE {' . CLOUD_SERVER_TEMPLATES_TABLE . '} 
    set `template_nickname`=\'%s\' ,  
        `description`=\'%s\' ,  
        `cloud_type`=\'%s\' ,  
        `instance_type`=\'%s\' ,  
        `image_id`=\'%s\' , 
        `count_instances`=\'%s\' ,  
        `user_name`=\'%s\' ,
        `key_name`=\'%s\' ,
        `kernel_id`=\'%s\' ,  
        `ramdisk_id`=\'%s\' ,   
        `group_id`=\'%s\' ,   
        `user_data`=\'%s\' , 
        `placement`=\'%s\' , 
        `updated`=\'%s\' 
         where template_id=\'%s\''; 

  $query_args   = array();
  $query_args[] =  $form_values['name_text'           ] ;
  $query_args[] =  $form_values['description_text'    ] ;
  $query_args[] =  $form_values['cloud_type_select'   ]
                ?  $form_values['cloud_type_select'   ]
                :  $form_values['cloud_type_val'      ] ;
  $query_args[] =  $form_values['instance_type_select'] ;
  $query_args[] = isset($form_values['image_id_select'])
                ?  $form_values['image_id_select'     ]
                : ( _cloud_server_templates_get_first_element_image($form_values['ec2_image_id_select'] ) ) ;
  $query_args[] =  $form_values['Count_text'          ] ;
  $query_args[] =  $form_values['user_name_text'     ] ;
  $query_args[] =  $form_values['ssh_keys_select'     ] ;
  $query_args[] = _cloud_server_templates_get_first_element_image($form_values['kernel_image_id_select' ]) ; 
  $query_args[] = _cloud_server_templates_get_first_element_image($form_values['ramdisk_image_id_select']) ; 

  if (empty($form_values['SG_select']) ) {
    $query_args[] = '' ;
  }
  else {
        $query_args[] = implode(',', $form_values['SG_select'] );
  }

  $query_args[] = $form_values['user_data_text'] ;
  $query_args[] = $form_values['zone_select'   ] ;
  $query_args[] = date('c') /* UTC */            ;
  $query_args[] = $server_template_id            ;
  //print_r($query_args);die;
  db_query( $update_query, $query_args );  
  
  if (module_exists('cloud_scripting')) {

    //delete old templates scripts
    $delete_scripts_query = 'update {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} set deleted=1
                  where server_template_id=\'%s\''; 
    $query_args = array(
      $server_template_id,
    );
    db_query( $delete_scripts_query, $query_args );
  }

  if (module_exists('cloud_alerts')) {
  //delete old templates alerts
    $delete_alerts_query = 'update {' . CLOUD_SERVER_TEMPLATES_ALERTS_TABLE . '} set deleted=1
                  where server_template_id=\'%s\''; 
    $query_args = array(
      $query_args[] = $server_template_id,
    );
    db_query( $delete_alerts_query, $query_args );
  }

  if (module_exists('cloud_scripting')) {
    $scripting_options = cloud_server_templates_get_scripting_options();
    //insert new templates scripts
    foreach ($scripting_options['CLOUD_SCRIPTING_TYPE_OPTIONS'] as $script => $script_val) {  
      $i=0;
      if (is_array($form_values[$script . '_selected']) && count($form_values[$script . '_selected'])>0) {
      foreach ($form_values[$script . '_selected'] as $script_id) {
        
        //insert templates scripts
        $_insert_scripts_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} 
          ( `id` ,  
            `server_template_id` , 
            `script_id` , 
            `order` , 
            `created` , 
            `updated` 
          )  values 
        '; 
                
        $_insert_scripts_query = $_insert_scripts_query . "  (  '%s', '%s', '%s', '%s', '%s', '%s' ) " ; 
        $query_args = array(
          uuid()             ,
          $server_template_id,
          $script_id         ,
          $i                 ,
          date('c')          , // UTC
          date('c')          , // UTC
        );
        $i++;
        db_query( $_insert_scripts_query, $query_args );
      }
    }  
  }
  if ( module_exists('inputs')) {
  //remove input parameters values for deleted scripts  
  $delete_inputsfromcripts_query = 'update {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} a set a.deleted=1
                  where a.template_id=' . $server_template_id . ' and a.param_id in (select param_id from ' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . ' c where c.script_id not in (select script_id from cloud_server_templates_scripts d where d.server_template_id =' . $server_template_id . ' and d.deleted=0))
          '; 
  db_query( $delete_inputsfromcripts_query);
  }
  /////
  }
  
  if (module_exists('cloud_alerts')) {
    if (is_array($form_values['alert_id_selected'])
    &&     count($form_values['alert_id_selected']) > 0) {
      foreach ($form_values['alert_id_selected'] as $alert_id) {
        //insert templates alerts
        $_alerts_inserts_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_ALERTS_TABLE . '} 
          ( `id` ,  
            `server_template_id` , 
            `alert_id` , 
            `created` , 
            `updated` 
          )  values 
        '; 
                
        $_alerts_inserts_query = $_alerts_inserts_query . "  (  '%s', '%s', '%s', '%s', '%s' ) " ; 
        $query_args = array(
          uuid(),
          $server_template_id ,
          $alert_id ,
          date('c') , // UTC
          date('c') , // UTC
        );
        db_query( $_alerts_inserts_query ,   $query_args );
      }
    }  
  }
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => 'Template "' . $form_values['name_text'] . '" has been modified.', 
      'link'    => '', //'design/server_templates/create&id=' . $server_template_id
    )
  );

  return;
}

function _cloud_server_templates_last_inserted_id($cloud_context='') {
  
  $query_args[] = '' ;
  $query_args[] = '' ;
  $result = db_query('select max(template_id) as maxtemplate_id from {' . CLOUD_SERVER_TEMPLATES_TABLE . '} ') ; 
  
  $db_obj =  db_fetch_object($result);
  return $db_obj->maxtemplate_id;
  
}



function _cloud_server_templates_get_server_template_details($cloud_context='') {

  return $query = 'SELECT c.kernel_id, c.ramdisk_id, c.* FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} c WHERE template_id=\'%s\'  ' ;
}

function _cloud_server_templates_get_server_template_detail($edit_or_view=FALSE, $cloud_context='') {
  $search_where = '';
  return $query =  'SELECT *
  FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} a
           WHERE 1 ' . $search_where .' and a.deleted=0 '  ;
}

/**
 * Copy a server template in database
 * This function copy entry in database and update audit logs as well.
 *
 * @param $template_id
 *        This is the template-id which is to be copied
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a last inserted template-id
 */
function _cloud_server_templates_copy($template_id) {
  $query = _cloud_server_templates_get_server_template_details() ; 
  $query_args[] = $template_id ;
  
  $result = db_query( $query, $query_args );
  
  $count = 0 ;
  
  $insert_args = array();
  
  while ($key = db_fetch_object($result)) {
  
    $insert_args[] = t('Copy of @name_@time', array(
                               '@name' => $key->template_nickname,
                               '@time' => time())
                      ) ;

    $insert_args[] = $key->description ;
    $insert_args[] = $key->image_id ;
    $insert_args[] = $key->count_instances ;
    $insert_args[] = $key->key_name ;
    $insert_args[] = $key->cloud_type ;
    $insert_args[] = $key->instance_type ;
    $insert_args[] = $key->placement ;
    $insert_args[] = $key->kernel_id ;
    $insert_args[] = $key->ramdisk_id ;
    $insert_args[] = $key->user_data ;
    $insert_args[] = $key->group_id ;
    $insert_args[] = $key->uid ;
    $insert_args[] = 1 ;
    $insert_args[] = 0 ;
    $insert_args[] = date('c') ; // UTC
    
    cloud_audit_user_activity(array(
        'type'    => 'user_activity', 
        'message' => t('Template "@template_nickname" has been Copied.', array('@template_nickname' => $key->template_nickname)), 
        'link'    => '', // 'design/server_templates/create&id=' . $server_template_id
      )
    );
    
    $count++ ;
    break ;
  }

  if ($count == 0 )
    return FALSE;    
  
  $insert_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_TABLE . '} 
                  ( `template_nickname` ,  
                    `description`, `image_id` , 
                    `count_instances` , 
                    `key_name`, 
                    `cloud_type`, 
                    `instance_type`, 
                    `placement`, 
                    `kernel_id`, 
                    `ramdisk_id`, 
                    `user_data`, 
                    `group_id`,
                    `uid`, 
                    `status`,
                    `deleted`,
                    `created`
                  )  values 
          ';

  $insert_query .= "  ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ) " ; 
  db_query( $insert_query ,   $insert_args );
  
  if (module_exists('cloud_scripting')) {
    // copy scripts and alerts as well  
    $query = _cloud_server_templates_get_server_for_copy($edit=TRUE) ; 
    $query_args = array(
      'a.template_id',
      $template_id   ,  
    );
    $result = db_query( $query, $query_args );
  
    while ($template_obj = db_fetch_object($result)) {
       $selected_scripts_ids[$template_obj->script_id] = $template_obj->order;
       $selected_alertsIds[$template_obj->alert_id] = $template_obj->alert_id;
     }
    
    
    $newtemplate_id = _cloud_server_templates_last_inserted_id();
    //copy scripts
    $_insert_scripts_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} 
                        ( `id` ,  
                          `server_template_id` , 
                          `script_id` , 
                          `order` , 
                          `created` , 
                          `updated` 
                        )  values 
                ';       
    $query_args = array();
    $_insert_scripts_params = '' ;
    foreach ($selected_scripts_ids as $script_id => $order) {
            
      $_insert_scripts_params .= "  (  '%s', '%s', '%s', '%s', '%s', '%s' ) ," ; 
      $query_args[] = uuid();
      $query_args[] = $newtemplate_id ;
      $query_args[] = $script_id ;
      $query_args[] = $order ;
      $query_args[] = date('c') ; // UTC
      $query_args[] = date('c') ; // UTC    
    }  
    $_insert_scripts_query .= $_insert_scripts_params;          
    $_insert_scripts_query = substr($_insert_scripts_query,  0,  -1);  
    db_query( $_insert_scripts_query ,   $query_args );
    ////
  }
  
  if (module_exists('cloud_alerts')) {
  //copy alerts
  $_alerts_inserts_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_ALERTS_TABLE . '} 
                    ( `id` ,  
                      `server_template_id` , 
                      `alert_id` , 
                      `created` , 
                      `updated` 
                    )  values 
            ';       
  $query_args = array();
  $_alerts_inserts_params = ''; 
  foreach ($selected_alertsIds as $alert_id => $alert_id) {
          
    $_alerts_inserts_params .= "  (  '%s', '%s', '%s', '%s', '%s' ) ," ; 
    $id = uuid();
    $query_args[] = $id;
    $query_args[] = $newtemplate_id ;
    $query_args[] = $alert_id ;
    $query_args[] = date('c') ; // UTC
    $query_args[] = date('c') ; // UTC    
  }        
  $_alerts_inserts_query .= $_alerts_inserts_params;    
  $_alerts_inserts_query = substr($_alerts_inserts_query,  0,  -1);  
  db_query( $_alerts_inserts_query ,   $query_args );
  ///
  }
  
  return TRUE;  
}

/**
 * Returns a query to get list of server-templates
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a sql-query
 */
function _cloud_server_templates_get_describe_all_templates_query($cloud_context='') {

  
  
   return $query =  'SELECT c.template_id,  c.key_name,  c.template_nickname,  c.count_instances ,  c.image_id,  c.instance_type FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} c where deleted=0 ' ;
}

/**
 * Returns a query to get list of server-templates by giving template-id
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $template_id
 *        This is the template-id of template
 * @return return a sql query
 */
function _cloud_server_templates_get_info($template_id) {

    $query = 'SELECT c.* FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} c WHERE template_id=\'%s\'  '  ; 
    $query_args = array(
      $template_id ,  
    ) ;
    $result = db_query( $query, $query_args );
    $template_obj = db_fetch_object($result);
    
    return $template_obj ;
}

/**
 * Returns a query to get list of server-templates
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a sql-query
 */
function cloud_server_templates_get_server_templates_by_cloud_db($cloud_context='') {  

  $query = 'SELECT template_id, template_nickname FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . "} c WHERE deleted=0 and cloud_type='%s' ";
  $query_args = array(
    $cloud_context,
  ) ;
  // $query       .= tablesort_sql( array(array('field' => 'template_nickname', 'sort' => 'asc')) ) ;
  $result       = db_query( $query, $query_args );

  $template_options = array() ;
  while ($tmpl = db_fetch_object($result)) {

    $template_options[$tmpl->template_id] = $tmpl->template_nickname ;
  }
  asort( $template_options);
  return $template_options ;
}

/**
 * Returns a query to get list of server-templates
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a sql-query
 */
function _cloud_server_templates_get_server_template_details_by_id($cloud_context='') {

  return $query = 'SELECT c.kernel_id, c.ramdisk_id, c.* FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} c WHERE template_id=\'%s\'  ' ;
}

/**
 * Returns a query to get list of server-templates by giving template-id
 * This function build a query and search parameters to sql assign during function call.
 *
 * @param $template_id
 *        This is the template-id of template
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a sql query
 */
function _cloud_server_templates_get_server_template_details_by_cloud($template_id='', $cloud_context='') {
  return $query = 'SELECT c.kernel_id, c.ramdisk_id, c.* FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} c  WHERE template_id="' . $template_id . '"  ' ;
}

/**
 * Copy a server template in database
 * This function copy entry in database and update audit logs as well.
 *
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a last inserted template-id
 */
function _cloud_server_templates_get_server_for_copy($edit_or_view=FALSE, $cloud_context='') {

  global $user;  
 
  if (in_array('actor', $user->roles)) {
    //$search_where = ' and a.uid=' . $user->uid;
  }

  $search_where = '' ;
  $group_by = '' ;
  $order_by = '' ;
  
  if ($cloud_context) {
    $search_where = ' and a.cloud_type="' . $cloud_context  . '" ';
  }
  
  if (!$edit_or_view) {
    $group_by = 'group by a.template_id';  
    $search_where .= ' and %s like \'%%%s%%\' ';  
  }
  else {
    $order_by = 'order by b.order';
    $search_where .= ' and %s = \'%s\' ';
  }
  
  if ( module_exists('cloud_scripting') && module_exists('cloud_alerts')) {
  return $query =  'SELECT distinct a.template_id as serverTemplateId, d.name as script_name, d.script_template as script_template, e.name as alert_name, a.status as template_status,  a.created as template_created,  a.updated as template_updated,    a.*, b.*, c.*, d.type
                    FROM {' . CLOUD_SERVER_TEMPLATES_TABLE         . '} a
               LEFT JOIN {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} b on a.template_id=b.server_template_id and b.deleted=0
               LEFT JOIN {' . CLOUD_SCRIPTING_TABLE                . '} d on b.script_id=d.id
               LEFT JOIN {' . CLOUD_SERVER_TEMPLATES_ALERTS_TABLE  . '} c on a.template_id=c.server_template_id and c.deleted=0
               LEFT JOIN {' . CLOUD_ALERTS_TABLE                   . '} e on c.alert_id=e.id
           WHERE 1 ' . $search_where .' and a.deleted=0 ' . $group_by . $order_by ;
  }         
}


/**
 * Returns a query to get list of server-template's scripts
 * Generate a Sql - query for template script list
 * @param 
 * @return return a sql-query
 */
function _cloud_server_templates_get_server_templates_scripts() {

  global $user;
  
  $group_by = '';
  $order_by = '';
  
  if (in_array('actor', $user->roles)) {
    //$search_where = ' and a.uid='.$user->uid;
  }

  $edit_or_view = isset($edit_or_view)  ? $edit_or_view :  FALSE ;
  $search_where = '' ;
  
  if (!$edit_or_view) {
   $search_where .= ' and %s like \'%%%s%%\' ';  
  }
  else {
    $search_where .= ' and %s = \'%s\' ';
  }
  
  return $query =  'SELECT distinct a.template_id as serverTemplateId,d.name as script_name,d.script_template as script_template,a.status as template_status, a.created as template_created, a.updated as template_updated,   a.*,b.*,d.type FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} a
               LEFT JOIN {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} b on a.template_id=b.server_template_id and b.deleted=0
               LEFT JOIN {' . CLOUD_SCRIPTING_TABLE . '} d on b.script_id=d.id
               WHERE 1 ' . $search_where . ' and a.deleted=0 ' . $group_by . $order_by ;
}

/**
 * Returns a query to get list of server-template's alerts
 * Generate a Sql - query for template alerts list
 * @param 
 * @return return a sql-query
 */
function _cloud_server_templates_get_server_templates_alerts() {

  global $user;
  
  $group_by = '';
  $order_by = '';
  
  if (in_array('actor', $user->roles)) {
    //$search_where = ' and a.uid='.$user->uid;
  }

  $edit_or_view = isset($edit_or_view) ? $edit_or_view : FALSE ;
  $group_by = '' ;
  $search_where = '' ;
  
  if (!$edit_or_view) {
   $group_by = 'group by a.template_id';  
   $search_where .= ' and %s like \'%%%s%%\' ';  
  }
  else {
    $order_by = 'order by b.order';
    $search_where .= ' and %s = \'%s\' ';
  }
  
  return $query =  'SELECT distinct a.template_id as serverTemplateId,e.name as alert_name,a.status as template_status, a.created as template_created, a.updated as template_updated,   a.*,c.* FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} a
               LEFT JOIN {' . CLOUD_SERVER_TEMPLATES_ALERTS_TABLE . '} c on a.template_id=c.server_template_id and c.deleted=0
               LEFT JOIN {' . CLOUD_ALERTS_TABLE . '} e on c.alert_id=e.id
               WHERE 1 ' . $search_where . ' and a.deleted=0 ' . $group_by . $order_by ;
}


function _cloud_server_templates_get_alerts_by_server_templates() {

  global $user;

  $group_by = '';
  $order_by = '';

  if (in_array('actor', $user->roles)) {
    //$search_where = ' and a.uid='.$user->uid;
  }

  $edit_or_view = isset($edit_or_view) ? $edit_or_view : FALSE ;
  $group_by = '' ;
  $search_where = '' ;

  if (!$edit_or_view) {
   $search_where .= ' and %s like \'%%%s%%\' ';
  }
  else {
    $search_where .= ' and %s = \'%s\' ';
  }

  return $query =  'SELECT a.template_id as serverTemplateId,e.name as alert_name,a.status as template_status, a.created as template_created, a.updated as template_updated,   a.*,c.* FROM
               {' . CLOUD_SERVER_TEMPLATES_ALERTS_TABLE . '} c
               LEFT JOIN  {' . CLOUD_SERVER_TEMPLATES_TABLE . '} a  on a.template_id=c.server_template_id and c.deleted=0
               LEFT JOIN {' . CLOUD_ALERTS_TABLE . '} e on c.alert_id=e.id
               WHERE 1 ' . $search_where . ' and a.deleted=0 ' . $order_by ;
}

function _cloud_server_templates_get_active_template_count_by_cloud() {

    $cloud_list = cloud_get_all_clouds() ; // Get all enabled clouds
    
    $query = 'SELECT count(*) as active_count, `cloud_type` FROM `cloud_server_templates` where `deleted`=0 group by `cloud_type` ' ;
    $cloud_count_arr = array() ;
    
    $result = db_query( $query);
    while ($cnt_row = db_fetch_object($result)) {

       if (isset($cloud_list[$cnt_row->cloud_type]) ) {
       
           $cloud_count_arr[$cnt_row->cloud_type] = $cnt_row->active_count ;
       }
    }

    return $cloud_count_arr ;
}