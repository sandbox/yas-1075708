<?php

/**
 * @file
 * Provides server template functionality for each cloud sub-system.
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

// Start : DB functions Related to Instances Template

/**
 * Insert a server template in database
 * This function insert entry in database and audit logs as well.
 *
 * @param $form_values
 *        This is the form-values submitted by new template form
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a last inserted template-id
 */
function _cloud_server_templates_instance_template_save($form_values, $cloud_context='') {
  
  
  $insert_query = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_TABLE . '} 
                  ( `template_nickname` ,  
                    `image_id` , 
                    `count_instances` , 
                    `key_name` , 
                    `instance_type` , 
                    `placement` , 
                    `kernel_id` , 
                    `ramdisk_id` , 
                    `user_data` , 
                    `group_id`
                  )  values 
          '; 
          
  $insert_query = $insert_query . "  (  '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ) " ; 
  
  $query_args[] = $form_values['nickname_text'] ;
  $query_args[] = $form_values['image_select'] ;
  $query_args[] = $form_values['Count_text'] ;
  $query_args[] = $form_values['ssh_keys_select'] ;
  $query_args[] = $form_values['Instance_Type_select'] ;
  $query_args[] = $form_values['zone_select'] ;
  $query_args[] = $form_values['kernel_select'] ;
  $query_args[] = $form_values['RAMDisk_select'] ;
  $query_args[] = $form_values['user_data_text'] ;
  $query_args[] = implode(',', $form_values['SG_select'] );
  
  db_query( $insert_query, $query_args );  
  
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('New Template has been added: @server_template_nickname', array('@server_template_nickname' => $form_values['nickname_text'])),
      'link'    => '',
    )
  );
    
  return;
}

/**
 * update a template to database
 * This function update entry in database and audit logs as well.
 *
 * @param $form_values
 *        This is the form-values submitted by edit template form
 * @param $cloud_context
 *        This is the sub-cloud who's template is being added
 * @return return a updated template-id
 */
function _cloud_server_templates_instance_template_update($form_values, $template_id, $cloud_context='') {

  $update_query = 'update {' . CLOUD_SERVER_TEMPLATES_TABLE . '} 
                set `template_nickname`=\'%s\' ,  
                    `image_id`=\'%s\' ,  
                    `count_instances`=\'%s\' ,  
                    `key_name`=\'%s\' ,  
                    `instance_type`=\'%s\' ,  
                    `placement`=\'%s\' ,  
                    `kernel_id`=\'%s\' ,  
                    `ramdisk_id`=\'%s\' ,  
                    `user_data`=\'%s\' ,  
                    `group_id`=\'%s\'  
                  where template_id=\'%s\'
          '; 
          
  //$update_query = $insert_query . "  (  , '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' )  " ; 
  
  $query_args = array(
    $form_values['nickname_text'       ],
    $form_values['image_select'        ],
    $form_values['Count_text'          ] ,
    $form_values['ssh_keys_select'     ] ,
    $form_values['Instance_Type_select'] ,
    $form_values['zone_select'         ] ,
    $form_values['kernel_select'       ] ,
    $form_values['RAMDisk_select'      ] ,
    $form_values['user_data_text'      ] ,
    implode(',', $form_values['SG_select'] ),
    $template_id 
  );
  
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('Instance Template has been modified: @server_template_nickname', array('@server_template_nickname' => $form_values['nickname_text'])),
      'link'    => ''
    )
  );
  
  db_query( $update_query, $query_args );  
  return;
}

/**
 * delete a template from database
 * This function deletes entry from database and updates audit logs.
 *  * 
 * @param $template_id
 *        This is the template-id which is to be deleted
 * @param $cloud_context
 *        This is the sub-cloud who's template is being deleted
 * @return return
 */
function _cloud_server_templates_instance_template_delete($template_id, $cloud_context='') {
  
  $delete_query = 'DELETE FROM {' . CLOUD_SERVER_TEMPLATES_TABLE . '} 
                  where template_id=\'%s\'
          '; 
  
  $query_args[] = $template_id ;
  db_query( $delete_query, $query_args );  
  return;
}


// End : DB functions Related to Instances Template