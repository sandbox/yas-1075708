<?php

/**
 * @file
 * Defines constants for cloud_activity_audit.*
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

define('CLOUD_ACTIVITY_AUDIT_PREFIX'            , 'cloud_'                                          ) ;
define('CLOUD_ACTIVITY_AUDIT_DRUPAL_USERS_TABLE', 'users'                                           ) ;
define('CLOUD_ACTIVITY_AUDIT_TABLE'             , CLOUD_ACTIVITY_AUDIT_PREFIX . 'activity_audit_log') ;
define('CLOUD_ACTIVITY_AUDIT_PATH'              , 'reports/activity_audit'                          ) ;
define('CLOUD_ACTIVITY_AUDIT_PAGER_LIMIT'       , 50                                                ) ;
