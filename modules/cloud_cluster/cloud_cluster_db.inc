<?php


/**
 * @file
 * Provides cluster feature which enables to bundle server templates.
 * Works with Cloud and Server Templates module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


function _cloud_cluster_get_clusters_query() {
  
   return $query =  'SELECT c.cluster_id, c.cluster_nickname, c.description, c.creator FROM {' . CLOUD_CLUSTER_TABLE . '} c WHERE deleted=0 and %s like \'%%%s%%\'  ' ;
}


function _cloud_cluster_get_count_db() {

   $cluster_count = 0 ;
   $query =  'SELECT count(*) as cluster_count FROM {' . CLOUD_CLUSTER_TABLE . '} c  WHERE deleted=0 ' ;
   $result = db_query( $query, NULL );
   while ($cluster = db_fetch_object($result)) {

       $cluster_count = $cluster->cluster_count ;
   }

  return $cluster_count ;   
}


function _cloud_cluster_get_cluster_by_id_query() {
  
   return $query =  'SELECT c.cluster_id ,  c.cluster_nickname,  c.description,  c.creator FROM {' . CLOUD_CLUSTER_TABLE . '} c  WHERE deleted=0 and cluster_id= \'%s\'  ' ;
}

function _cloud_cluster_get_cluster_by_filter_query() {

   return $query =  'SELECT c.cluster_id ,  c.cluster_nickname,  c.description,  c.creator FROM {' . CLOUD_CLUSTER_TABLE . '} c  WHERE deleted=0 and %s = \'%s\'  ' ;
}

/**
 * Insert a cluster in database
 * This function insert entry in database and audit logs as well.
 *
 * @param $form_values
 *        This is the form-values submitted by new cluster form
 * @return return a last inserted cluster-id
 */ 
function _cloud_cluster_save_cluster_db($form_values, $creator) {
  
  $insert_query = 'INSERT INTO {' . CLOUD_CLUSTER_TABLE . '} 
                  ( `cluster_nickname` ,  
                    `description` , 
                    `creator` 
                  )  values 
          '; 
          
  $insert_query = $insert_query . "  (  '%s', '%s', '%s' ) " ; 

  $query_args = array(
    $form_values['nickname_text'   ] ,
    $form_values['description_text'] ,
    $creator                         ,
  ) ;
  
  db_query( $insert_query, $query_args );  
  
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('Cluster has been added: @cluster_name', array('@cluster_name' => $form_values['nickname_text'])), 
      'link'    => '',
    )
  );


  // Get the id of newly inserted cluster
  $select_query =  'SELECT c.cluster_id ,  c.cluster_nickname,  c.description,  c.creator FROM {' . CLOUD_CLUSTER_TABLE . '} c
                        WHERE deleted=0 
                        and cluster_nickname= \'%s\'
                        and description= \'%s\'
                        and creator= \'%s\'
                        order by cluster_id desc LIMIT 1 ' ;

  $select_args = array(
    $form_values['nickname_text'   ] ,
    $form_values['description_text'] ,
    $creator                         ,
  ) ;

  $result = db_query( $select_query, $select_args );

   $cluster_id = NULL ;
   while ($cluster = db_fetch_object($result)) {

       $cluster_id = $cluster->cluster_id ;
   }

  return $cluster_id;
}


function _cloud_cluster_update_cluster_db($form_values, $cluster_id) {
  
  $update_query = 'update {' . CLOUD_CLUSTER_TABLE . '} 
                set `cluster_nickname`=\'%s\' ,  
                    `description`=\'%s\' 
                  where cluster_id=\'%s\'
          '; 

  $query_args = array(
    $form_values['nickname_text'   ],
    $form_values['description_text'],
    $cluster_id                     ,
  );
  db_query( $update_query, $query_args );  
  
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',  
      'message' => t('Cluster has been modified: @cluster_nickname', array('@cluster_nickname' => $form_values['nickname_text'])), 
      'link'    => ''
    )
  );       

  return;
}



/**
 * Delete a Cluster using Cluster Id
 * 
 * @param $cluster_id
 * @return
 */
function _cloud_cluster_delete_by_id($cluster_id) {

  $query = 'update {' . CLOUD_CLUSTER_SERVER_TABLE . '} set deleted=1
                  where cluster_id=\'%s\''; 
  $query_args = array(
    $cluster_id 
  );
  $result = db_query( $query, $query_args );
  
  $query = 'update {' . CLOUD_CLUSTER_TABLE . '} set deleted=1
                  where cluster_id=\'%s\'';
  $query_args = array(
    $cluster_id 
  );
  $result = db_query( $query, $query_args );
  
  // User Activity Log
  cloud_audit_user_activity(array(
        'type'    => 'user_activity', 
        'message' => t('Cluster has been deleted: @cluster_id', array('@cluster_id' => $cluster_id)),
        'link'    => ''
     )
  );

  return TRUE ;
}


/**
 * Return all cluster list
 * 
 * @param $cloud
 * @return
 */
function _cloud_cluster_get_all_clusters_query($cloud) {
  
    return ' select * from {' . CLOUD_CLUSTER_TABLE . '} where deleted=0  ';
}


// End : DB functions Related to Cluster


