<?php

/**
 * @file
 * Provides cluster feature which enables to bundle server templates.
 * Works with Cloud and Server Templates module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


// Start : DB functions Related to Cluster_server

function _cloud_cluster_save_cluster_server_db($form_values) {

  $insert_query = 'INSERT INTO {' . CLOUD_CLUSTER_SERVER_TABLE . '}
    ( `cluster_id`            ,
      `template_id`           ,
      `server_nickname`       ,
      `server_sshkey`         ,
      `server_security_group` ,
      `server_zone`           ,
      `failover_flag`         ,
      `elastic_ip`
    )  values
  ';

  $insert_query = $insert_query . " ( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ) " ;

  $cluster_id = $form_values['cluster_id'] ;


  $failover_select = isset($form_values['failover_select']) ? $form_values['failover_select'] : FALSE ;
  $query_args = array(
    $cluster_id                              ,
    $form_values['template_select'        ]  ,
    $form_values['nickname_text'          ]  ,
    $form_values['ssh_keys_select'        ]  ,
    @implode(',', $form_values['SG_select'] ),
    $form_values['zone_select'            ]  ,
    $failover_select  ,
  ) ;

  if ($failover_select) {
    $query_args[] = $form_values['elastic_ip_select'] ;
  }
  else
    $query_args[] = '' ; 

  db_query( $insert_query, $query_args );

  if ($failover_select) {
    $server_id = _cloud_cluster_get_last_added_server_db($form_values) ;
    _cloud_failover_save_server_scenario( $server_id, $form_values['failover_scenario_select']) ;
  }

  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',
      'message' => t('Cluster Server has been added: @cluster_name', array('@cluster_name' => $form_values['nickname_text'])),
      'link'    => '',
    )
  );

  return;
}


function _cloud_cluster_save_cluster_template_db($values) {

  $insert_query = 'INSERT INTO {' . CLOUD_CLUSTER_SERVER_TABLE . '}
    ( `cluster_id`            ,
      `template_id`           ,
      `server_nickname`       ,
      `server_sshkey`         ,
      `server_security_group` ,
      `server_zone`           ,
      `failover_flag`         ,
      `elastic_ip`            ,
      `instance_id`
    )  values
  ';

  $insert_query = $insert_query . " ( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ) " ;

  $query_args = array(
    $values['cluster_id'             ]  ,
    $values['template_select'        ]  ,
    $values['nickname_text'          ]  ,
    $values['ssh_keys_select'        ]  ,
    $values['SG_select'              ]  ,
    $values['zone_select'            ]  ,
    $values['failover_select'        ]  ,
    ''                                  ,
    $values['instance_id'            ]
  ) ;

  db_query( $insert_query, $query_args );

  return;
}


function _cloud_cluster_servers_get_query() {

  return $query = ' select * from
              {' . CLOUD_CLUSTER_SERVER_TABLE . '} e WHERE e.deleted=0 and e.cluster_id=%s ' ;
}


function _cloud_cluster_server_get_query() {

  return $query = ' select * from
              {' . CLOUD_CLUSTER_SERVER_TABLE . '} e WHERE e.deleted=0 and e.cluster_id=%s and e.server_id=%s ' ;
}


function _cloud_cluster_templates_get_query() {

  return $query = ' select e.instance_id, e.server_nickname as server_nickname , tmpl.cloud_type as cloud_type, tmpl.template_nickname as template_nickname , e.cluster_id, e.deleted, tmpl.template_id as template_id, e.template_id, e.server_id , tmpl.count_instances from
              {' . CLOUD_CLUSTER_SERVER_TABLE . '} e 
              LEFT JOIN {' . CLOUD_SERVER_TEMPLATES_TABLE . '} tmpl
              on e.template_id = tmpl.template_id
              WHERE e.deleted=0 and e.cluster_id=%s ' ;
}

function _cloud_cluster_templates_server_get_query() {

  return $query = ' select e.instance_id, e.server_nickname as server_nickname , tmpl.cloud_type as cloud_type, tmpl.template_nickname as template_nickname , e.cluster_id, e.deleted, tmpl.template_id as template_id, e.template_id, e.server_id , tmpl.count_instances from
              {' . CLOUD_CLUSTER_SERVER_TABLE . '} e
              LEFT JOIN {' . CLOUD_SERVER_TEMPLATES_TABLE . '} tmpl
              on e.template_id = tmpl.template_id
              WHERE e.deleted=0 and e.cluster_id=%s and e.server_id=%s ' ;
}

function _cloud_cluster_servers_get_count_db($cluster_id) {

  $query = ' select count(*) cnt from
              {' . CLOUD_CLUSTER_SERVER_TABLE . '} e WHERE e.deleted=0 and e.cluster_id=%s ' ;

  $query_args = array(
    $cluster_id,
  ) ;

  $result     = db_query( $query, $query_args );
  while ($cnt_res = db_fetch_object($result)) { // Cluster server object

    return $cnt_res->cnt ;
  }

  return 0 ;
}


function _cloud_cluster_get_server_query() {

  $query = 'select * from  {' . CLOUD_CLUSTER_SERVER_TABLE . '}
                  where server_id=\'%s\' and deleted=0
          ';

  return $query ;
}


function _cloud_cluster_get_last_added_server_db($form_values) {

  $query = 'SELECT * FROM {' . CLOUD_CLUSTER_SERVER_TABLE . "} c  WHERE deleted=0 and cluster_id= '%s' and template_id= '%s' and server_nickname= '%s'
        ORDER BY `server_id` DESC LIMIT 1 " ;
  $query_args = array(
    $form_values['cluster_id'] ,
    $form_values['template_select'] ,
    $form_values['nickname_text'  ] ,
  ) ;
  
  $result = db_query( $query, $query_args );

  while ($key = db_fetch_object($result)) {

    return $key->server_id ;
  }

}


function _cloud_cluster_delete_server_db($server_id) {

  $delete_query = 'update {' . CLOUD_CLUSTER_SERVER_TABLE . '} set deleted=1
                  where server_id=\'%s\'';

  $query_args = array(
    $server_id,
  ) ;
  db_query( $delete_query, $query_args );

  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',
      'message' => 'Cluster Server has been deleted.',
      'link'    => '',
    )
  );

  return;
}


function _cloud_cluster_delete_all_server_db($cluster_id) {

  $delete_query = 'update {' . CLOUD_CLUSTER_SERVER_TABLE . '} set deleted=1
                  where cluster_id=\'%s\'';

  $query_args = array(
    $cluster_id,
  ) ;
  db_query( $delete_query, $query_args );

  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',
      'message' => 'Cluster Servers have been deleted.',
      'link'    => '',
    )
  );

  return;
}


function _cloud_cluster_update_cluster_server_query() {

  $update_query = 'update {' . CLOUD_CLUSTER_SERVER_TABLE . '}
                  set %s=\'%s\' where server_id=\'%s\' ';

  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',
      'message' => 'Cluster server has been modified.',
      'link'    => '',
    )
  );

  return $update_query ;
}


// End : DB functions Related to Cluster_server
