<?php

/**
 * @file
 * Provides pricing for hourly rate configuration for Billing module.
 * Works with Cloud and Billing module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


// scripting functions 
//start
function _cloud_pricing_get_instances($cloud_context='') {

  
  $search_where = '';
  if (!empty($cloud_context)) {
    $search_where = ' and cloud_type="' . $cloud_context . '" ';
  }
  
  return $query =  'SELECT * FROM {' . CLOUD_PRICING_INSTANCES_TABLE . '} c WHERE %s like \'%%%s%%\' and deleted=0 ' . $search_where . ' ' ;
}

function _cloud_pricing_delete($id) {
    
  //// add watchdog log
  $query = _cloud_pricing_get_instances() ; 
  $query_args = array(
    // TODO: What kind of id? (by Manoj)
    'id' ,
    $id  ,   
  );
  $result = db_query( $query, $query_args );
  $count = 0 ;  
  $pricing_obj = db_fetch_object($result);    
    
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('Pricing has been deleted: @pricing', array('@pricing' => $pricing_obj->instance_type)),
      'link'    => '',
    )
  );
    
  $delete_query = 'update {' . CLOUD_PRICING_INSTANCES_TABLE . '} set deleted=1
                  where id=\'%s\'
          '; 
  $query_args = array(
    $id,
  );
  db_query( $delete_query, $query_args );
  return;  
}

/**
 * Insert a pricing in database
 * This function insert entry in database and audit logs as well.
 *
 * @param $form_values
 *        This is the form-values submitted by new pricing form
 * @return return a last inserted pricing-id
 */ 
function _cloud_pricing_insert($form_values) {

  $insert_query_instance = 'INSERT INTO {' . CLOUD_SERVER_TEMPLATES_INSTANCE_TYPE_TABLE . '} 
                  ( 
                    `instance_type` , 
                    `description` , 
                    `cloud_type` 
                  )  values 
          '; 
          
  $insert_query_instance = $insert_query_instance . "  ( '%s' , '%s' , '%s') " ; 

  $query_args = array() ;
  $query_args[]  = $form_values['instance_type_select'    ] ;
  $query_args[]  = $form_values['description_text'        ] ;
  $cloud_context = !empty($form_values['cloud_context'           ])
                 ? $form_values['cloud_context'           ]
                 : '' ;
  $query_args[] = $cloud_context ;

  db_query( $insert_query_instance, $query_args );
  
  $insert_query = 'INSERT INTO {' . CLOUD_PRICING_INSTANCES_TABLE . '} 
                  ( 
                    `instance_type`       , 
                    `description`         , 
                    `linux_or_unix_usage` , 
                    `windows_usage`       , 
                    `cloud_type`          , 
                    `created`             ,
                    `updated` 
                  )  values 
          '; 
          
  $insert_query = $insert_query . "  ( '%s' , '%s' , '%s', '%s', '%s', '%s', '%s' ) " ; 

  $query_args = array() ;
  $query_args[]  = $form_values['instance_type_select'    ] ;
  $query_args[]  = $form_values['description_text'        ] ;
  $query_args[]  = $form_values['linux_or_unix_usage_text'] ;
  $query_args[]  = $form_values['windows_usage_text'      ] ;
  $cloud_context = !empty($form_values['cloud_context'    ])
                 ? $form_values['cloud_context'           ]
                 : '' ;
  $query_args[] = $cloud_context ;
  $query_args[] = date('c') ; // UTC
  $query_args[] = date('c') ; // UTC

  db_query( $insert_query, $query_args );  
  
  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('New Pricing has been added: @pricing', array('@pricing' => $form_values['instance_type_select'])),
      'link'    => '',
    )
  );

  return;
}

function _cloud_pricing_update($form_values, $pricing_id='') {
  
  $query_args = array();
  $update_query_instance = 'UPDATE {' . CLOUD_SERVER_TEMPLATES_INSTANCE_TYPE_TABLE . '} 
                set   
                    `instance_type`=\'%s\'  
                  where instance_type=\'%s\'
          '; 
          
  $query_args[] = $form_values['instance_type_select'] ;
  $query_args[] = $form_values['instance_type_old'] ;

  db_query( $update_query_instance, $query_args );
  
  $query_args = array();
  $update_query = 'UPDATE {' . CLOUD_PRICING_INSTANCES_TABLE . '} 
                set   
                    `instance_type`=\'%s\'       ,
                    `description`=\'%s\'         ,
                    `linux_or_unix_usage`=\'%s\' ,
                    `windows_usage`=\'%s\'       ,
                    `cloud_type`=\'%s\'          ,
                    `updated`=\'%s\'
                  where id=\'%s\'
          '; 
          
  $query_args[] = $form_values['instance_type_select'    ] ;
  $query_args[] = $form_values['description_text'        ] ;
  $query_args[] = $form_values['linux_or_unix_usage_text'] ;
  $query_args[] = $form_values['windows_usage_text'      ] ;
  $query_args[] = !empty( $form_values['cloud_context'] ) ? $form_values['cloud_context'] : '' ;
  $query_args[] = date('c') ; // UTC
  $query_args[] = $pricing_id ;

  db_query( $update_query, $query_args );  
  
  cloud_audit_user_activity( array(
      'type'    => 'user_activity', 
      'message' => t('Pricing has been modified: @pricing', array('@pricing' => $pricing_id)),
      'link'    => '', //'design/alerts/create&id=' . $alert_id
    )
  );

  return;
}

function  cloud_pricing_delete_data($cloud_context) {

     $delete_query = 'DELETE FROM {' . CLOUD_PRICING_INSTANCES_TABLE . '} where `cloud_type`=\'%s\' ' ;
     $query_args   = array(
       $cloud_context,
     ) ;
     
     db_query( $delete_query, $query_args );
}


function  cloud_pricing_delete_all_data() {

     $delete_query = 'DELETE FROM {' . CLOUD_PRICING_INSTANCES_TABLE . '} ' ;
     $query_args = array() ;
     
     db_query( $delete_query, $query_args );
}


function _cloud_pricing_get_by_id($cloud_context , $instance_type) {

    $query_args = array() ;
    $select_query = 'SELECT * FROM {' . CLOUD_PRICING_INSTANCES_TABLE . '} c WHERE `instance_type`=\'%s\'  and `cloud_type`=\'%s\' ';
    $query_args[] = $instance_type ;
    $query_args[] = $cloud_context ;

    $result = db_query( $select_query, $query_args );

    $pricing_info = array() ;
    while ($pricing_obj = db_fetch_object($result)) {

      $pricing_info['instance_type'      ] = $pricing_obj->instance_type ;
      $pricing_info['cloud_type'         ] = $pricing_obj->cloud_type ;
      $pricing_info['linux_or_unix_usage'] = $pricing_obj->linux_or_unix_usage ;
    }

    return $pricing_info ;
}

//end