<?php 

/**
 * @file
 * Provides pricing for hourly rate configuration for Billing module.
 * Works with Cloud and Billing module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


module_load_include('inc', 'cloud'        , 'cloud_constants'        ) ; 
module_load_include('inc', 'cloud_pricing', 'cloud_pricing_constants') ;
module_load_include('inc', 'cloud_pricing', 'cloud_pricing_db'       ) ;

/**
 * Implementation of hook_install().
 */
function cloud_pricing_install() {

  // Create tables.
  drupal_install_schema('cloud_pricing');  
} 

/**
 * Implementation of hook_uninstall().
 */
function cloud_pricing_uninstall() {
  // Remove tables.
  drupal_uninstall_schema('cloud_pricing');
} 

/**
 * Implementation of hook_schema().
 */

function cloud_pricing_schema() {

  drupal_load('module', 'cloud');
  $schema = array();
  
  $schema[CLOUD_PRICING_INSTANCES_TABLE] = array(
    'description'           => t('Instances Auto-scaling'), 
    'fields'                => array(
      'id'                  => array('type' => 'serial'  , 'length'  =>  11), 
      'cloud_type'          => array('type' => 'varchar' , 'length'  => 255), 
      'instance_type'       => array('type' => 'varchar' , 'length'  => 255), 
      'linux_or_unix_usage' => array('type' => 'float'   , 'default' =>   0), 
      'windows_usage'       => array('type' => 'float'   , 'default' =>   0), 
      'description'         => array('type' => 'text'      ), 
      'created'             => array('type' => 'datetime', ), 
      'updated'             => array('type' => 'datetime', ), 
      'deleted'             => array('type' => 'int'     , 'length' =>   3, 'default' => 0), 
    ), 
    'primary key'           => array('id'), 
  ); 

  // To add more schema just add one more $schema['newtable'] array.
  return $schema;
} 


/**
 * Implementation of hook_enable().
 *
 * Update entries from the already enabled clouds
 */
function cloud_pricing_enable() {

  drupal_load( 'module', 'cloud');
  // Check for the Cloud information and update the Pricing info accordingly
  $cloud_list = cloud_get_all_clouds();
  foreach ($cloud_list  as $cloud_context ) {

    $params = module_invoke($cloud_context , 'cloud_set_info') ;
    $pricing_data = empty($params['cloud_pricing_data'] )
                  ? array()
                  : $params['cloud_pricing_data'] ;
    foreach ($pricing_data as $key => $data) {

      $data_values = array() ;
      $data_values['instance_type_select'    ] = $data['instance_type'     ] ;
      $data_values['description_text'        ] = $data['description'       ] ;
      $data_values['linux_or_unix_usage_text'] = $data['linux_or_unix_cost'] ;
      $data_values['windows_usage_text'      ] = $data['windows_cost'      ] ;
      $data_values['cloud_context'           ] = $cloud_context              ;
  
      // Insert the data in the Pricing table
      _cloud_pricing_insert($data_values) ;
    }
  }
}


/**
 * Implementation of hook_disable().
 *
 * Delete entries from the enabled clouds
 */
function cloud_pricing_disable() {

  drupal_load( 'module', 'cloud');
  cloud_pricing_delete_all_data() ;
}

