<?php 

/**
 * @file
 * Provides pricing for hourly rate configuration for Billing module.
 * Works with Cloud and Billing module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

/**
 * Updated by yas   2011/02/14
 * Updated by yas   2011/02/02
 */

/**
 * Returns a form with listing of pricing
 * Build a form including table header and table body
 * @param $form_submit
 *        This is the form-submit submitted by list pricing form
 * @param $cloud_context
 *        This is the sub-cloud who's pricing is being displayed
 * @return return a form
 */
// / pricing module
function cloud_pricing_list($form_submit, $cloud_context='') {

  $column  = 'name' ;
  $form['options'] = array(
    '#type'   => 'fieldset', 
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>'
 // '#title' => t('Operations'), 
  );

  $options = array(
    t('Instance Type')
  ) ;

  $filter     = cloud_get_filter_value( $form_submit , 'filter'    ) ;
  $filter_col = cloud_get_filter_value( $form_submit , 'operation' ) ;
  $filter     = trim($filter);

  if ($filter_col == 0) {
    $column  = 'Instance Type' ;
    $sql_col = 'instance_type' ;
  } 

  if (isset($filter)) {

    $query_args[] = $sql_col ;
    $query_args[] = $filter  ;
  }
  else {

    $filter       = ' 1 ' ;
    $query_args[] = ' ' ;
  } 
  asort($options);
  $form['options']['label'    ] = array( '#type' => 'item'     , '#title'   => t('Filter'));
  $form['options']['operation'] = array( '#type' => 'select'   , '#options' => $options, '#default_value' => $filter_col);
  $form['options']['filter'   ] = array( '#type' => 'textfield', '#size'    => 40      , '#default_value' => $filter);
  $form['options']['submit'   ] = array( '#type' => 'submit'   , '#value'   => t('Apply'));

  if (user_access('create pricing')) {
    $form['options']['cloud_pricing'] = array('#type' => 'submit', '#value' => t('Create')) ;
  } 

  $form['header'] = array('#type' => 'value', 
    '#value' => array(
      array('data' => t('Instance Type'), 'field' => 'instance_type'        ,
                                          'sort'  => 'asc'                  ,
                                          'class' => 'nickname-column'    ) ,
      array('data' => t('Linux Usage'  ), 'field' => 'linux_or_unix_usage') , 
      array('data' => t('Windows Usage'), 'field' => 'windows_usage'      ) , 
      array('data' => t('Updated'      ), 'field' => 'updated'            ) , 
      array('data' => t('Action'       ), 'class' => 'action-column'      ) ,
    )
  );
  
  $query = _cloud_pricing_get_instances($cloud_context) ; 
  // print_r($query_args);die;
  $query .= tablesort_sql($form['header']['#value']) ;

  $result = pager_query($query, CLOUD_PRICING_PAGER_LIMIT, 0, NULL, $query_args); 
  // $destination = drupal_get_destination();
  // print_r($destination);  die;
  while ($pricing_obj = db_fetch_object($result)) {

    // print 'here';die;

    $form['instance_type'      ][$pricing_obj -> id] = array(array( '#value' => t($pricing_obj->instance_type)));
    $form['hdnName'            ][$pricing_obj -> id] = array( '#type' => 'hidden' , '#value' => addslashes($pricing_obj->instance_type          ));
    $form['linux_or_unix_usage'][$pricing_obj -> id] = array(array( '#value' => t('$' . number_format($pricing_obj->linux_or_unix_usage, 3     ))));
    $form['windows_usage'      ][$pricing_obj -> id] = array(array( '#value' => t('$' . number_format($pricing_obj->windows_usage      , 3     ))));
    $form['Updated'            ][$pricing_obj -> id] = array(array( '#value' =>           format_date(strtotime($pricing_obj->updated), 'small')));
  } 

  $form['pager'        ] = array('#value' => theme('pager', NULL, CLOUD_PRICING_PAGER_LIMIT, 0));
  $form['cloud_context'] = array('#type'  => 'hidden', '#value' => ($cloud_context) );
  $form['#redirect'    ] = FALSE;

  return $form;
} 

function theme_cloud_pricing_list($form) {

  $form['pager']['#value'] = !empty($form['pager']['#value']) ? $form['pager']['#value'] : '';
  $rows = array();

  if ( !empty($form['instance_type']) ) {

    foreach (element_children($form['instance_type']) as $key) {
  
      $row   = array(array('data' =>
        drupal_render($form['instance_type'      ][$key]), 'class' => 'nickname-column'),
        drupal_render($form['linux_or_unix_usage'][$key]),
        drupal_render($form['windows_usage'      ][$key]),
        drupal_render($form['Updated'            ][$key]),
      );
      
      $prop['onclick'] = cloud_get_messagebox('Are you sure you want to delete the Pricing (for "' . $form['hdnName'][$key]['#value'] . '" ?') ;
  
      $action_data = '' ;
      if (user_access('delete pricing')) {
  
        $action_data .= cloud_display_action('images/icon_delete', t('Delete') , 'admin/settings'
                     . '/' . $form['cloud_context']['#value'] . '/pricing/' . urlencode($key)  . '/delete', array('query' => 'id=' . urlencode($key), 'html' => TRUE), $prop['onclick']); //,  'id=' . urlencode($key) , $prop );
      } 
  
      if (user_access('edit pricing')) {
        $action_data .= cloud_display_action('images/icon_clear', t('Edit') , 'admin/settings'
                     . '/' . $form['cloud_context']['#value'] . '/pricing/' . urlencode($key)  . '/edit'  , array('query' => 'id=' . urlencode($key), 'html' => TRUE)); //,  'id=' . urlencode($key)  );
      } 
  
      $row[]  = $action_data ;
      $rows[] = $row;
    } 
  }

  $reload_link = l('- Refresh Page -', 'admin/settings/' . $form['cloud_context']['#value'] . '/pricing/info') ;
  $ref_link    = array(
    '#type'   => 'item',
    '#prefix' => '<div id="link_reload" align="right">',
    '#value'  => $reload_link,
    '#suffix' => '</div>',
  );

//print_r($form);die;
  $output  = drupal_render($form['options']);
  $output .= theme('table', $form['header']['#value'], $rows);
  if ($form['pager']['#value']) {

    $output .= drupal_render($form['pager']);
  } 
  $output .= drupal_render($ref_link);
  $output .= drupal_render($form    ); 

  return $output;
} 

function cloud_pricing_list_submit($form_id, $form_values) {

  $form_values = $form_values['values'];
  if ($form_values['op'] == t('Create')) {
    return drupal_goto('admin/settings/' . $form_values['cloud_context'] . '/pricing/create') ;
  }

  return;
} 


function cloud_pricing_delete($pricing_id='', $cloud_context) {  

  _cloud_pricing_delete($pricing_id) ;

  drupal_set_message(t('Pricing has been deleted successfully.'));
  drupal_goto('admin/settings/' . $cloud_context . '/pricing/info') ;

  return;
} 


function cloud_pricing_new($form_submit, $pricing_id='', $cloud='') {

  $cloud_context = $cloud;
  $action = !($pricing_id) ? t('Add') : t('Edit');

  if ($action == t('Add')) {

    $instance_type_options = _cloud_server_templates_get_instance_type_list() ;
  }
  else {

    $instance_type_options = _cloud_server_templates_get_instance_type() ;
  } 

  $form['fieldset_pricing_info'] = array('#type' => 'fieldset', /* '#title' => t('Pricing Info') */);
  
  $instance_type_options[''] = '- Select -';

//asort($instance_type_options);

  $form['instance_type']['instance_type_label' ] = array('#type' => 'item', '#title' => t('Instance Type'), '#required' => TRUE);
  //$form['instance_type']['instance_type_select'] = array('#type' => 'select', '#options' => $instance_type_options);
  $form['instance_type']['instance_type_select' ] = array('#type' => 'textfield', '#size' => 50);

  $form['description']['description_label'] = array('#type' => 'item', '#title' => t('Description') , '#required' => TRUE);
  $form['description']['description_text' ] = array('#type' => 'textarea'); 

  $form['linux_or_unix_usage']['linux_or_unix_usage_label'] = array('#type' => 'item'     , '#title' => t('Linux Usage Charge per Hour ($)') , '#required' => TRUE);
  $form['linux_or_unix_usage']['linux_or_unix_usage_text' ] = array('#type' => 'textfield', '#size'  => 100);

  $form['windows_usage']['windows_usage_label'] = array('#type' => 'item', '#title' => t('Windows Usage Charge per Hour ($)'), '#required' => TRUE);
  $form['windows_usage']['windows_usage_text' ] = array('#type' => 'textfield', '#size' => 100);

  if ($pricing_id) {

    $query      = _cloud_pricing_get_instances() ;
    $query_args = array(
      'id',
      $pricing_id,
    );
    $result     = db_query( $query, $query_args );
    $count      = 0 ;

    $pricing_object = db_fetch_object($result);
    $action = t('Edit'); 
    $form['pricing_id'] = array('#type' => 'hidden', '#value' => $pricing_id);
    $form['instance_type_old'] = array('#type' => 'hidden', '#value' => $pricing_object -> instance_type);
    $form['instance_type'      ]['instance_type_select'    ]['#default_value'] = isset_variable($pricing_object -> instance_type);
  //$form['instance_type'      ]['instance_type_select'    ]['#disabled'     ] = TRUE ;
    $form['description'        ]['description_text'        ]['#default_value'] = isset_variable($pricing_object -> description);
    $form['linux_or_unix_usage']['linux_or_unix_usage_text']['#default_value'] = isset_variable($pricing_object -> linux_or_unix_usage);
    $form['windows_usage'      ]['windows_usage_text'      ]['#default_value'] = isset_variable($pricing_object -> windows_usage);
  } 

  $form['submit_buttons'] = array(
    '#type'   => 'fieldset',
    '#prefix' => '<span class="clear"></span><div class="container-inline"><div class="buttons">',
    '#suffix' => '</div></div>',
  );
  
  $form['submit_buttons'][$action ] = array( '#type' => 'submit', '#value' => t($action     ));
  $form['submit_buttons']['Cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'    )); 
  $form['cloud_context'           ] = array( '#type' => 'hidden', '#value' => $cloud_context );

  return $form ;
} 

function theme_cloud_pricing_new($form) {

  $rows = array(
    array(
      drupal_render($form['instance_type']['instance_type_label' ]),
      drupal_render($form['instance_type']['instance_type_select']),
    ),
    array(
      drupal_render($form['description']['description_label']),
      drupal_render($form['description']['description_text' ]),
    ),
    array(
      drupal_render($form['linux_or_unix_usage']['linux_or_unix_usage_label']),
      drupal_render($form['linux_or_unix_usage']['linux_or_unix_usage_text' ]),
    ),
    array( 
      drupal_render($form['windows_usage']['windows_usage_label']),
      drupal_render($form['windows_usage']['windows_usage_text' ]),
    ),
  );

  $table = theme('table', NULL, $rows );
  $form['fieldset_pricing_info']['#children'] = $table;

//cf. Waning by Coder module: Potential problem: when FAPI element '#type' is set to 'markup' (default), '#value' only accepts filtered text, be sure to use check_plain(), filter_xss() or similar to ensure your $variable is fully sanitized.
//$form['fieldset_pricing_info']['list'] = array('#type' => 'markup', '#value' => $table);

  $output  = drupal_render($form['fieldset_pricing_info']);
  $output .= drupal_render($form['submit_buttons'       ]);
  $output .= drupal_render($form);

  return $output;
}


function cloud_pricing_new_validate($form_id, $form_values) {
  $form_values = $form_values['values'];
  if ($form_values['op'] == t('Add') || $form_values['op'] == t('Edit')) {
    // print_r($form_values); die;

    if (empty($form_values['instance_type_select']) ) {
      form_set_error('',  t('You must enter valid Instance Type'));
    }

    if ( $form_values['op'] == t('Add') )   {

      $pricing_obj = _cloud_pricing_get_by_id( $form_values['cloud_context'] , $form_values['instance_type_select'] ) ;
      if (empty($pricing_obj) === FALSE ) {

        form_set_error('',  t('Instance type already exists.'));
      }
    }

    if (empty($form_values['description_text'])) {
      form_set_error('',  t('You must enter description'));
    }
    elseif (!preg_match(CLOUD_PRICING_VALID_NUMBER, $form_values['linux_or_unix_usage_text'])) {
      form_set_error('',  t('You must enter valid usage for Linux'));
    }
    elseif (!preg_match(CLOUD_PRICING_VALID_NUMBER, $form_values['windows_usage_text'])) {
      form_set_error('',  t('You must enter valid usage for Windows'));
    }
    
  } 
  return;
} 


function cloud_pricing_new_submit($form_id, $form_values) {

  $form_values = $form_values['values'];
  $redirect_to = 'admin/settings/' . $form_values['cloud_context'] . '/pricing/info';

  if ($form_values['op'] == t('Cancel')) {
    drupal_goto($redirect_to) ;
    return ;
  } 
  elseif ($form_values['op'] == t('Add')) {

    _cloud_pricing_insert($form_values) ;

    drupal_set_message(t('Pricing has been saved.'));
    drupal_goto($redirect_to) ;
  } 
  elseif ($form_values['op'] == t('Edit')) {

    $pricing_id = $form_values['pricing_id'] ;
    _cloud_pricing_update($form_values , $pricing_id) ;
    drupal_set_message(t('Pricing has been saved.'));
    drupal_goto($redirect_to) ;
  } 
  return;
} 


function cloud_pricing_view_submit($form_id, $form_values) {

  $form_values = $form_values['values'];

  if ($form_values['op'] == t('Back')) {

    drupal_goto(CLOUD_PRICING_PATH . '/info') ;

    return ;
  } 
} 