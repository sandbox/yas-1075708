BASIC INFO
==========

- Provides pricing for hourly rate configuration for Billing module.
- You can set up pricing 
- Works with Cloud and Billing module.


HOW TO USE
==========

1) Enable Pricing module
2) Go to the menu: Administer | Site configuration | Amazon EC2, OpenStack, Eucalyptus, or XCP
3) Click 'Pricing Info tab'
4) Click 'New Pricing' to add a new pricing item
5( or edit the existing predefined pricing item(s)


DIRECTORY STRUCTURE
===================

cloud
  +-modules (depends on Cloud module)(Cloud is a core module for Cloud package)
    +-cloud_activity_audit
    +-cloud_alerts
    o-cloud_auto_scaling
    +-cloud_billing
    +-cloud_cluster
    x-cloud_failover
    +-cloud_inputs
    o-cloud_pricing (This module)
    +-cloud_resource_allocator
    x-cloud_scaling_manager
    +-cloud_scripting
    +-cloud_server_templates

    x... Not released yet.


CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org
2010/11/09 ver.0.7  released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt