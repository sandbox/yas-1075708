<?php

/**
 * @file
 * cloud_alerts_db.inc
 * Provides a set of trigger and action for cloud management
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


// alerts functions 

function _cloud_alerts_get() { 
  
  return $query =  'SELECT * FROM {' . CLOUD_ALERTS_TABLE . '} c WHERE %s like \'%%%s%%\' and deleted=0' ;
}


function _cloud_alerts_get_count_db() {

   $alerts_count = 0 ;
   $query =  'SELECT count(*) as alerts_count FROM {' . CLOUD_ALERTS_TABLE . '} c  WHERE deleted=0 ' ;
   $result = db_query( $query, NULL );
   while ($alerts = db_fetch_object($result)) {

       $alerts_count = $alerts->alerts_count ;
   }

  return $alerts_count ;
}


function _cloud_alerts_delete($id) {  
  
  //// add watchdog log
  $query        = _cloud_alerts_get() ; 
  $query_args   = array(
    'id' ,
    $id  ,    
  );
  $result       = db_query( $query, $query_args );
  $count        = 0 ;  
  $alert_obj    = db_fetch_object($result);    
    
  // User Activity Log
  cloud_audit_user_activity( array(
        'type'    => 'user_activity',
        'message' => t('Alert has been deleted: @alert_name', array('@alert_name' => $alert_obj->name)),
        'link'    => '',
    )
  );
    
  $delete_query = 'update {' . CLOUD_ALERTS_TABLE . '} set deleted=1
                   where id=\'%s\'
                  '; 
  $query_args = array(
    $id,
  );
  db_query( $delete_query, $query_args );
  
  //delete from alert template table    
  $delete_query = 'update {' . CLOUD_SERVER_TEMPLATES_ALERTS_TABLE . '} set deleted=1
                   where alert_id=\'%s\'
                  '; 
  $query_args = array(
    $id,
  );
  db_query( $delete_query, $query_args );
  ////
  return;  
}

/**
 * Insert a alert in database
 * This function insert entry in database and audit logs as well.
 *
 * @param $form_values
 *        This is the form-values submitted by new alert form
 * @return return a last inserted alert-id
 */ 
function _cloud_alerts_insert($form_values) {
  
  

  $insert_query = 'INSERT INTO {' . CLOUD_ALERTS_TABLE . '} 
                  ( 
                    `name`               ,
                    `description`        ,
                    `metric`             ,
                    `variable`           ,
                    `condition`          ,
                    `threshold`          ,
                    `duration_in_minutes`,
                    `escalate`           ,
                    `created`            ,
                    `updated` 
                  )  values 
          '; 
          
  $insert_query = $insert_query . "  ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s') " ; 
  $id = uuid();
  //$query_args[] = !empty($form_values['alert_id']) ? $form_values['alert_id'] : $id;
  $query_args[] = $form_values['name_text'                 ] ;
  $query_args[] = $form_values['description_text'          ] ;
  $query_args[] = $form_values['metric_select'             ] ;
  $query_args[] = $form_values['variable_select'           ] ;
  $query_args[] = $form_values['condition_select'          ] ;
  $query_args[] = $form_values['threshold_text'            ] ;
  $query_args[] = $form_values['duration_in_minutes_select'] ;
  $query_args[] = $form_values['escalate_select'           ] ;
  $query_args[] = date('c') ; // UTC
  $query_args[] = date('c') ; // UTC

  db_query( $insert_query ,  $query_args );  
  
  // User Activity Log
  cloud_audit_user_activity( array(
        'type'    => 'user_activity',
        'message' => t('New Alert has been added: @alert_name', array('@alert_name' => $form_values['name_text'])),
        'link'    => '',
    )
  );

  return;
  //watchdog('user_activity',   'New Alert"' . $form_values['name_text'] . '") has been added.',   WATCHDOG_WARNING,   'design/alerts/create&id=' . $id);
}

function _cloud_alerts_update($form_values,   $alert_id='') {
  
  

  $update_query = 'UPDATE {' . CLOUD_ALERTS_TABLE . '} 
                set `name`=\'%s\'               ,
                    `description`=\'%s\'        ,
                    `metric`=\'%s\'             ,
                    `variable`=\'%s\'           ,
                    `condition`=\'%s\'          ,
                    `threshold`=\'%s\'          ,
                    `duration_in_minutes`=\'%s\',
                    `escalate`=\'%s\'           ,
                    `updated`=\'%s\'
                  where id=\'%s\'
          '; 
          
  $query_args[] = $form_values['name_text'                 ] ;
  $query_args[] = $form_values['description_text'          ] ;
  $query_args[] = $form_values['metric_select'             ] ;
  $query_args[] = $form_values['variable_select'           ] ;
  $query_args[] = $form_values['condition_select'          ] ;
  $query_args[] = $form_values['threshold_text'            ] ;
  $query_args[] = $form_values['duration_in_minutes_select'] ;
  $query_args[] = $form_values['escalate_select'           ] ;
  $query_args[] = date('c') ; // UTC
  $query_args[] = !empty($form_values['alert_id']) ? $form_values['alert_id'] : $alert_id ;

  db_query( $update_query ,    $query_args );  
  
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',
      'message' => t('Alert has been modified: @alert_name', array('@alert_name' => $form_values['name_text'])),
      'link'    => '', // 'design/alerts/create&id=' . $alert_id
    )
  );

  return;
}


function _cloud_alerts_get_all() {
 
  $query  = _cloud_alerts_get() ;
//$query .= tablesort_sql( array(array('field' => 'name', 'sort' => 'asc')) ) ;
  $query_args = array(
    'name',
    ''    ,
  );
  $result = db_query( $query, $query_args ); 
  
  $alert_options = array();
  while ($alert = db_fetch_object($result)) {
  
    $alert_options[$alert->id] = $alert->name ;
  }

//asort($alert_options);
  return $alert_options ;
}

//end