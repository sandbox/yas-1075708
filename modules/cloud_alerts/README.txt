BASIC INFO
==========

- Provides a set of trigger and action for cloud management
- Works with Cloud. Failover and auto-scaling module.

* This module will do nothing; not implemented for sending e-mail, etc.
* This module should be implemented as based on trigger/action on Drupal.

HOW TO USE
==========

1) Enable Alert module
2) Go to the menu: Design | Alerts | New Alert Button
3) Create a server template
4) Select alert(s) in a server template


TODO
====

- This module started with D5, so it needs to match D6's trigger and action APIs. 


DIRECTORY STRUCTURE
===================

cloud
  +-modules (depends on Cloud module)(Cloud is a core module for Cloud package)
    +-cloud_activity_audit
    o-cloud_alerts (This module)
    x-cloud_auto_scaling
    +-cloud_billing
    +-cloud_cluster
    x-cloud_failover
    +-cloud_inputs
    +-cloud_pricing
    +-cloud_resource_allocator
    x-cloud_scaling_manager
    +-cloud_scripting
    +-cloud_server_templates

    x... Not released yet.


CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.


End of README.txt