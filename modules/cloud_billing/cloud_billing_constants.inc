<?php

/**
 * @file
 * Provides billing functionality
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

define('CLOUD_BILLING_PREFIX'                 , 'cloud_'                                  ) ;
define('CLOUD_BILLING_INSTANCES_DETAILS_TABLE', CLOUD_BILLING_PREFIX . 'instances_details') ;
define('CLOUD_BILLING_PAGER_LIMIT'            , 50                                        ) ;
