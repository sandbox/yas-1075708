<?php

/**
 * @file
 * Provides usage estimate table based on user's activities.
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


// scripting functions 
//start
function _cloud_billing_generate_report($start, $end, $cloud_context='') {

  $clouds     = array();
  $cloud_list = cloud_get_all_clouds();
  foreach ($cloud_list as $cloud) {
  $clouds[] = $cloud;
  }
    $search_where = '';
    if ($cloud_context) {
      $search_where = ' and b.cloud_type="' . $cloud_context . '" ';
    }
    $search_where1 = '';

    if ($start && $end) {
      if ($end) {
        list($yr, $mo, $da) = explode('-', $end);
        $end = date('c',  mktime(0,  0,  0, $mo, $da+1, $yr)); //changed it to start of next month
        $now = date('c');//date('Y-m-dcH:i:s');
        $end = ($end < $now) ? $end : $now;
      }

      $start_search = 'if (launch_time < "' . $start . '", "' . $start . '", launch_time) ';
      $end_search = 'if (instance_state_name != "terminated", "' . $end . '", if (terminated_date < "' . $end . '" and terminated_date > "' . $start . '", terminated_date, "' . $end . '")) ';
      
      $search_where_sub = ' and launch_time < ' . $end_search . ' and (terminated_date > "' . $start . '" or terminated_date = "0000-00-00 00:00:00" or terminated_date IS null) and instance_state_name != "pending" ';
    }

    return $query =  'SELECT a.instance_type, b.id, b.description, b.linux_or_unix_usage, b.windows_usage,  ' . 
        '(select sum(TIMESTAMPDIFF(hour, ' . $start_search . ', ' . $end_search . ')) from {' . CLOUD_BILLING_INSTANCES_DETAILS_TABLE . '} c where a.instance_type=c.instance_type ' . $search_where_sub . ' and c.cloud_type="' . $cloud_context . '" group by c.instance_type) as total_hours,  ' . 
        '(select count(instance_id) from {' . CLOUD_BILLING_INSTANCES_DETAILS_TABLE . '} e where a.instance_type=e.instance_type ' . $search_where_sub . ' and e.cloud_type="' . $cloud_context . '"  group by e.instance_type) as tInstances,  '.
        '(select min(launch_time) from {' . CLOUD_BILLING_INSTANCES_DETAILS_TABLE . '} d where a.instance_type=d.instance_type and d.cloud_type="' . $cloud_context . '" group by d.instance_type) as start_date FROM {' . CLOUD_INSTANCE_TYPE_TABLE . '} a ' . 
        'left join {' . CLOUD_PRICING_INSTANCES_TABLE . '} b on a.instance_type=b.instance_type and b.deleted=0 and b.cloud_type="' . $cloud_context . '" ' . 
        'WHERE 1 and b.cloud_type="' . $cloud_context . '" group by a.instance_type ' ;
   
}
//end