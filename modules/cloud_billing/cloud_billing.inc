<?php

/**
 * @file
 * *.inc file: Billing
 * Provides usage estimate table based on user's activities.
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

define('CLOUD_BILLING_PATH', 'design/billing');

function _cloud_billing_usage_estimate($cloud_context = '' ) {
  return @drupal_get_form('cloud_billing_report', $cloud_context);
}


/**
 * Returns a form with listing of usage estimate
 * Build a form including table header and table body
 * @param $form_submit
 *        This is the form-submit submitted by billing form
 * @param $cloud_context
 *        This is the sub-cloud who's billing details is being displayed
 * @return return a form
 */
function cloud_billing_report($form_submit='', $cloud_context='') {

  global $user;

  $column = 'name' ;
  $total  = 0;
  $lumsum = 0;
  
  $form['header'] = array(
      '#type'  => 'value',  
      '#value' => array(
      array('data' => t('Date' ) ), 
      array('data' => t('Total') ), 
    )
  );
  
  $month_range_list = _cloud_billing_get_month_range_array();
  
  foreach ($month_range_list as $start => $end ) {

    $query = _cloud_billing_generate_report($start, $end, $cloud_context);
    //print $query;die;
    $result = db_query( $query );
    $total  = 0;

    while ($billing_obj = db_fetch_object($result)) {

      $total_instance_type_charge = 0;
      $unit_price = $billing_obj->linux_or_unix_usage;
      $total_instance_type_charge = ($billing_obj->total_hours > 0 )
                                  ? ($billing_obj->total_hours * $unit_price)
                                  : 0;
      //print $unit_price . '=' . $billing_obj->total_hours . '->';
      $total += $total_instance_type_charge;
    }  
    //die;
    $form['date'  ][$start] = array(
      array(
        '#value' => l($start . ' to ' . $end,
                      'reports/usage_estimate/'. $start . '/' . $end . '/details/' . $cloud_context,
                      array('query' => 'start=' . $start . '&end=' . $end))
      )
    ); // array(), 'start=' . $start . '&end=' . $end ,  NULL,  FALSE,  TRUE ) ) ); //t( $start . ' to ' . $end ) ) );  
    $form['totals'][$start] = array( array('#value' => t( '$' . number_format($total, 2)) ) );
    
    $lumsum += $total;
  }
  $form['cloud_context'            ] = array( '#type' => 'hidden' , '#value' => ($cloud_context) );
  $form['Total'] = array('#value' => '$' . number_format($lumsum, 2)) ;
// print 'passed 1';die;

  return $form;
}


function theme_cloud_billing_report($form) {
  
  $rows = array();
  foreach (element_children($form['date']) as $key) {
    $rows[] = array(
      drupal_render($form['date'   ][$key]),
      drupal_render($form['totals' ][$key]),
   // drupal_render($form['Updated'][$key]),
   // $action_data ;
    );
  }
  $rows[] = array(
    '',
    drupal_render($form['Total']),
  );

  $output  = drupal_render( $form['options']);
  $output .= theme('table', $form['header' ]['#value'], $rows);
   
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager' ]);
  }

  $reload_link = l('- Refresh Page -', 'reports/usage_estimate/' . $form['cloud_context']['#value']) ;  
  $ref_link = array(
    '#type'   => 'item',
    '#prefix' => '<div id="link_reload" align="right">',
    '#suffix' => '</div>',
    '#value'  => $reload_link,
  );  

  $output .= drupal_render($ref_link) ;
  $output .= drupal_render($form    ) ;
//print_r($form); die;

  return $output; 
}

function cloud_billing_report_submit($form_id, $form_values) {

  global $user;

  $form_values = $form_values['values'];
  return;
}

/**
 * Returns a form with listing of usage estimate
 * Build a form including table header and table body
 * @param $form_submit
 *        This is the form-submit submitted by billing form
 * @param $start
 *        This is start date of billing report
 * @param $end
 *        This is end date of billing report
 * @param $cloud_context
 *        This is the sub-cloud who's billing details is being displayed
 * @return return a form
 */
function cloud_billing_report_details($form_submit='', $start='', $end='', $cloud_context='') {

  $column         = 'name' ;
  $filter     = cloud_get_filter_value( $form_submit , 'filter' ) ;
  $filter_col = cloud_get_filter_value( $form_submit , 'operation' ) ;  
  
  $query_args     = array();
  $form['header'] = array(
  
    '#type' => 'value',  
    '#value' => array(
      array('data' => t('Rate (per Hour)' ), 'field' => 'linux_or_unix_usage', 'sort' => 'asc' ), 
      array('data' => t('Instance Type ID'), 'field' => 'instance_type' ) ,
      array('data' => t('Description'     ), 'field' => 'description'  ) ,
      array('data' => t('Instance(s)'     ), 'field' => 'tInstances'   ) , 
      array('data' => t('Usage (in Hours)'), 'field' => 'total_hours'  ) , 
      array('data' => t('Totals'          ), 'field' => 'total_hours'  ) , 
      array('data' => t('Start Date'      )                            ) , 
      array('data' => t('End Date'        )                            ) , 
      //array('data' => t('Action')       )
    )
  );

  $query = _cloud_billing_generate_report($start, $end, $cloud_context) ;
//print_r($query_args); die;
  $query .= tablesort_sql( $form['header']['#value'] ) ;   
  //print $query;die;
  $result = pager_query( $query, CLOUD_BILLING_PAGER_LIMIT, 0, NULL, $query_args );

  $total       = 0;
  $t_usage     = 0;
  $t_instances = 0;

  while ($billing_obj = db_fetch_object($result)) {

    $unit_price = $billing_obj->linux_or_unix_usage;
  //print $unit_price;die;
    $date_created = '' ;
    $date_updated = '' ;
    $start_date = ($start) ? date_format(date_create($start), 'm/d/Y')
                                       : '-';
    $end_date   = ($end)   ? date_format(date_create($end > date('Y-m-d')
                                       ? date('Y-m-d')
                                       : $end), 'm/d/Y')
                                       : '-';

    $total_instance_type_charge = ($billing_obj->total_hours > 0           )
                                ? ($billing_obj->total_hours * $unit_price )
                                :  0 ;
    $st_usage                   =  $billing_obj->total_hours ;
    $st_instances               =  $billing_obj->tInstances  ;

    $form['rate'         ][$billing_obj->id] = array(array('#value' => t( '$' . number_format($unit_price, 3)) ) );  
    $form['instance_type'][$billing_obj->id] = array(array('#value' => t( $billing_obj->instance_type ) ) );
    $form['description'  ][$billing_obj->id] = array(array('#value' => t( $billing_obj->description . ' instance ' ) ) );
    $form['stinstnaces'  ][$billing_obj->id] = array(array('#value' => t(($billing_obj->tInstances  > 0)
                                                                    ?    $billing_obj->tInstances
                                                                    :    0 ) ) ) ;
    $form['usage'        ][$billing_obj->id] = array(array('#value' => t(($billing_obj->total_hours > 0)
                                                                    ?    $billing_obj->total_hours
                                                                    :    0 ) ) );
    $form['totals'       ][$billing_obj->id] = array(array('#value' => t( '$' . number_format($total_instance_type_charge, 2)) ) );
    $form['start'        ][$billing_obj->id] = array(      '#value' => $start_date) ;
    $form['end'          ][$billing_obj->id] = array(      '#value' => $end_date  ) ;
  //$form['Updated'      ][$billing_obj->id] = array('#value' => date_format($date_updated, 'm/d/Y') ) ;
    $total       += $total_instance_type_charge ;
    $t_usage     += $st_usage                   ;
    $t_instances += $st_instances               ;
  }

  $form['Total'     ] = array('#value' => '$' . number_format($total, 2)) ;
  $form['tUsage'    ] = array('#value' => $t_usage) ;
  $form['tInstances'] = array('#value' => $t_instances) ;
  $form['cloud_context'] = array('#type' => 'hidden' , '#value' => $cloud_context ) ;

  //print 'here1';die;
  return $form;
}


function theme_cloud_billing_report_details($form) {
    
  $output = drupal_render($form['options']);
  $action_data = '';
  
  $rows = array();    
  $form['rate'] = !empty($form['rate']) ? $form['rate'] : '';
  $form['pager']['#value'] = !empty($form['pager']['#value']) ? $form['pager']['#value'] : '';
  $rows = array();
  foreach (element_children($form['rate']) as $key) {

    $rows[] = array(
      drupal_render($form['rate'         ][$key]),
      drupal_render($form['instance_type'][$key]),
      drupal_render($form['description'  ][$key]),
      drupal_render($form['stinstnaces'  ][$key]),
      drupal_render($form['usage'        ][$key]),
      drupal_render($form['totals'       ][$key]),
      drupal_render($form['start'        ][$key]),
      drupal_render($form['end'          ][$key]),
      $action_data,
    );
  }

  $rows[] = array(
    $row = array(
      '',
      drupal_render($form['tInstances']),
      drupal_render($form['tUsage'    ]),
      drupal_render($form['Total'     ]),
    ),
  );
  
  $output .= theme('table', $form['header']['#value'], $rows);
   
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $cloud_context = $form['cloud_context']['#value'] ;
  $reload_link = l('- Back to Report Page -', 'reports/usage_estimate/' . $cloud_context );  
  $ref_link = array(
                '#type'   => 'item',
                '#prefix' => '<div id="link_reload" align="right">',
                '#suffix' => '</div>',
                '#value'  => $reload_link
              );  
  $output .= drupal_render($ref_link);

  $output .= drupal_render($form);
  //print_r($form);die;

  return $output; 
}

function _cloud_billing_get_month_range_array() {

  $month_range_list = array();
  for ($i=0; $i<12; $i++ ) {//for last 1 year
    _cloud_billing_get_month_range($start, $end, $i);
    $month_range_list[$start] = $end;
  }

  return $month_range_list;
}

function _cloud_billing_get_month_range(&$start_date, &$end_date, $offset=0) { 

  $start_date = ''; 
  $end_date   = '';    
  $date       = date('Y-m-d'); 
  
  list($yr, $mo, $da) = explode('-', $date); 
  $start_date = date('Y-m-d',  mktime(0, 0, 0, $mo - $offset, 1, $yr)); 
  
  $i = 2; 
  
  list($yr, $mo, $da) = explode('-', $start_date); 
  
  while (date('d',  mktime(0, 0, 0, $mo, $i, $yr)) > 1) { 
    $end_date = date('Y-m-d',  mktime(0, 0, 0, $mo, $i, $yr)); 
    $i++; 
  } 
}

function _cloud_billing_check_for_instance($instance_id, $cloud_context='') {

  $check_query = 'SELECT count(*) as tCount FROM {'
               . CLOUD_BILLING_INSTANCES_DETAILS_TABLE
               . '} where instance_id="' . $instance_id . '" and cloud_type="' . $cloud_context . '" ';

  $check_result = db_query( $check_query );
  if ($check_result) {
    $row = db_fetch_object( $check_result );
    return $row->tCount;
  }

  return 0;
}


function _cloud_billing_update( $cloud_context, $instance_id='', $instance_state='', $launch_time='', $instance_type='') {

  $update_query  = 'update {'
                 . CLOUD_BILLING_INSTANCES_DETAILS_TABLE
                 . '} set terminated_date="' . date('c')
                 . '" where instance_state_name="terminated" and terminated_date IS NULL';
  $update_result = db_query( $update_query );

  if (empty($instance_id)) return;

  if ( _cloud_billing_check_for_instance($instance_id, $cloud_context) == 0) {

    $detail_insert_query = 'INSERT INTO {' . CLOUD_BILLING_INSTANCES_DETAILS_TABLE
                         . '} (instance_id, instance_state_name, launch_time, instance_type, cloud_type) values ("'
                         . $instance_id    . '", "'
                         . $instance_state . '", "'
                         . $launch_time    . '", "'
                         . $instance_type  . '", "'
                         . $cloud_context  . '")  ';  

    db_query( $detail_insert_query);  
  }

  $update_query  =  'update {' . CLOUD_BILLING_INSTANCES_DETAILS_TABLE
                   . '} set instance_state_name="'
                   . $instance_state
                   .  '" where instance_id="'
                   . $instance_id . '"';

  $update_result = db_query( $update_query );
  
  return;
}