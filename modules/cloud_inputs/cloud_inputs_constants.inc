<?php

/**
 * @file
 * Provides scripting feature such as bash, Perl and etc (Mainly bash scripts).
 * Works with Cloud and Server Templates module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

define('CLOUD_INPUTS_PREFIX'                , 'cloud_'       ) ;
define('CLOUD_INPUTS_PATH'                  , 'design/inputs') ;
define('CLOUD_INPUTS_PAGER_LIMIT'           , 50             ) ;