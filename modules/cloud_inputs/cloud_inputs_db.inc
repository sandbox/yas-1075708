<?php

/**
 * @file
 * Provides input parameter user interface for Server Templates and Cluster module.
 * Works with Cloud module.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

// scripting functions 
//start
function _cloud_inputs_get($id='') {

  return $query =  'SELECT distinct *, a.param_id as param_id,  b.template_id as server_template_id FROM {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '} a
               LEFT JOIN {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} b on a.param_id=b.param_id and b.deleted=0 and template_id=' . $id . '               
               where a.script_id in (select script_id from {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} where deleted=0 and %s=%s) group by a.input_parameter order by b.param_id' ;
}

function _cloud_inputs_get_cluster($cluster_id,  &$template_ids) {

  $template_ids = _cloud_inputs_get_templates_by_cluster($cluster_id);
  $search_where = ' where 1';
  $join_on = '';
  if ( _cloud_inputs_get_cluster_count($cluster_id)>0) {
    $search_where .=' and cluster_id = ' . $cluster_id;
  }
  else {
    if ($template_ids) {
      $search_where .=' and a.script_id in (select script_id from {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} where deleted=0 and server_template_id in (' . $template_ids . ') )' ;
      //$join_on .= ' and  c.server_template_id in (' . $template_ids.')';
    }  
    else {
      $search_where .=' and cluster_id = ' . $cluster_id;
    }
  }
  //LEFT JOIN {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} c on c.deleted=0 ' . $join_on.'
  return $query =  'SELECT distinct *, a.param_id as param_id, b.template_id as server_template_id FROM {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '} a
               LEFT JOIN {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} b on a.param_id=b.param_id and cluster_id=' . $cluster_id . ' and b.deleted=0               
               ' . $search_where . ' group by a.input_parameter order by b.param_id' ;
}

function _cloud_inputs_update($form_values) {
  
  $query_args = array();
  $module     = $form_values['module'];

  if ($form_values['module'] == 'cloud_server_templates') {

    $query_args[] = 'server_template_id';
    $query_args[] = $form_values['template_id'];
    $update_id    = 'template_id';
    $update_val   = 'template_value';
    $update_valID = $form_values['template_id'];
  }
  else {

    //print $form_values['template_ids'] . 'here';die;
    $query_args[] = 'template_id';
    $query_args[] = $form_values['template_ids'];//$form_values['cluster_id'];
    $query_args[] = 'cluster_id';
    $query_args[] = $form_values['cluster_id'];
    $update_id    = 'cluster_id';
    $update_val   = 'cluster_value';
    $update_valID = $form_values['cluster_id'];
  }
  //print_r($query_args);die;
  if ($form_values['module'] == 'cloud_server_templates') {

    $query       = _cloud_inputs_get($form_values['template_id']) ;
    $result      = db_query( $query, $query_args );
    $result_temp = db_query( $query, $query_args );
  }
  else {

    $result      = _cloud_inputs_get_templates_inputs($query_args, $module);
    $result_temp = _cloud_inputs_get_templates_inputs($query_args, $module);
  }

  //print $query;die;
  $total_rows = 0;
  if ( ( $result_temp )) {
    $total_rows = 0;
    while ($obj = db_fetch_object($result_temp)) {
      $total_rows++;  
    }
  }
  if ( $total_rows > 0 ) {    

    while ($input_obj = db_fetch_object($result)) {

      $param_id = $input_obj->param_id;
      //print_r($param_id);
      
      //print $update_valID;die;
      $get_query = 'select * from {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} 
                where ' . $update_id . '=\'%s\' and param_id=\'%s\' ';

      $query_args   = array(    
        $update_valID ,
        $param_id     ,
      );
      $result1 = db_query( $get_query, $query_args );    
      $result1_temp = db_query( $get_query, $query_args );
      
      if ( ( $result1_temp )) {
        $total_input_rows = 0;
        while ($obj = db_fetch_object($result1_temp)) {
        $total_input_rows++;
        }
      }
      
      //$total_input_rows = mysql_num_rows($result1);
      //$total_input_rows = !empty($total_input_rows) ? $total_input_rows : 0; // db_num_rows($result1);
      
      if ( $total_input_rows > 0 ) {

        $update_query = 'UPDATE {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} 
                  set '   . $update_val . '=\'%s\', value_of_instance_id=\'%s\' 
                  where ' . $update_id  . '=\'%s\' and param_id=\'%s\' '; 

        $input_type = $form_values[$param_id . '_input_type_select'];
      //print $update_id;die;
        $query_args = array(
          $form_values[$param_id . '_' . $input_type],
          $form_values[$param_id . '_instance_select'],
          $update_valID,
          $param_id,
        );
        //print_r($query_args);die;
        db_query( $update_query, $query_args );
      }
      else {      
      
      $insert_query = 'INSERT INTO {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} 
                  ( `param_id` ,  
                    `template_id` , 
                    `template_value` , 
                    `cluster_id` , 
                    `cluster_value` , 
                    `value_of_instance_id` 
                  )  values ';  
            
      $insert_query = $insert_query . " ( '%s', '%s', '%s', '%s', '%s', '%s' ) " ;
      
      
      $input_type = $form_values[$param_id . '_input_type_select'];
      $query_args = array(
        $param_id,
        !empty($form_values['template_id'])
        ?      $form_values['template_id']
        : $input_obj->server_template_id,
        ( $form_values['module'] == 'cloud_server_templates')
        ? $form_values[$param_id . '_' . $input_type]
        : '',
          $form_values['cluster_id'],
        ( $form_values['module'] == 'cloud_cluster')
        ? $form_values[$param_id . '_' . $input_type]
        : '',
        ( !empty ($form_values[$param_id . '_instance_id']) ? $form_values[$param_id . '_instance_id'] : '' ),
      );
      
      db_query( $insert_query, $query_args );
      
      }
      
      $update_query = 'UPDATE {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '} 
                set param_type=\'%s\' 
                where param_id=\'%s\'
          '; 
      $query_args = array(
        $input_type ,
        $param_id   ,
      );
      db_query( $update_query, $query_args );
      
      // User Activity Log
      cloud_audit_user_activity( array(
          'type'    => 'user_activity', 
          'message' => t('Input has been saved: @param_id', array('@param_id' => $param_id)),
          'link'    => '', //'design/inputs&template_id=' . $form_values['template_id'] . '&cluster_id=' . $form_values['cluster_id'] . '&module=' . $form_values['module']
        )
      );
    }  
  }
  else {
    //print 'here';die;
    if ($form_values['module'] == 'cloud_server_templates') {
      $query_args = array(
        'server_template_id'       ,
        $form_values['template_id'],
      );
      $query = _cloud_inputs_get($form_values['template_id']) ;
    }
    else {
      $query = _cloud_inputs_get_cluster($form_values['cluster_id'], $template_ids);  
      //print $query;die;
    }  
    //print $query;
    $result = db_query( $query, $query_args );
    //$total_rows = mysql_num_rows($result);//$result->num_rows;//db_num_rows($result);
    //print $total_rows;die;
    while ($input_obj = db_fetch_object($result)) {

      $param_id = $input_obj->param_id;
      //print_r($input_obj);die;  
      $input_parameter = $input_obj->input_parameter;

      $insert_query = 'INSERT INTO {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} 
                  ( `param_id`            ,  
                    `template_id`         , 
                    `template_value`      , 
                    `cluster_id`          , 
                    `cluster_value`       , 
                    `value_of_instance_id` 
                  )  values 
          ';  
            
      $insert_query = $insert_query . "  ( '%s', '%s', '%s', '%s', '%s', '%s' ) " ;
      
      
      $input_type = $form_values[$param_id . '_input_type_select'];
      $query_args = array(
          $param_id,
         ($form_values['template_id'])
        ? $form_values['template_id']
        : $input_obj->server_template_id,
         ($form_values['module'] == 'cloud_server_templates')
        ? $form_values[$param_id . '_' . $input_type]
        : '',
          $form_values['cluster_id'],
         ($form_values['module'] == 'cloud_cluster')
        ? $form_values[$param_id . '_' . $input_type]
        : '',
          (!empty($form_values[$param_id . '_instance_id']) ? $form_values[$param_id . '_instance_id'] : ''),
      );
      
      db_query( $insert_query, $query_args );  
      
      $update_query = 'UPDATE {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '} 
                set param_type=\'%s\' 
                where param_id=\'%s\'
          '; 
      $query_args = array(
        $input_type ,
        $param_id   ,
      );
      db_query( $update_query, $query_args );
      
      cloud_audit_user_activity( array(
          'type'    => 'user_activity', 
          'message' => t('Input has been modified: id: @param_id', array('@param_id' => $param_id)),
          'link'    => '', // 'design/inputs&template_id=' . $form_values['template_id'] . '&cluster_id=' . $form_values['cluster_id'] . '&module=' . $form_values['module']
        )
      );
    }  
  }

  return;
}

function _cloud_inputs_get_templates_inputs($query_args, $module = '') {

  $search_where = '';
  if ($module != 'cloud_server_templates') {
    if ($query_args[2] && $query_args[3])
      $search_where = ' and ' . $query_args[2] . ' in (' . $query_args[3] . ')';
  }
  else {
    if ($query_args[0] && $query_args[1])
      $search_where = ' and ' . $query_args[0] . ' in (' . $query_args[1] . ')';
  }
  $query = 'select *, template_id as server_template_id from {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} where 1 ' . $search_where . ' order by param_id';
  //print $query;die;
  $result = db_query( $query ); 

  return $result;
}

function _cloud_inputs_get_template_scripts_inputs_count($query_args) {
  
  $query = 'select * from {' . CLOUD_SERVER_TEMPLATES_SCRIPTS_TABLE . '} a left join {' . CLOUD_SCRIPTING_INPUT_PARAMETER_TABLE . '} b on a.script_id=b.script_id where a.deleted=0 and a.server_template_id in (' . $query_args[1] . ')';
  $result = db_query( $query ); 
  
  //$db_obj =  db_fetch_object($result);
  return $result;
  
}

function _cloud_inputs_get_templates_by_cluster($cluster_id) {

  $query = 'select template_id as template_id from {' . CLOUD_CLUSTER_SERVER_TABLE . '} where deleted=0 and cluster_id=' . $cluster_id ;
      
  if (module_exists('cloud_auto_scaling')) {
    $query .= ' union select server_template_id as template_id from {' . CLOUD_AUTO_SCALING_INSTANCE_ARRAY_TABLE . '} where deleted=0 and cluster_id=' . $cluster_id . '';
  }

  $result       = db_query( $query ); 
  $template_ids = array();

  while ($dep_obj = db_fetch_object($result)) {
    $template_ids[] =  $dep_obj->template_id;
  }
  $template_ids = implode(',', $template_ids);

  return $template_ids;
}

function _cloud_inputs_get_cluster_count($cluster_id) {

  $query = 'select * from {' . CLOUD_INPUTS_PARAMETER_VALUES_TABLE . '} where cluster_id=' . $cluster_id;
  $result = db_query( $query ); 
  $num_rows = !empty($result->num_rows) ? $result->num_rows : '';
  return $num_rows;//mysql_num_rows($result);
}

// end